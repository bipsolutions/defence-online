<?php
/**
 * @package WordPress
 * @subpackage HTML5-Reset-WordPress-Theme
 * @since HTML5 Reset 2.0
 */
 get_header(); ?>

<?php if(do_can_user_view_page()): ?>

<div class="level">

 <div class="pure-g dco-content do-suppier-single">
 	<div class="pure-u-24-24">
				<h2 class="line-along"><?php echo strtoupper( get_the_title()	) ; ?></h2>
				<a class="cta backbutton hide-xs hide-sm" href="<?php echo get_the_permalink(884); ?>">Back to Suppliers List</a>
		</div>
 	<div class="pure-u-1 pure-u-md-14-24">

 		<div class="padding-top padding-right">

			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<div class="pure-g">
						
						

						<div class="pure-u-24-24">
								<?php if($banner = get_field('banner_image')):?>
										<img class="banner" src="<?php echo $banner;?>"/>
								<?php endif?>

							
							<?php $image = get_field('logo');?>
									<a class="alignright supplier-logo-single" href="<?php echo get_the_permalink($post->ID)?>"><img alt="<?=$image['alt']; ?>" class="padding " src="<?php echo $image['sizes']['profile-logo']?>" /></a>
							<?php the_content();?>


						</div>
					</div>
			
			
			
		

			<?php endwhile; endif; ?>

		
 		</div>
 		
 	</div>

	 <div class="pure-u-1 pure-u-md-10-24 do-sidebar">

	 		<div class="pure-g grey" style="background: repeating-linear-gradient(45deg, rgba(204,204,204,1), 90%, rgba(204,204,204,0.8)), url(<?php echo $image['sizes']['main-feature']?>);">
	 				<div class="pure-u-24-24">
	 					<h3>Company Criteria</h3>

	 					<?php $regions = do_get_supplier_region_markup($post->ID);?>
							<?php $cats = do_get_supplier_cat_markup($post->ID);?>
								
							<?php if($regions): ?>
								<p><span class="supplier-region">Region: <span><?php echo $regions;?></span></span></p>
							<?php endif?>
							<?php if($cats): ?>
								<p><span class="supplier-cat">Category: <span><?php echo $cats;?></span></span></p>
	 						<?php endif?>
	 				
	 					<h3>Contact Details</h3>

	 					<div class="pure-g">
	 						<div class="pure-u-12-24">
	 								<?php if(get_field('address')):?>
	 								<h4>Address</h4>
	 								<p><?php the_field('address') ?></p>
	 								<?php endif?>

	 								<?php if( have_rows('social_links') ): ?>

										<h4>Social Links</h4>

										<?php while( have_rows('social_links') ): the_row(); ?>

											<a href="<?php the_sub_field('social_url');?>" class="fa-stack">
													<i class="fa fa-circle fa-stack-2x" aria-hidden="true"></i>
													<i class="fa <?php echo get_sub_field('icon')->class; ?> fa-stack-1x fa-inverse"></i>
													
											</a>

										<?php endwhile; ?>

									<?php endif; ?>
	 							</div>
	 							<div class="pure-u-12-24">
	 								<?php if(get_field('email')):?>
	 								<h4>Email</h4>
	 								<p><a href="mailto:<?php the_field('email') ?>"><?php the_field('email') ?></p>
	 								<?php endif ?>
	 								<?php if(get_field('telephone')):?>
	 								<h4>Telephone</h4>
	 								<p><a href="mailto:<?php the_field('telephone') ?>"><?php the_field('telephone') ?></p>
	 							<?php endif; ?>
	 							<?php if(get_field('website')):?>
	 								<h4>Website</h4>
	 								<p><a target="_blank" href="<?php the_field('website') ?>"><?php the_field('website') ?></p>
	 							<?php endif?>
	 							</div>
	 					</div>
	 					
	 					


	 				</div>


	 			
	 		</div>
	 		<?php if(get_field('products_or_servies')):?>
	 		<div class="pure-g grey product-servies">
	 				<div class="pure-u-24-24">
	 					<h3>Products and Services</h3>

	 					
	 					<?php the_field('products_or_servies')?>
	 				
	 					


	 				</div>


	 			
	 		</div>
	 		<?php endif;?>
	 		
	 </div>

 </div>
	</div>

<?php else: ?>
	<!-- What to do if the content is gated -->
	<?php include_once('access.php'); ?>

<?php endif; ?>
	
<?php get_footer(); ?>