<?php
require_once('../../../wp-load.php');

$id 		= $_POST['userid'];
$pubid 		= $_POST['pubid'];

$downloadCount = get_post_meta( $pubid, 'pub_download', true );

if($downloadCount){
	$downloadCount++;
}else{
	$downloadCount = 1;
}

update_post_meta( $pubid, 'pub_download', $downloadCount );

$downloadUserCount = get_user_meta($id, 'downloaded_' . $pubid, true);

if($downloadUserCount){
	$downloadUserCount++;
}else{
	$downloadUserCount = 1;
}

update_user_meta($id, 'downloaded_' . $pubid, $downloadUserCount);

echo json_encode($id);

?>