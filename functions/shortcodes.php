<?php

// This is shortcode to allow the client to add the Password Reset link URL into a custom registration email
// The code in here is basically copied form the core function that generates the default registration email.
// 
// **NOTE** that $user_id has to be passed in on the $atts array(). This function is used in my
// do_do_password_reset_confirmation() function and I get round the user_id issue but doing a find and replace
// to the sghortcode string.
// 
function do_password_link($atts){

	global $wpdb, $wp_hasher;

	$atts = shortcode_atts(
		array(
			'user_id' => '2',
		),
		$atts
	);

	$user = get_userdata( $atts['user_id'] );

	$key = wp_generate_password( 20, false );

	// Now insert the key, hashed, into the DB.
    if ( empty( $wp_hasher ) ) {
        require_once ABSPATH . WPINC . '/class-phpass.php';
        $wp_hasher = new PasswordHash( 8, true );
    }
    $hashed = time() . ':' . $wp_hasher->HashPassword( $key );
    $wpdb->update( $wpdb->users, array( 'user_activation_key' => $hashed ), array( 'user_login' => $user->user_login ) );

    $message .= network_site_url("wp-login.php?action=rp&key=$key&login=" . rawurlencode($user->user_login), 'login');

	return $message;
	
}

add_shortcode( 'do_password_link', 'do_password_link' );

// This is shortcode to allow the client to add the users first name into a custom registration email
//
// **NOTE** that $user_id has to be passed in on the $atts array(). This function is used in my
// do_do_password_reset_confirmation() function and I get round the user_id issue but doing a find and replace
// to the sghortcode string.
//
function do_firstname($atts){
	
	$atts = shortcode_atts(
		array(
			'user_id' => '2',
		),
		$atts
	);

	$firstname = get_field('first_name', 'user_'. $atts['user_id'] );

	return $firstname;
	
}

add_shortcode( 'do_firstname', 'do_firstname' );

// This is shortcode to allow the client to add the users last name into a custom registration email
//
// **NOTE** that $user_id has to be passed in on the $atts array(). This function is used in my
// do_do_password_reset_confirmation() function and I get round the user_id issue but doing a find and replace
// to the sghortcode string.
//

function do_surname($atts){
	
	$atts = shortcode_atts(
		array(
			'user_id' => '2',
		),
		$atts
	);

	$user = get_userdata( $atts['user_id'] );

	$surname = $user->last_name;

	return $surname;
	
}

add_shortcode( 'do_surname', 'do_surname' );


// This is shortcode to allow the client to add the users email address into a custom registration email
//
// **NOTE** that $user_id has to be passed in on the $atts array(). This function is used in my
// do_do_password_reset_confirmation() function and I get round the user_id issue but doing a find and replace
// to the sghortcode string.
//

function do_email($atts){

	$atts = shortcode_atts(
		array(
			'user_id' => '2',
		),
		$atts
	);

	$user = get_userdata( $atts['user_id'] );

	$email = $user->user_email;

	return $email;
	
}

add_shortcode( 'do_email', 'do_email' );


// This is shortcode to allow the client to add the users username address into a custom registration email
//
// **NOTE** that $user_id has to be passed in on the $atts array(). This function is used in my
// do_do_password_reset_confirmation() function and I get round the user_id issue but doing a find and replace
// to the sghortcode string.
//

function do_username($atts){

	$atts = shortcode_atts(
		array(
			'user_id' => '2',
		),
		$atts
	);

	$user = get_userdata( $atts['user_id'] );

	$email = $user->user_login;

	return $email;
	
}

add_shortcode( 'do_username', 'do_username' );

// This is shortcode to allow the client to add a list of webinars into a page.
//

function do_webinar_shortcode($atts){
		$atts = shortcode_atts(
		array(
			'category_id' => '4',
			'number_of_posts' => -1,
			'title' => '',
		),
		$atts
	);

	//Get all webinar custom posts.
	$items = do_post_by_custom_post('webinars', intval($atts['number_of_posts'])); ?>

	<!-- Print the title if it is defined in the shortcode -->
	<?php if($atts != ""): ?>
	<div class="pure-g webinars-container">
			<div class="pure-u-24-24">
				<?php if($atts['title'] != ''): ?>
					<h2 class="post-list-title"><i class="fa fa-headphones" aria-hidden="true"></i> <?php echo $atts['title']; ?></h2>
				<?php endif;?>
			</div>
	<?php endif;

	// Print out each webinar from the $items object fool.
	if($items){
		foreach($items as $item): ?>

				<div class="pure-u-10-24">
					<?php the_field('webinar_date', $item->ID) ?>
				</div>
				<div class="pure-u-12-24">
					<a href="<?php echo $link = get_the_permalink($item->ID);?>"><?php echo $item->post_title; ?></a>
				</div>
				<div class="pure-u-2-24">
					<a href="<?php echo $link;?>"><i class="fa fa-play-circle" aria-hidden="true"></i></a>
				</div>

		<?php endforeach; ?>
	<?php } ?>
	</div>
	<?php
}

add_shortcode( 'do_webinarlist', 'do_webinar_shortcode' );


// This is shortcode to allow the client to display a list of posts from a specific category on a page
function do_post_shortcode( $atts ) {

	//Defaults
	$atts = shortcode_atts(
		array(
			'category_id' => '4',
			'number_of_posts' => '5',
			'title' => '',
			'img' => '#',
			'cta' => '#',
			// If length is not defined then use the globol option for the excerpt as set in 'Site Options'
			'length' => get_field('wordcount_for_post_excerpt_in_post_boxes','options')
		),
		$atts
	);
	// Get all the posts assigned to the requested category
	// Note: do_get_category_posts defined in functions.php
	$items 		= do_get_category_posts($atts['category_id'], $atts['number_of_posts']);

	// the $atts['img'] is if the client wants a image on the title of the list of post. This was added in because
	// our resident Belgian wanted a DCI image add to a list of DCI syndicated posts.
	$imagecode 	= $atts['img'] != '#' ? '<img src="'. $atts['img'] .'" style="width: 1.25em; vertical-align: sub; margin-left: 10px;"/>' : '';

	// Get the URL for the page to display all posts under the requested category
	$ctaURL 	= $atts['cta'] != '#' ? $atts['cta'] : get_category_link( $atts['category_id'] );
	?>

	<!-- Print the title if the client has defined it -->
	<?php if($atts['title'] != ''): ?>
			<h2 class="post-list-title"><?php echo $atts['title']; ?><?php echo $imagecode;?></h2>
	<?php endif;?>

	<!-- Print the posts -->
	<?php if($items):?>

		<?php foreach($items as $item):?>

			<div class="shortcode_item_container"><h3 class="post-list-title"><a href="<?php echo get_the_permalink($item->ID); ?>"><?php echo $item->post_title; ?></a></h3>
			
					<?php echo do_get_content_extract($item->ID, $atts['length'], "<i class='fa fa-arrow-circle-right' aria-hidden='true'></i>");?></div>

		<?php endforeach;?>

	<?php endif; ?>

	<p><a class="cta" href="<?php echo $ctaURL ; ?>">VIEW ALL</a></p>

	<?php


}
add_shortcode( 'do_postlist', 'do_post_shortcode' );

// This is shortcode to allow the client to display a appropriately styled button on a page
function mj_render_cta( $atts ) {

	// Defaults
	extract( $a = shortcode_atts(
		array(
			'colour' => '#212f63',
			'id' => 'cta',
			'text' => 'Learn More',
			'url' => '#',
			'textcolor' => '#FFF',
			'tab' => null,
		), $atts )
	);

	if(substr($a['colour'],0,1) == '#'){

	  	$colour = $a['colour'];

	 }
	
	$tab = !$a['tab'] ? '' : "target='_blank'";

	$html = "<a id='".$a['id']."' ".$tab. " class='cta' style='background-color:".$colour."; color:".$a['textcolor']."' href='".$a['url']."'>";
	$html .= $a['text'];
	$html .= "</a>";

	return $html;


}
add_shortcode( 'cta', 'mj_render_cta' );

// This is shortcode to allow the client to display a image that is linked to download the media pack.
// The reason for this function is that we want to track the users who are downloaing the packs so this function
// sets up a ajax call to log the user click. See process-mediapack-click.php for the meta data it creates.
//
// NOTE: The image that image used and the file used for the media pack are defined in the 'Site Options' page. 

function do_do_media_pack(){

	// Calling the $post glabal so we can log where the user download the media pack from.
	global $post;
	//Set a unique ID so we can avoid javascript problems if there is more than one of these shortcodes on the same
	// page
	$uniqueid = uniqid();
	?>
	<a target="_blank" id="mp-<?php echo $uniqueid ?>" data-user="<?php echo get_current_user_id();?>" data-source="<?php echo $post->ID; ?>" href="<?php echo get_field('media_pack_file','options'); ?>">
	<img src="<?php echo get_field('media_pack_download_image','options')?>" alt="downloadmediapack"  />
	</a>
	<script>
	jQuery(document).ready(function () { 

		jQuery('#mp-<?php echo $uniqueid ?>').click(function(e){

			var data = {
				'userid' : jQuery(this).data('user'),
				'source' : jQuery(this).data('source'),
			}
			jQuery.ajax({
				 type	: "POST",
				 url 	: "<?php bloginfo('template_url');?>/process-mediapack-click.php",
				 data 	: data,
			}); 
		});
	});
	</script>
	<?php
}
add_shortcode( 'mediapack', 'do_do_media_pack' );

// This is shortcode to allow the client to display a ilink to download the media pack.
// The reason for this function is that we want to track the users who are downloaing the packs so this function
// sets up a ajax call to log the user click. See process-mediapack-click.php for the meta data it creates.
//
// NOTE: The image that the file used for the media pack is defined in the 'Site Options' page. 

function do_do_media_pack_cta(){
	//Set a unique ID so we can avoid javascript problems if there is more than one of these shortcodes on the same
	// page
	$uniqueid = uniqid();
	// Calling the $post glabal so we can log where the user download the media pack from.
	global $post;

	// Using output buffering because if we dont the shortcode will always spit this output at the start of the page. I donno why...
	ob_start(); ?>

		<a target="_blank" class="cta" id="mp-<?php echo $uniqueid ?>" data-user="<?php echo get_current_user_id();?>" data-source="<?php echo $post->ID; ?>" href="<?php echo get_field('media_pack_file','options'); ?>">
				Download Media Pack
				</a>
			<script>
	jQuery(document).ready(function () { 

		jQuery('#mp-<?php echo $uniqueid ?>').click(function(e){

			var data = {
				'userid' : jQuery(this).data('user'),
				'source' : jQuery(this).data('source'),
			}
			jQuery.ajax({
				 type: "POST",
				 url: "<?php bloginfo('template_url');?>/process-mediapack-click.php",
				 data: data,
				success: function (result) {
						console.log(result)	;  	         		  
				}
			}); 
		});
	});
	</script>
	<?php
	$output = ob_get_clean();
	return $output;
}

add_shortcode( 'mediapackbutton', 'do_do_media_pack_cta' );

// This is shortcode to allow the client to display a ilink to download the media pack on the marketing page template!!!.
// The reason for this function is that we want to track the users who are downloaing the packs so this function
// sets up a ajax call to log the user click. See process-mediapack-click.php for the meta data it creates.
//
// NOTE: The image that the file used for the media pack is defined in the 'Site Options' page. 

function do_do_media_pack_marketing_cta(){
	//Set a unique ID so we can avoid javascript problems if there is more than one of these shortcodes on the same
	// page
	$uniqueid = uniqid();
	// Calling the $post glabal so we can log where the user download the media pack from.
	global $post;

	// Using output buffering because if we dont the shortcode will always spit this output at the start of the page. I donno why...
	ob_start(); ?>

		<a target="_blank" class="cta marketing-cta" id="mp-<?php echo $uniqueid ?>" data-user="<?php echo get_current_user_id();?>" data-source="<?php echo $post->ID; ?>" href="/contact-us/"><i class="fa fa-download" aria-hidden="true"></i> 
				<?php if($buttonText = get_field('marketing_page_button_text', $post->ID)):?>
					<?php echo $buttonText; ?>
				<?php else:?>
					Download Media Pack
				<?php endif?>
				</a>
			<script>
	jQuery(document).ready(function () { 

		jQuery('#mp-<?php echo $uniqueid ?>').click(function(e){

			var data = {
				'userid' : jQuery(this).data('user'),
				'source' : jQuery(this).data('source'),
			}
			jQuery.ajax({
				 type: "POST",
				 url: "<?php bloginfo('template_url');?>/process-mediapack-click.php",
				 data: data,
				success: function (result) {
						console.log(result)	;  	         		  
				}
			}); 
		});
	});
	</script>
	<?php
	$output = ob_get_clean();
	return $output;
}

add_shortcode( 'mediapackbuttonMarketing', 'do_do_media_pack_marketing_cta' );