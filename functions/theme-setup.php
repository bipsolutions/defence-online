<?php

add_image_size( 'site-logo', 314, 106, false );
add_image_size( 'sub-feature', 287, 164, true );
add_image_size( 'mini-logo', false, 50, false );
add_image_size( 'main-feature-main', 900, 475, true );
add_image_size( 'main-feature', 900, 568, true );
add_image_size( 'main-feature-mini', 600, 379, true );
add_image_size( 'single-post-feature', 580, 325, true );
add_image_size( 'slider-logo', 200, 200, false );
add_image_size( 'profile-logo', 300, 300, false );

function do_reset_setup() {
		
	add_theme_support( 'automatic-feed-links' );
	register_nav_menu( 'primary', __( 'Navigation Menu', 'html5reset' ) );
	add_theme_support( 'post-thumbnails' );

}

add_action( 'after_setup_theme', 'do_reset_setup' );

function html5reset_wp_title( $title, $sep ) {
	
	global $paged, $page;

	if ( is_feed() )
		return $title;

	$title .= get_bloginfo( 'name' );

	$site_description = get_bloginfo( 'description', 'display' );
		if ( $site_description && ( is_home() || is_front_page() ) )
			$title = "$title $sep $site_description";

		if ( $paged >= 2 || $page >= 2 )
			$title = "$title $sep " . sprintf( __( 'Page %s', 'html5reset' ), max( $paged, $page ) );

		return $title;
}

add_filter( 'wp_title', 'html5reset_wp_title', 10, 2 );

if ( !function_exists( 'core_mods' ) ) {
		
	function core_mods() {
			
		if ( !is_admin() ) {
			wp_deregister_script( 'jquery' );
			wp_register_script( 'jquery', ( "https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js" ), false);
			wp_enqueue_script( 'jquery' );

			}
	}

	add_action( 'wp_enqueue_scripts', 'core_mods' );

}

register_nav_menu( 'primary', __( 'Navigation Menu', 'html5reset' ) );

function posted_on() {
	
	printf( __( '<span class="sep">Posted </span><a href="%1$s" title="%2$s" rel="bookmark"><time class="entry-date" datetime="%3$s" pubdate>%4$s</time></a> by <span class="byline author vcard">%5$s</span>', '' ),
			esc_url( get_permalink() ),
			esc_attr( get_the_time() ),
			esc_attr( get_the_date( 'c' ) ),
			esc_html( get_the_date() ),
			esc_attr( get_the_author() )
	);
}

if( function_exists('acf_add_options_page') ) {
	
	$args = array(
		
		'page_title' => 'Site Options',
		'position' => '24.5',
	);
	
	acf_add_options_page( $args );
		
	$args = array(
		
		'page_title' => 'General Sidebar',
		'position' => '24.6',
		'post_id' => 'do-sidebar'

	);

	acf_add_options_page( $args );

	$args = array(
		
		'page_title' => 'Journey Options',
		'position' => '24.7',
		'post_id' => 'do-journey'

	);

	acf_add_options_page( $args );

	$args = array(
		
		'page_title' => 'Supplier Categories',
		'post_id' => 'supplier-cats',
		'parent_slug' => 'edit.php?post_type=supplier',

	);

	acf_add_options_page( $args );

	$args = array(
		
		'page_title' => 'Supplier Regions',
		'post_id' => 'supplier-regions',
		'parent_slug' => 'edit.php?post_type=supplier',

	);

	acf_add_options_page( $args );

}

function remove_menus(){

  remove_menu_page( 'edit-comments.php' ); 

}

add_action('admin_head', 'remove_unneededUI');

function remove_unneededUI() {
  echo '<style>
    #tagsdiv-news_section {
     display:none
    } 
  </style>';
}

add_action( 'admin_menu', 'remove_menus' );

function do_admin_enqueue($hook) {

    wp_enqueue_script( 'do_admin_script', get_template_directory_uri() . '/_/js/admin.js' );

}

add_action( 'admin_enqueue_scripts', 'do_admin_enqueue' );

add_role(
    'stakeholder',
    __( 'Stakeholder' ),
    array(
        'edit_posts'   => true,
        'upload_files'   => true,
        'publish_posts' => true,

    )
);

function dco_redirect_admin(){

	if ( !current_user_can( 'manage_options' ) && !(defined('DOING_AJAX') && DOING_AJAX) ) {

		wp_redirect( site_url() );
	
	exit;

	}
}

add_action( 'admin_init', 'dco_redirect_admin' );


if (!current_user_can('manage_options')) {
	add_filter('show_admin_bar', '__return_false');
}

function my_acf_google_map_api( $api ){
	
	$api['key'] = 'AIzaSyBvuWihKyYb9MxrYfOleywk2iA0ycUty3A';
	
	return $api;
	
}

add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');

function show_users_own_attachments( $query ) {
 
	  $current_user = wp_get_current_user();

	  if(in_array_any( array('stakeholder'), $current_user->roles)){
	  	$id = get_current_user_id();
	 	$query['author'] = $id;
	  }
	 

 return $query;

}

add_filter( 'ajax_query_attachments_args', 'show_users_own_attachments', 1, 1 );


function do_supplier_category_choices($field){

	$field['choices'] = array();

	$choices = get_field('supplier_categories', 'supplier-cats');

	$the_choices = preg_split('/\r\n|[\r\n]/', $choices);

	foreach ($the_choices as $the_choice_key => $value) {
		$field['choices'][$value] = $value;
	}
	
	return $field;
}


add_filter('acf/load_field/key=field_57a1c565aa93d', 'do_supplier_category_choices');

function do_supplier_category_region_choices($field){

	$field['choices'] = array();

	$choices = get_field('supplier_region_select', 'supplier-regions');

	$the_choices = preg_split('/\r\n|[\r\n]/', $choices);

	foreach ($the_choices as $the_choice_key => $value) {
		$field['choices'][$value] = $value;
	}
	
	return $field;
}


add_filter('acf/load_field/key=field_57a1c39c5911a', 'do_supplier_category_region_choices');