<?php

// I created a Stakeholder class object becasuse I wanted a quick way of getting all/some the stakeholders
// and have all the queried stakeholders meta collected into a single object that I can refer to when needed.

class do_stakeholder{
	// Get all the stakeholders.
	// NOTE: Stakeholders are a custom user role create on this theme.

	public function get_all($args = null, $registeredsort = null){

		$args['role'] = 'stakeholder';

		$args['meta_query'] = array(
			// Only get stakeholders whos maintenance mode is turned off
			'relation'		=> 'OR',
			array(
				'key'	  	=> 'maintenance_mode',
				'value'	  	=> '0',
				'compare' 	=> '=',
			),
			array(
				'key'	  	=> 'maintenance_mode',
				'value'	  	=> false,
				'compare' 	=> '=',
			)

		);

		if($registeredsort){
			$args['orderby'] = 'registered';
			$args['order'] = 'DESC';
		}

		$stakeholders = get_users($args);

		// Gat all the $stakeholders meta data and push it into an array called 'meta' on this object
		if($stakeholders){

			foreach($stakeholders as $key => $stakeholder){

				$meta = get_user_meta($stakeholder->ID);
				$nice_meta_array = array();
				foreach ($meta as $key => $value) {
					if(substr($key,0,1) != '_'){
						$nice_meta_array[$key] = $value[0];
					}
					
				}

				$stakeholder->meta = $nice_meta_array;

			}

		}
		

		return $stakeholders;

	}

	// Get one stakeholder by its id.
	// NOTE: Stakeholders are a custom user role create on this theme.
	public function get_one($stakeholder_id){

		$stakeholder 		= get_user_by('ID', $stakeholder_id);
		$meta 				= get_user_meta($stakeholder_id);
		$nice_meta_array 	= array();

		if($meta){

			foreach ($meta as $key => $value) {
				if(substr($key,0,1) != '_'){
					$nice_meta_array[$key] = $value[0];
				}
				
			}

			$stakeholder->meta = $nice_meta_array;

		}
			
		return $stakeholder;

	}

}

// This is a function to disguise a post ID in a generated hash to be used as a get parameter
// Love me some hash.
//
// I use this function in stakeholder-newpost.php to allow the user to view their own posts and
// delete their own posts if required. 
function do_encode_hash($post_id){

	$hash = ($post_id * 132544365355454) - 655432345;

	return $hash;
}

// This is a function to change a hash generated from the above function back to the post ID 
// Love me some hash.
//
// I use this function in stakeholder-newpost.php to allow the user to view their own posts and
// delete their own posts if required.
function do_decode_hash($hash){

	$post_id = ($hash + 655432345) / 132544365355454;

	return $post_id;
}

// This is me hooking into when a stakeholder saves a post. It ensures that the image the client has selected  
// for the 'sh_featured_image' custom field is also assigned as the 'featured image' on the post.
function set_stakeholder_featured_image($value, $post_id, $field){
	
	if($value != ''){

			set_post_thumbnail( $post_id, $value);
	
	}
		
	return $value;

}
add_filter('acf/update_value/name=sh_featured_image', 'set_stakeholder_featured_image', 10, 3);

// To maintain a proper permalink structure for the stakeholders I have set it up so that user
// with a 'stakeholder' role always has a link a stakeholder custom post. This allows a stakeholder to
// have a URL on the site such as /stakeholder/bipsolutions/ rather than /user/mazza-is-a-cock
// This function simply allows you to get the user ID that is linked to a specific stakeholder custom
// post type.
function do_get_stakeholder_id($post_id = null){

	global $post;

	$post_id = $post_id ? $post_id : $post->ID;

	$user = get_field('stakeholder_assigned', $post_id);

	return $user['ID'];

}

// Get all the posts created buy a spefic stakeholder
function do_get_stakeholder_posts($stakeholder_id, $limit = null){

	// Get the user assigned to the stakeholder custom post type (see notes on above function)
	$user_id = do_get_stakeholder_id($stakeholder_id);

	$limit = $limit ? $limit : -1;

	$args = array(
		'posts_per_page'   => $limit,
		'orderby'          => 'date',
		'order'            => 'DESC',
		'post_type'        => 'post',
		'author'	   	   => $user_id,
		'post_status'      => 'publish',							
	);
						
	$author_posts = get_posts($args);

	return $author_posts;

}

// Get all the case studies added buy a spefic stakeholder/user
function do_get_case_studies($user_id, $limit = null){

	$cs = get_field('case_studies', 'user_'.$user_id);

	return $cs;

}

// Get all the ebooks added buy a spefic stakeholder/user
function do_get_ebooks($user_id, $limit = null){

	$eb = get_field('sh_ebooks', 'user_'.$user_id);

	return $eb;

}

// Get the Stakeholder Company Name by the User ID.
//
// NOTE: To maintain a proper permalink structure for the stakeholders I have set it up so that user
// with a 'stakeholder' role always has a link a stakeholder custom post. This allows a stakeholder to
// have a URL on the site such as /stakeholder/bipsolutions/ rather than /user/mazza-is-a-cock
function get_the_stakeholder_by_author($user_id){

	// Call the WordPress Database global to run a mysql query
	global $wpdb;
	// Query 'stakeholder_assigned' custom meta that is equal to the desired user id.
	$query = "select post_id, meta_key from $wpdb->postmeta where meta_value = '" . $user_id . "' and meta_key= 'stakeholder_assigned'";
    
    $result = $wpdb->get_row($query, OBJECT );
    // Get the post ID of the result.  This will be the stakeholder post id that is assigned to the user.
    $stakeholderName = get_the_title($result->post_id);

	return $stakeholderName;
}

// Get the Stakeholder Post ID by the User ID.
//
// NOTE: To maintain a proper permalink structure for the stakeholders I have set it up so that user
// with a 'stakeholder' role always has a link a stakeholder custom post. This allows a stakeholder to
// have a URL on the site such as /stakeholder/bipsolutions/ rather than /user/mazza-is-a-cock
function get_the_stakeholder_id_by_author($user_id){
	
	// Call the WordPress Database global to run a mysql query	
	global $wpdb;
	// Query 'stakeholder_assigned' custom meta that is equal to the desired user id.
	$query = "select post_id, meta_key from $wpdb->postmeta where meta_value = '" . $user_id . "' and meta_key='stakeholder_assigned'";
    
    $result = $wpdb->get_row($query, OBJECT );
    // Get the post ID of the result.  This will be the stakeholder post id that is assigned to the user.
    $stakeholderID = $result->post_id;

	return $stakeholderID;
}

// This function returns the required markup for a specific stakeholder ID
function do_stakeholder_menu($author_id){

	$stakeholder_id 	= get_the_stakeholder_id_by_author($author_id);
	$stakeholder_posts 	= do_get_stakeholder_posts($stakeholder_id);
	$photos 			= get_photos_from_stakeholder('user_' .$author_id);
	$videos				= get_videos_from_stakeholder('user_' .$author_id);
	$services			= get_field('sh_services', 'user_' .$author_id);
	$case_studies		= do_get_case_studies($author_id, false);
	$eBooks				= do_get_ebooks($author_id);
	$current_user_id 	= get_current_user_id();
	$user_info 			= get_userdata($current_user_id);

	?>

		<li><a href="<?php echo get_the_permalink($stakeholder_id); ?>">Home</a></li>
		<?php if($services):?>
			<li><a href="<?php echo get_the_permalink($stakeholder_id); ?>?services=yes">Services</a></li>
		<?php endif; ?>
		<?php if($stakeholder_posts):?>
			<li><a href="<?php echo get_author_posts_url($author_id); ?>">News</a></li>
		<?php endif; ?>
		<?php if($case_studies):?>
			
			<li>
				<a href="#casestudies">Case Studies</a>
					<ul>
						<?php $csIndex = 0; foreach ($case_studies as $case_study) :?>
						
							<?php $caseStudyUrl = get_the_permalink($stakeholder_id). "?casestudy=yes&csindex=" . $csIndex; ?>
								
								<li><a href="<?php echo $caseStudyUrl; ?>"><?php echo $case_study['sh_cs_title']; ?></a></li>
							
							<?php $csIndex++; ?>
						
						<?php endforeach?>
					
					</ul>
			
			</li>
				
		<?php endif; ?>
		<?php if($eBooks):?>
			<li><a href="<?php echo get_the_permalink($stakeholder_id); ?>?ebooks=yes">eBooks</a></li>
		<?php endif; ?>
		<?php if($photos):?>
			<li><a href="<?php echo get_the_permalink($stakeholder_id); ?>?photos=yes">Photos</a></li>
		<?php endif; ?>
		<?php if($videos):?>
			<li><a href="<?php echo get_the_permalink($stakeholder_id); ?>?videos=yes">Videos</a></li>
		<?php endif; ?>
		<?php if(is_array($user_info->roles) && in_array('stakeholder', $user_info->roles) ):?>
			<li><a href="<?php bloginfo('url');?>/stakeholder-admin/" class="cta-full cta-red"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> STAKEHOLDER ADMIN</a></li>
		<?php endif?>
		
	<?php
}

// Get Videos from a specific Stake holder
// Note: the variable $stakeholder_id has to be a string 'user_{$id}' where {$id} is the user id. 
function get_videos_from_stakeholder($stakeholder_id){
	
	$videos = get_field('sh_videos', $stakeholder_id);
	return $videos;
}
// Get Photos from a specific Stake holder
// Note: the variable $stakeholder_id has to be a string 'user_{$id}' where {$id} is the user id. 
function get_photos_from_stakeholder($stakeholder_id){

	$photos = get_field('photos', $stakeholder_id);
	return $photos;

}

// Do the markup for the stakeholder gallery. Note $photos attribute come from function above when
// used in a theme
function do_sh_gallery($photos){
	?>
	
	<?php if($photos): ?>
		<div id="mainwrap">
			
			<ul>

				<?php foreach($photos as $photo):?>
					
					<li><a href="<?php echo $photo['sizes']['main-feature']?>" data-rel="lightbox"><img alt="<?=$photo['alt']; ?>" class="sh-gallery-image" src="<?php echo $photo['sizes']['sub-feature']; ?>" /></a></li>
				
				<?php endforeach; ?>
			
			</ul>

		</div>
	<?php endif;?>

	<?php

}

// Function to create an excerpt of the content added into a stakeholders Case Study
function do_get_casestudy_extract($sh_content, $extractWords = null, $ReadMoreText = null){

	if($extractWords){

		$words = explode(' ', $sh_content, ($extractWords + 1));

		$ReadMoreText = $ReadMoreText ? " ... " . "<a href='". get_permalink($post_id) ."'>". $ReadMoreText . "</a>" : "";

		if(count($words) > $extractWords){

		  	array_pop($words);

		  	$content = implode(' ', $words) .  $ReadMoreText;

		}else{

		  	$content = implode(' ', $words);
		  	
		}

	}
	// Strip out all links and icons.
	return strip_tags($content, '<a><i>');
}

// Function to create an excerpt of the content added into a stakeholders About Us Section
function do_get_stakehoder_extract($sh_content, $extractWords = null, $ReadMoreText = null){

	// This extract will only be used on the 'stakeholder shop front level' (levels/stakeholder.php)
	// so for this we know we dont want h tags displaying here. Below strips out the tags and the content of
	// the tags also.
	$sh_content= preg_replace('/<h2>(.*?)<\/h2>/s', "", $sh_content);
	$sh_content= preg_replace('/<h1>(.*?)<\/h1>/s', "", $sh_content);
	$sh_content= preg_replace('/<h4>(.*?)<\/h4>/s', "", $sh_content);
	$sh_content= preg_replace('/<h3>(.*?)<\/h3>/s', "", $sh_content);

	if($extractWords){

		$words = explode(' ', $sh_content, ($extractWords + 1));

		$ReadMoreText = $ReadMoreText ? " ... " . "<a href='". get_permalink($post_id) ."'>". $ReadMoreText . "</a>" : "";

		if(count($words) > $extractWords){

		  	array_pop($words);

		  	$content = implode(' ', $words) .  $ReadMoreText;

		}else{

		  	$content = implode(' ', $words);
		  	
		}

	}
	// Strip out all links, icons and images.
	return strip_tags($content, '<a><i><img>');
}

// Extract the youTube ID from the various types of youTube URLS.
function extractUTubeVidId($url){
	/*
	* type1: http://www.youtube.com/watch?v=9Jr6OtgiOIw
	* type2: http://www.youtube.com/watch?v=9Jr6OtgiOIw&feature=related
	* type3: http://youtu.be/9Jr6OtgiOIw
	* thumb example: http://img.youtube.com/vi/CCdVhxuBqsA/0.jpg
	*/
	$vid_id = "";
	$flag = false;
	if(isset($url) && !empty($url)){
		/*case1 and 2*/
		$parts = explode("?", $url);
		if(isset($parts) && !empty($parts) && is_array($parts) && count($parts)>1){
			$params = explode("&", $parts[1]);
			if(isset($params) && !empty($params) && is_array($params)){
				foreach($params as $param){
					$kv = explode("=", $param);
					if(isset($kv) && !empty($kv) && is_array($kv) && count($kv)>1){
						if($kv[0]=='v'){
							$vid_id = $kv[1];
							$flag = true;
							break;
						}
					}
				}
			}
		}
		
		/*case 3*/
		if(!$flag){
			$needle = "youtu.be/";
			$pos = null;
			$pos = strpos($url, $needle);
			if ($pos !== false) {
				$start = $pos + strlen($needle);
				$vid_id = substr($url, $start, 11);
				$flag = true;
			}
		}
	}
	return $vid_id;
}

// This function ensures that the stakeholder 'select form navigation thingy' is only displayed on the
// header of the page designated as the 'Stakeholder Page' in the 'Site Options'.
function the_stakeholder_select_form(){
		global $post;

		if(get_field('stakeholder_root_page','options') == $post->ID){
			stakeholder_select_form();
		}

}
add_action('do_after_head', 'the_stakeholder_select_form');

// This function hooks into feature_sh_video field object (acf) to set the choices in the admin
// (site options > featured stakeholder stuff) for the client to choose a featured stakeholder
// video from all the videos added by stakeholders
function feature_video_select_choices( $field ) {
    
    // reset choices
    $field['choices'] = array();

    // get all stakeholders
    $stakeholders = new do_stakeholder;

    $all_stakeholders = $stakeholders->get_all();

   	$stakeholder_videos = array();

   	//For all the stakeholders get their videos and work out the types of videos they are
   	if($all_stakeholders){

   		foreach ($all_stakeholders as $stakeholder) {
    	
	    	$videos = get_field('sh_videos', 'user_' . $stakeholder->ID);

	    	if($videos){
	    		foreach($videos as $video){

	    		$video_type 	= getVideoType($video['url']);

	    		switch ($video_type) {
	    			case 'vimeo':
	    				$video_id 		= getVimeoId($video['url']);
	    				$videoTitle 	= get_vimeo_title_by_id($video_id);
	    				break;
	    			case 'youTube':
	    				$video_id 		= extractUTubeVidId($video['url']);
	    				$videoTitle 	= get_youtube_title_by_id($video_id);
	    				break;
	    			
	    			default:
	    				$video_id = 0;
	    				break;
	    		}
				
				$companyName 	= get_the_stakeholder_by_author($stakeholder->ID);

				// Push all derived data into a nice array.
	    		array_push($stakeholder_videos, 
	    			array(
	    				'type'				=>		$video_type,
	    				'video'				=>		$video_id,
	    				'title'				=>		$videoTitle,
	    				'stakeholder'		=>		$companyName
	    			));
	    		}
	    	}
    	
    	}

   	}
   	
   	// Now set readable choices for the user to select from.
   	//
   	// Note: I am piping date into the key of the choices to I can easily extract that data when using the
   	// results in the theme.
   	if($stakeholder_videos){
   		foreach ($stakeholder_videos as $stakeholder_video) {
    		$field['choices'][$stakeholder->ID . '|' . $stakeholder_video['video'].'|'. $stakeholder_video['type']] = $stakeholder_video['stakeholder'] . " > " . $stakeholder_video['title'];
    	}
   	}

    return $field;
    
}

add_filter('acf/load_field/name=feature_sh_video', 'feature_video_select_choices');

// This function hooks into select_an_ebook field object (acf) to set the choices in the admin
// (site options > featured stakeholder stuff) for the client to choose a featured stakeholder
// video from all the videos added by stakeholders
function feature_ebooks_select_choices( $field ) {
    
    $field['choices'] = array();

    $stakeholders = new do_stakeholder;

    $all_stakeholders = $stakeholders->get_all();

   	$stakeholder_ebooks = array();

   	if($all_stakeholders){

   		foreach ($all_stakeholders as $stakeholder) {
    	
	    	$ebooks = get_field('sh_ebooks', 'user_' . $stakeholder->ID);

	    	if($ebooks){

	    		$index = 0;

	    		foreach($ebooks as $ebook){

						$companyName 	= get_the_stakeholder_by_author($stakeholder->ID);

	    				array_push($stakeholder_ebooks, 
	    					array(
	    						'title'				=>		$ebook['sh_eb_title'],
	    						'description'		=>		$ebook['sh_eb_description'],
	    						'stakeholder_id'	=>		$stakeholder->ID,
	    						'stakeholder'		=>		$companyName,
	    						'index'				=>		$index,
	    					));

	    				$index++;

	    		}
	    	}
    	
    	}

   	}
   
   	// Now set readable choices for the user to select from.
   	//
   	// Note: I am piping date into the key of the choices to I can easily extract that data when using the
   	// results in the theme.
    if($stakeholder_ebooks){

    	foreach ($stakeholder_ebooks as $stakeholder_ebook) {

    		$field['choices'][$stakeholder_ebook['stakeholder_id']."|".$stakeholder_ebook['index']] = $stakeholder_ebook['stakeholder'] . " > " . $stakeholder_ebook['title']; 
    		
   		}
    }
    

    return $field;
    
}

add_filter('acf/load_field/name=select_an_ebook', 'feature_ebooks_select_choices');
add_filter('acf/load_field/name=ebook_select', 'feature_ebooks_select_choices');


function do_get_latest_ebooks($limit = null){

	if(!limit){
		$limit = 6;
	}

	global $wpdb;
	$stakeholders = new do_stakeholder;

    $all_stakeholders = $stakeholders->get_all();

    $stakeholder_live_ids = array();

    foreach ($all_stakeholders as $stakeholder) {
    	array_push($stakeholder_live_ids, $stakeholder->ID);
    }
    $ids = join("','",$stakeholder_live_ids);  
   	
   	$query = "select user_id, meta_key, meta_value from wp_usermeta
   			  where meta_key like 'sh_ebooks%'
   			  and user_id in ('".$ids."')
	   		  and meta_key like '%sh_eb_file%'
			  and meta_value<>''
              order by umeta_id desc limit ". $limit;

   	$results =  $wpdb->get_results($query);

   	if($results){
   		
   		$ebookString = array();

   		foreach ($results as $result) {
   			
   			$string = $result->meta_key;
   			$int = intval(preg_replace('/[^0-9]+/', '', $string), 10);
   			array_push(	$ebookString, $result->user_id."|". $int);
   				
   		}

   	}

   	return $ebookString;

}
function do_get_ebook_infomation($ebookString){

	return array(
		'stakeholder_id' => 	$stakeholder_id  = explode("|",$ebookString)[0],
		'companyName' => 		$companyName 	 = get_the_stakeholder_by_author($stakeholder_id),
		'stakeholder_url' => 	$stakeholder_url = get_the_permalink(get_the_stakeholder_id_by_author($stakeholder_id)),
		'ebook_index' =>		$ebook_index	 = explode("|",$ebookString)[1],
		'the_ebook' =>			$the_ebook		 = get_field('sh_ebooks', 'user_'. $stakeholder_id),
		'ebUrl' => 				$ebUrl			 = $the_ebook[$ebook_index]['sh_eb_file'],
		'ebTitle' => 			$ebTitle		 = $the_ebook[$ebook_index]['sh_eb_title'],
		'ebDescription' => 		$ebDescription   = $the_ebook[$ebook_index]['sh_eb_description'],
	);

}

// Do the markup for the stakeholder navigation level
function stakeholder_select_form($showBackButton = null, $forSidebar = null){

		$stakeholdersObj = new do_stakeholder;
		$stakeholders = $stakeholdersObj->get_all();
	
		?>
		<?php if($stakeholders):?>
		<div class='level blue do-stakeholder-select' style="<?php if(!$showBackButton){echo "margin-bottom:10px";}; ?>">
 			
 			<div class="pure-g dco-content ">

 				<div class="pure-u-24-24 ">
				
					<div class="padding-vertical">

						<?php if($showBackButton):?>

						<div class="stakeholder-do-logo">
							
							<div id="main-logo-container">
								<!-- Note this link is connected to the push slider functionality (jPushMenu()) by id nav-toggle-stakeholder (see /_/inc/functions.js) -->
								<span id="nav-toggle-stakeholder" class="nav-toggle-stakeholder menu-top" href="<?php bloginfo('url'); ?>" ><img alt="<?=get_field('do_logo_for_dark_backgrounds', 'options')['alt']; ?>" class="no-border" src="<?php echo dco_main_dark_logo_image_info()['url'];?>"></span>
							
							</div>
								
						</div>

						<?php endif?>

							<!-- Do markup for the select box false means 'dont show back button'-->
							<?php do_stakeholder_select_box(false); ?>
						

					</div>

				</div>

			</div>
		
		</div>
		<?php endif;?>
	<?php
}

// Do the markup for the stakeholder select box (and javascript navigation).
function do_stakeholder_select_box($showBackButton){

	// Get all the Stakeholders
	$stakeholdersObj 	= new do_stakeholder;
	$stakeholders 		= $stakeholdersObj->get_all();

	// Get a unique id to be used to prevent issues if this function is called more than once on the
	// same page.
	$unique_id 			= uniqid();
	?>
	<?php if($stakeholders):?>
		<form id="search_stakeholders" method="get" style="<?php if($showBackButton){echo "float:right";}; ?>" >
			
			<!-- Note that we are running the function doStakeSearch() on change of the slect form -->
			<span class="stake-title"><span class="stake-title-inner">GO TO A STAKEHOLDER PAGE <?php do_do_help_icon(get_field('content_of_stakeholder_information_icon','options')); ?></span> <select class="stake_search" id="stake_search<?php echo $unique_id; ?>" name="stake_search<?php echo $unique_id; ?>" onchange="doStakeSearch<?php echo $unique_id; ?>()">

				<option value="-1">Select a stakeholder</option>
				
				<?php foreach($stakeholders as $stakeholder):?>

					<?php if($stakeholder->meta['sh_company_name']):?>
						<!-- The key for the option field is the URL of the stakeholder page so we can 
								pass this into javascript -->
						<option value="<?php echo get_the_permalink(get_the_stakeholder_id_by_author($stakeholder->ID));?>"><?php echo $stakeholder->meta['sh_company_name'];?></option>
					
					<?php endif?>
				
				<?php endforeach?>
				</select>
		
			</span>
							
		</form>

		<script>

			function doStakeSearch<?php echo $unique_id; ?>(){
				// Redirect to specific page on change of the select form as marked up above.	
				if(jQuery('#stake_search<?php echo $unique_id; ?>').val() != -1){
					window.location.href = jQuery('#stake_search<?php echo $unique_id; ?>').val();
				}
					
			}

		</script>
	<?php endif; ?>
	<?php
}

// This function is a doozy. In this we are hooking into the core get_permalink() function
// check to see if the url belongs to post that has a category assosiated with Stakeholders.
// If it does then we return a url for the post author instead.
function do_change_permalink($url) {
	global $post;
	// Get the post ID of the URL
	$postid = url_to_postid( $url );
	// Get the ID of the Stakeholders Category
	$stakeholderFeedID = get_cat_ID( 'Stakeholders' );
	// Get all the post categories OBJECTS from the post
	$post_categoryobjects = get_the_category($postid);
	// Define and empty array to push data into
	$catidsinpost = array();
	// For each category object push the term id only into the $catidsinpost array
	if(post_categoryobjects){

		foreach($post_categoryobjects as $post_categoryobject){
			$catid = $post_categoryobject->term_id;
			array_push($catidsinpost, $catid);
		}

	}
	// Now do a search for the exsistance of the Stakeholder Catergory ID in our derived array.
	// If it exsists change the permalink to the author url otherwise keep as is.
	if( in_array($stakeholderFeedID, $catidsinpost) ){

		$url = get_author_posts_url(get_post_field( 'post_author', $postid ));

	};

    return $url;
}

add_filter('the_permalink', 'do_change_permalink');

// Function that looks at user and sets their stakeholder site background image to either an uploaded
// file or a preset background depending on what they have chosen in their admin.
function get_stakeholder_background($sh_id){

	if($bgimage = get_field('upload_your_own_background', 'user_' . $sh_id)){

		$headerBackground = $bgimage['url'];

	}else{

		$headerBackground = get_field('header_background', 'user_' . $sh_id);

	}

	return $headerBackground;
	
}

// Returns true if global post id is has been assigned to the stakeholder category.
function is_stakeholder_news(){

	global $post;

	$postid 				= $post->ID;
	// Get the ID of the Stakeholders Category
	$stakeholderFeedID 		= get_cat_ID( 'Stakeholders' );
	// Get all the post categories OBJECTS from the post
	$post_categoryobjects 	= get_the_category($postid);
	// Define and empty array to push data into
	$catidsinpost 			= array();
	// For each category object push the term id only into the $catidsinpost array
	if($post_categoryobjects){

		foreach($post_categoryobjects as $post_categoryobject){

			$catid = $post_categoryobject->term_id;
			array_push($catidsinpost, $catid);

		}
	}
	// Now do a search for the exsistance of the Stakeholder Catergory ID in our derived array.
	// If it exsists change the permalink to the author url otherwise keep as is.
	if(in_array($stakeholderFeedID, $catidsinpost)){

		return true;

	};

	return false;


}

// Email people as defined in 'Site Options' when a stakeholder adds a new post.
// This function hooks into the save post filter in ACF, if the post id has been
// flagged as a stakeholder post (see stakeholder-newpost.php in which meta data
// of 'stakeholder_post' gets added to the acf_form on save). Note! the priority
// of 22 on the filter registration, this means that this function gets fired AFTER
// the post has been added to the database, hence we have the post_id as parameter.
function when_stakeholder_adds_new_post( $post_id ) {
    
    if( get_post_meta( $post_id, 'stakeholder_post' ) ) {

       $post_author 	= get_post_field( 'post_author', $post_id );
       $stakeholderName = get_the_stakeholder_by_author($post_author);

       $recipients 		= preg_replace('/\s+/', '', get_field('stakeholder_newpost_email','options'));
	   $recipients 		= explode(",", $recipients);

	   wp_mail( $recipients, 'Stakeholder post added by ' . $stakeholderName, 'You should now moderate this post and publish it by visiting '. get_edit_post_link($post_id,''));

    }

    return $post_id;

}

add_filter('acf/save_post' , 'when_stakeholder_adds_new_post', 22 );

// When a user updates thier stakeholder admin for the first time create a stakeholder post and assign a link
// via post meta to the user.
function create_stakeholder_page($post_id){

	// When a user is a stakeholder and they update i.e the exsistance of the required field
	// 'sh_company_name' in the post id meta data
	if(get_field('sh_company_name', $post_id)){

		// Now we get all stakeholder posts because we want to check if this user already has a link
		// associated
		$args = array(
			'posts_per_page'   => -1,
			'post_type'        => 'stakeholders',
			'post_status'      => array('publish', 'pending', 'draft', 'future', 'private', 'inherit'),

		);

		$stakeholderPosts = get_posts($args);

		$linkExsists = false;

		$debug = array();

		//Check stakeholder posts to see if user id is on any of the stakeholder meta
		if($stakeholderPosts){

			foreach ($stakeholderPosts as $stakeholderPost) {
				
				$userlink = get_field('stakeholder_assigned', $stakeholderPost->ID);

				if('user_'. $userlink['ID'] == $post_id){

					$linkExsists = $stakeholderPost->ID;

				}

			}

		}
		

		//If link alread exsists just assign the title and permalink of the post incase the user has changed their company name
		if($linkExsists){

			$title = get_field('sh_company_name', $post_id);
			
			$update_title = array(
					// The ID of the post to update
      				'ID'           => $linkExsists,
      				// Update the title of the post
      				'post_title'   => $title,
      				// Update the permalink of the post
      				'post_name'	   => sanitize_title_with_dashes(get_field($title))
  			);

			// Update the post in the database
  			wp_update_post( $update_title );

		}else{
			//If no link. Create a new post. Give it a title and set the user to page relationship
			$title = get_field('sh_company_name', $post_id);

			$my_post = array(
			   // Set the title of the post to the compnay name.
			  'post_title'    => $title,
			  // Set to draft so the new stakeholder post can be moderated
			  'post_status'   => 'draft',
			  'post_type'	  => 'stakeholders'
			);
			// Convert string 'user_{id}' into '{$id}'
			$user_id 			= intval(substr( $post_id, 5));
			// Add new post and collect ID of post
			$new_stakeholder 	= wp_insert_post( $my_post );
			// Assign link of user to the new post stakeholder post id via the 'stakeholder_assigned' meta
			update_field('stakeholder_assigned', $user_id, $new_stakeholder);

		}
		
	}

	// Now send an email to recipients (set in Site Options) so they can moderate this activity
	
	//Gewt user info
	$current_user_id 	= get_current_user_id();
	$user_info 			= get_userdata($current_user_id);

	// If the user who pressed save is a stakeholder and its the first time they have saved...
	if(is_array($user_info->roles) && in_array('stakeholder', $user_info->roles)){

		if(get_user_meta($current_user_id, 'stakeholder_first_save', true) == ''){
			
			$recipients = preg_replace('/\s+/', '', get_field('stakeholder_activates_email','options'));

			$recipients = explode(",", $recipients);
			// Send an email
			wp_mail( $recipients, $user_info->user_login . ' just activated thier stakeholder micro-site', 'You should now moderate this post and publish it by visiting '.get_bloginfo('url').'/wp-admin/edit.php?post_type=stakeholders');

		}
		// Now update user meta so we know they have saved and this email wont go out again.
		update_user_meta($current_user_id, 'stakeholder_first_save', 1);

	}
	
}

add_filter('acf/save_post' , 'create_stakeholder_page', 21 );

// Function to test if the stakeholder maintaince mode is on. This function will allow the owner of the page
// to bypass the logic (and also administrators)
function is_maintenance_mode_on(){

	global $post;

	$current_page_user_id 		= do_get_stakeholder_id($post_id);
	$current_user 				= wp_get_current_user();

	if(in_array_any( array('administrator'), $current_user->roles)){
		return false;
	}

	//Override maintenance mode if the page is the current users page.
	if( is_user_logged_in() ){
		$user_id = get_current_user_id();

		if($user_id == $current_page_user_id){
				return false;
		}
	}
	// If not the owner or admin then respect the maintainance mode decision...
	$mode = get_field('maintenance_mode', 'user_' . $current_page_user_id);

	if($mode){
	    return true;
	}

	return false;
}

// Do the markup for the stakeholders gallery level.
function do_do_stakeholder_photos($stakeholderID = null, $companyName){

	 $unique_id = uniqid();

	?>
		<?php if($photos = get_photos_from_stakeholder($stakeholderID)):?>
			
			<div class="level single-stakeholder">

				<div class="pure-g dco-content <?php echo $unique_id; ?>">

		 			<div class="pure-u-24-24">

						<div class="padding-horizontal">
							<h2 class="line-along">PHOTOS BY <?php echo $companyName; ?></h2>
						</div>
				
					</div>

					<div class="pure-u-24-24">

						<?php

							if($photos){
								do_sh_gallery($photos); 
							}
						
						?>

					</div>
	 
				</div>

			</div>
	
		<?php endif?>

	<?php

}

// Do the markup for the stakeholders services level.
function do_do_stakeholder_services($stakeholderID = null, $companyName){

	$unique_id = uniqid();

	?>

	<?php if($services = get_field('sh_services', $stakeholderID)):?>

		 <div class="level single-stakeholder">

			<div class="pure-g dco-content <?php echo $unique_id; ?>">

			 	<div class="pure-u-24-24">

			 		<div class="padding-horizontal">
						<h2 class="line-along">SERVICES PROVIDED BY <?php echo $companyName; ?></h2>
					</div>
					
				</div>

				<?php foreach ($services as $service): ?>
					
					<div class="pure-u-2-24">
						
					</div>

					<div class="pure-u-2-24">

						<?php if($service['service_icon']):?>
								<p style="text-align:center; margin-top: 10px;"><i class="fa <?php echo $service['service_icon']->class; ?> large-fa" aria-hidden="true"></i></p>
						<?php else:?>
								<p style="text-align:center; margin-top: 10px;"><i class="fa fa-cogs large-fa" aria-hidden="true"></i></p>
						<?php endif;?>

					</div>

					<div class="pure-u-18-24">
						
						<h3 class="service-title"><strong><?php echo $service['service_name'];?></strong></h3>
						<p><?php echo $service['service_description'];?></p>

					</div>

					<div class="pure-u-2-24">
						
					</div>
				<?php endforeach; ?> 	
				 
			</div>

		</div>

	<?php endif;?>

	<?php


}

// Do the markup for the stakeholders ebooks level.
function do_do_stakeholder_ebooks($stakeholderID = null, $companyName){

	$unique_id = uniqid();

?>

	<?php if($ebs = do_get_ebooks($stakeholderID)):?>
		
		<div class="level single-stakeholder cs-level">

			<div class="pure-g dco-content <?php echo $unique_id; ?>">

				<?php
					$ebs = array_reverse($ebs);
				?>

		 		<div class="pure-u-24-24">

		 			<div class="padding-horizontal">
						<h2 class="line-along">EBOOKS BY <?php echo $companyName; ?></h2>
					</div>
				</div>

				<?php foreach ($ebs as $eb): ?>
					
					<?php $ebUrl = $eb['sh_eb_file']; ?>
					<div class="pure-u-2-24">
						
					</div>
					
					<div class="pure-u-2-24">

						<p style="text-align:center; margin-top: 10px;"><a target="_blank" href="<?php echo $ebUrl; ?>"><i class="fa fa-file-pdf-o large-fa" aria-hidden="true"></i></a></p>
					
					</div>

					<div class="pure-u-18-24">
						
						<h3 class="cs-title"><strong><a target="_blank" href="<?php echo $ebUrl; ?>"><?php echo $eb['sh_eb_title'];?></a></strong></h3>
						<?php if($eb['sh_eb_description']):?>
							<?php echo $eb['sh_eb_description'];?>
						<?php endif; ?>
						<p><a target="_blank" class="cta" href="<?php echo $ebUrl; ?>">DOWNLOAD</a></p>

					</div>

					<div class="pure-u-2-24">
					
					</div>
			
					<?php $csIndex ++;?>
		
				<?php endforeach; ?>
		 	 
			</div>

		</div>

	<?php endif; ?>

<?php

}

// Do the markup for a list of videos. Works in tandem with do_do_stakeholder_videos()
function do_video_list($videos){
	
	$count_videos = count($videos);
	$index = 1;
	?>
	 	<div class="pure-g">
			<?php foreach($videos as $video):?>
				<?php if ( $count_videos & 1 && $count_videos == $index) :?>
					<div class="pure-u-1 pure-u-md-24-24 center">
						<div class="single-video-row">
							<div class="padding">
								<?php echo do_shortcode('[fve]'. $video['url'] . '[/fve]'); ?>
							</div>
						</div>
					</div>
				<?php else: ?>
					<div class="pure-u-1 pure-u-md-12-24">
						<div class="padding">
							<?php echo do_shortcode('[fve]'. $video['url'] . '[/fve]'); ?>
						</div>
					</div>
				<?php endif; ?>
			<?php $index++; endforeach;?>
		</div>	
	
		
	<?php 
}

// Do the markup for the stakeholders video level.
function do_do_stakeholder_videos($stakeholderID = null, $companyName){

	$unique_id = uniqid();

?>
	<?php if($videos = get_videos_from_stakeholder($stakeholderID)):?>

		<div class="level single-stakeholder">

			<div class="pure-g dco-content <?php echo $unique_id; ?>">

				 	<div class="pure-u-24-24">

				 		<div class="padding-horizontal">
							<h2 class="line-along">VIDEOS BY <?php echo $companyName; ?></h2>
						</div>

					</div>

					<div class="pure-u-24-24">

						<?php do_video_list($videos); ?>

					</div>
			 
			</div>

		</div>

	<?php endif?>

<?php 

}

// Do the markup for the stakeholders about us level.
function do_do_stakeholder_about($stakeholderID = null, $companyName){

?>		
	<?php if($aboutus = get_field('sh_about_us_text', $stakeholderID )):?>

		<div class="level single-stakeholder">

			<div class="pure-g dco-content do-suppier-single">
 				
	 			<div class="pure-u-24-24">

					<div class="padding-horizontal">
						
						<h2 class="line-along">ABOUT <?php echo $companyName; ?></h2>

					</div>
					
				</div>

	 			<div class="pure-u-1">

	 				<div class="padding-top padding-right">

						<div class="pure-g">

							<div class="pure-u-1 pure-u-md-24-24 cs-content">
									
								<div class="padding"><?php echo $aboutus;?></div>

							</div>

						</div>
			
	 				</div>
	 		
	 			</div>

			</div>

		 </div>

	<?php endif;?>

<?php

}

// Do the markup for the stakeholders latest news level.
function do_do_stakeholder_latest_news($companyName){

	$unique_id 			= uniqid();
	// Get max of 4 stakholder posts associated to this user.
	$stakeholder_posts 	= do_get_stakeholder_posts($post->ID, 4);
	// Count the results so we can display accordingly
	$post_count 		= count($stakeholder_posts);
	
	 if($stakeholder_posts && $post_count >= 2):?>
			
		<div class="level single-stakeholder">

			<div class="pure-g dco-content <?php echo $unique_id; ?>">

				<div class="pure-u-24-24">

					<div class="padding-horizontal">
						<h2 class="line-along">LATEST NEWS FROM <?php echo $companyName; ?></h2>
					</div>
				
				</div>

				<?php 
					// Change the size of the columns for the news posts depending on the amount of
					// posts
					
					$dist = $post_count ? (24 / $post_count) : 24; ?>

 				<?php foreach ($stakeholder_posts as $stakeholder_post): ?>
 		
 					<div class="pure-u-1 pure-u-md-<?php echo $dist;?>-24 sub-feature sub-feature-sh">
 			
 						<div class="padding">
 				
 							<?php do_sub_feature_panel($stakeholder_post, array('excerpt' => true)); ?>
 			
 						</div>
 		
 					</div>
 	
 				<?php endforeach ?>

 				<div class="pure-u-24-24">
 					
 					<p><a class="cta" href="<?php echo get_author_posts_url( do_get_stakeholder_id()); ?>">VIEW ALL NEWS BY <?php echo strtoupper(get_the_title()) ; ?></a></p>
 	
 				</div>

 			</div>

		</div>

		<script type="text/javascript">
	
			jQuery(document).imagesLoaded( function() {
				// Align all the sections of the news markup based on the maxium heights of each section
				get_max_height_from_set('.<?php echo $unique_id;  ?> .sub-image-container',0);
				get_max_height_from_set('.<?php echo $unique_id;  ?> .slide-title',0);
				get_max_height_from_set('.<?php echo $unique_id;  ?> .posted',0);
				get_max_height_from_set('.<?php echo $unique_id;  ?> .sub-feature-title-container',0);
				get_max_height_from_set('.<?php echo $unique_id;  ?> .sub-feature-excerpt',0);

			});

		</script>

	<?php endif?>

<?php

}

// Do the markup for the stakeholders case study list
function do_do_stakeholder_case_study_list($stakeholderID = null, $companyName){

	$unique_id 			= uniqid();

?>

	<?php if($case_studies = do_get_case_studies($stakeholderID)):?>
				
		<div class="level single-stakeholder cs-level" id="casestudies">

			<div class="pure-g dco-content <?php echo $unique_id; ?>">

				<?php
					$case_studies 		= array_reverse($case_studies);
					$case_studies_count = count($case_studies);
				?>

 				<div class="pure-u-24-24">

 					<div class="padding-horizontal">
						<h2 class="line-along">CASE STUDIES BY <?php echo $companyName; ?></h2>
					</div>

				</div>

				<?php $csIndex = $case_studies_count - 1; foreach ($case_studies as $case_study): ?>
					
					<?php $caseStudyUrl = get_the_permalink($stakeholder_id). "?casestudy=yes&csindex=" . $csIndex; ?>
						
					<div class="pure-u-2-24">
		
					</div>
					
					<div class="pure-u-2-24">

							<p style="text-align:center; margin-top: 10px;"><a href="<?php echo $caseStudyUrl; ?>"><i class="fa fa-book large-fa" aria-hidden="true"></i></a></p>
		
					</div>
					<div class="pure-u-18-24">
		
						<h3 class="cs-title"><strong><a href="<?php echo $caseStudyUrl; ?>"><?php echo $case_study['sh_cs_title'];?></a></strong></h3>
						
						<p><?php echo do_get_casestudy_extract($case_study['sh_cs_content'], 75, '<a class="readmore" href="'.$caseStudyUrl.'"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>') ;?></p>

					</div>

					<div class="pure-u-2-24">
		
					</div>
						<?php $csIndex --;?>
				
				<?php endforeach; ?>
 	
 			</div>

		</div>
	
	<?php endif; ?>


<?php

}

// Do the markup for the stakeholders case study page
function do_do_stakeholder_case_study_page($stakeholderID = null, $companyName){

	$unique_id 			= uniqid();

?>
		<?php $cs = do_get_case_studies($stakeholderID)[$_GET['csindex']];?>
		
		<div class="level single-stakeholder cs-content">

			<div class="pure-g dco-content <?php echo $unique_id; ?>">

		 		<div class="pure-u-24-24">

					<h2 style="margin-bottom:0" class="line-along"><?php echo $cs['sh_cs_title'] ?></h2>
					
					<p style="margin-top:0; padding-top:0"><em>Case Study by: <?php echo $companyName; ?></em></p>
				
				</div>

				<div class="pure-u-24-24">
					<?php echo $cs['sh_cs_content'] ?>
				</div>

			</div>
	
		</div>

<?php

}

function do_get_all_stakeholder_authors_raw(){

	// Get all Stakeholder Posts of the required post status
	$args = array(
			'posts_per_page' => -1,
			'post_type'      => 'stakeholders',
	);

	$results = get_posts($args);
	

	$author_ids = array();

	//For each Stakeholder post get the author id and add it to an array
	foreach ($results as $result) {
		
		$stakeholder_id	= do_get_stakeholder_id($result->ID);
		
		array_push($author_ids, $stakeholder_id);
		
	}

	return $author_ids;
}

// Get an array of ID os all the stakeholder users from a desired post status (of the stakeholder post)
function do_get_all_stakeholder_authors($post_status){

	// Get all Stakeholder Posts of the required post status
	$args = array(
			'post_status' 	 => $post_status,
			'posts_per_page' => -1,
			'post_type'      => 'stakeholders',
	);

	$results = get_posts($args);
	

	$author_ids = array();

	//For each Stakeholder post get the author id and add it to an array
	foreach ($results as $result) {
		// In this current functionality we want to only include athors whos maintenace mode
		// is on. Because further down the line we are adding these results to a list of posts
		// we want to excude. TODO: Increace the scope of the function to be more dynamic to be allowed to be
		// used in other senarios.
		$stakeholder_id	= do_get_stakeholder_id($result->ID);
		$mode = get_field('maintenance_mode', 'user_' . $stakeholder_id);
		if($mode || $mode==1){
			array_push($author_ids, $stakeholder_id);
		}
		
	}

	return $author_ids;

};

// Get all the posts by authors who are stakeholders
function do_get_all_posts_by_stakeholders($post_status){

	$author_ids = do_get_all_stakeholder_authors($post_status);
	
	$query = new WP_Query(
		array(
			'author__in' => $author_ids,
 			'post_type'  => 'post',
 		) );
	
    return $query->posts;

}