<?php

//Get the keys from a URL.
function do_get_url_var($url, $key){

	$url_array = parse_url($url);
	
	parse_str($url_array['query'], $vars);
						
	return $vars[$key];

}
//Return True if a post id exsists
function do_post_exists( $id ) {
    return is_string( get_post_status( $id ) );
}

function generate_ad_padding($top = null,$bottom=null,$left=null,$right=null){
    
    $top = !is_null($top) ? $top : "10";
    $bottom = !is_null($bottom) ? $bottom : "10";
    $left = !is_null($left) ? $left : "0";
    $right = !is_null($right) ? $right : "0";
    $style= "padding-top:".$top."px; padding-bottom: ".$bottom."px; padding-left: ".$left."px; padding-right: ".$right."px; ";
    return $style;
}
function remove_autop($content){
    $content = str_replace(array("<p>","</p>"), "", $content);
    return $content;
}

//Remove whitespace from a string
function remove_whitespace($output){

		$output = str_replace(array("\r\n", "\r"), "\n", $output);
		$lines = explode("\n", $output);
		$new_lines = array();

		if($lines){
				foreach ($lines as $i => $line) {
    				if(!empty($line))
       				 $new_lines[] = trim($line);
					}
		}
		
		return implode($new_lines);

}

// Quicky function to get the site URL

function do_site_url(){

    $site_url = get_bloginfo('url');

    return $site_url;
    
}

add_shortcode( 'do_site_url', 'do_site_url' );

//Search in a multidimentional array for the exsistance of something
function in_array_r($needle, $haystack, $strict = false) {
    foreach ($haystack as $item) {
        if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && in_array_r($needle, $item, $strict))) {
            return true;
        }
    }

    return false;
}

// Get array of filenames in a specific directory.
function mj_get_the_level_parts($dir)
{

    $filecount = 0;
    $the_part_file = array();
                      
        if ($handle = opendir($dir))
            {

              while (false !== ($entry = readdir($handle)))

                {
                  
                  if ($entry != "." && $entry != "..")

                    {
                        $ext = pathinfo($entry, PATHINFO_EXTENSION);
                        $entry = str_replace(".".$ext, "", $entry);                
                        $the_part_file[$filecount] = $entry;
                        $filecount++;
                    }

                }
            
              closedir($handle);

            }

          return $the_part_file;
}

//Returns true if any of an array of needles are in the array haystack.
function in_array_any($needles, $haystack) {
   return !!array_intersect($needles, $haystack);
}

// Function to convert a hex colour to an rgb (D-uh!!!!!!)
function hex2rgb($hex) {
   $hex = str_replace("#", "", $hex);

   if(strlen($hex) == 3) {
      $r = hexdec(substr($hex,0,1).substr($hex,0,1));
      $g = hexdec(substr($hex,1,1).substr($hex,1,1));
      $b = hexdec(substr($hex,2,1).substr($hex,2,1));
   } else {
      $r = hexdec(substr($hex,0,2));
      $g = hexdec(substr($hex,2,2));
      $b = hexdec(substr($hex,4,2));
   }
   $rgb = array($r, $g, $b);
   //return implode(",", $rgb); // returns the rgb values separated by commas
   return $rgb; // returns an array with the rgb values
}


/**
 * Extracts the daily motion id from a daily motion url.
 * Returns false if the url is not recognized as a daily motion url.
 */
function getDailyMotionId($url)
{
    if (preg_match('!^.+dailymotion\.com/(video|hub)/([^_]+)[^#]*(#video=([^_&]+))?|(dai\.ly/([^_]+))!', $url, $m)) {
        if (isset($m[6])) {
            return $m[6];
        }
        if (isset($m[4])) {
            return $m[4];
        }
        return $m[2];
    }
    return false;
}
/**
 * Extracts the vimeo id from a vimeo url.
 * Returns false if the url is not recognized as a vimeo url.
 */
function getVimeoId($url)
{
    if (preg_match('#(?:https?://)?(?:www.)?(?:player.)?vimeo.com/(?:[a-z]*/)*([0-9]{6,11})[?]?.*#', $url, $m)) {
        return $m[1];
    }
    return false;
}
/**
 * Extracts the youtube id from a youtube url.
 * Returns false if the url is not recognized as a youtube url.
 */
function getYoutubeId($url)
{
    $parts = parse_url($url);
    if (isset($parts['host'])) {
        $host = $parts['host'];
        if (
            false === strpos($host, 'youtube') &&
            false === strpos($host, 'youtu.be')
        ) {
            return false;
        }
    }
    if (isset($parts['query'])) {
        parse_str($parts['query'], $qs);
        if (isset($qs['v'])) {
            return $qs['v'];
        }
        else if (isset($qs['vi'])) {
            return $qs['vi'];
        }
    }
    if (isset($parts['path'])) {
        $path = explode('/', trim($parts['path'], '/'));
        return $path[count($path) - 1];
    }
    return false;
}
/**
 * Gets the thumbnail url associated with an url from either:
 *
 *      - youtube
 *      - daily motion
 *      - vimeo
 *
 * Returns false if the url couldn't be identified.
 *
 * In the case of you tube, we can use the second parameter (format), which
 * takes one of the following values:
 *      - small         (returns the url for a small thumbnail)
 *      - medium        (returns the url for a medium thumbnail)
 *
 *
 *
 */
function getVideoThumbnailByUrl($url, $format = 'small')
{
    if (false !== ($id = getVimeoId($url))) {
        $hash = unserialize(file_get_contents("https://vimeo.com/api/v2/video/$id.php"));
        /**
         * thumbnail_small
         * thumbnail_medium
         * thumbnail_large
         */
        return $hash[0]['thumbnail_large'];
    }
    elseif (false !== ($id = getDailyMotionId($url))) {
        return 'https://www.dailymotion.com/thumbnail/video/' . $id;
    }
    elseif (false !== ($id = getYoutubeId($url))) {
        /**
         * http://img.youtube.com/vi/<insert-youtube-video-id-here>/0.jpg
         * http://img.youtube.com/vi/<insert-youtube-video-id-here>/1.jpg
         * http://img.youtube.com/vi/<insert-youtube-video-id-here>/2.jpg
         * http://img.youtube.com/vi/<insert-youtube-video-id-here>/3.jpg
         *
         * http://img.youtube.com/vi/<insert-youtube-video-id-here>/default.jpg
         * http://img.youtube.com/vi/<insert-youtube-video-id-here>/hqdefault.jpg
         * http://img.youtube.com/vi/<insert-youtube-video-id-here>/mqdefault.jpg
         * http://img.youtube.com/vi/<insert-youtube-video-id-here>/sddefault.jpg
         * http://img.youtube.com/vi/<insert-youtube-video-id-here>/maxresdefault.jpg
         */
        if ('medium' === $format) {
            return 'https://img.youtube.com/vi/' . $id . '/hqdefault.jpg';
        }
        return 'https://img.youtube.com/vi/' . $id . '/default.jpg';
    }
    return false;
}
/**
 * Returns the type of the actual video for a given url which belongs to either:
 *
 *      - youtube
 *      - daily motion
 *      - vimeo
 *
 * Or returns false in case of failure.
 * This function can be used for creating video sitemaps.
 */
function getVideoLocation($url)
{
    if (false !== ($id = getDailyMotionId($url))) {
        return 'https://www.dailymotion.com/embed/video/' . $id;
    }
    elseif (false !== ($id = getVimeoId($url))) {
        return 'https://player.vimeo.com/video/' . $id;
    }
    elseif (false !== ($id = getYoutubeId($url))) {
        return 'https://www.youtube.com/embed/' . $id;
    }
    return false;
}
/**
 * Returns the type of a video for a given url which belongs to either:
 *
 *      - youtube
 *      - daily motion
 *      - vimeo
 *
 * Or returns false in case of failure.
 * This function can be used for creating video sitemaps.
 */
function getVideoType($url)
{
    if (false !== ($id = getDailyMotionId($url))) {
        return 'dailyMotion';
    }
    elseif (false !== ($id = getVimeoId($url))) {
        return 'vimeo';
    }
    elseif (false !== ($id = getYoutubeId($url))) {
        return 'youTube';
    }
    return false;
}
/**
 * Returns the html code for an embed responsive video, for a given url.
 * The url has to be either from:
 * - youtube
 * - daily motion
 * - vimeo
 *
 * Returns false in case of failure
 */
function getEmbedVideo($url)
{
    $code = <<<EEE
    <style>
        .embed-container { 
            position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; 
        }
        .embed-container iframe, .embed-container object, .embed-container embed { 
            position: absolute; top: 0; left: 0; width: 100%; height: 100%; 
        }
    </style>
EEE;
    if (false !== ($id = getDailyMotionId($url))) {
        $code .= <<<EEE
<div class='embed-container'><iframe src='https://www.dailymotion.com/embed/video/$id' frameborder='0' webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe></div>
EEE;
    }
    elseif (false !== ($id = getVimeoId($url))) {
        $code .= <<<EEE
<div class='embed-container'><iframe src='https://player.vimeo.com/video/$id' frameborder='0' webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe></div>
EEE;
    }
    elseif (false !== ($id = getYoutubeId($url))) {
        $code .= <<<EEE
<div class='embed-container'><iframe src='https://www.youtube.com/embed/$id' frameborder='0' allowfullscreen></iframe></div>
EEE;
    }
    else {
        $code = false;
    }
    return $code;
}

// Returns the title of a Vimeo video by passing the vimeo ID

function get_vimeo_title_by_id($id){

    $hash = json_decode(file_get_contents("https://vimeo.com/api/v2/video/".$id.".json"));

     return $hash[0]->title;
}

// Returns the title of a youTube video by passing the youTube ID

function get_youtube_title_by_id($id){

    $content        = file_get_contents("https://youtube.com/get_video_info?video_id=".$id);
    parse_str($content, $ytarr);
    $videoTitle     =  $ytarr['title'];

    return $videoTitle ;
}

// Returns true if current device is a mobile and not a tablet.
//
// Uses class crerated by: http://mobiledetect.net/
function do_wp_is_mobile() {
    include_once ( get_template_directory() . '/mobile-detect.php');
    $detect = new Mobile_Detect;
    if( $detect->isMobile() && !$detect->isTablet() ) {
        return true;
    } else {
        return false;
    }
}

// Returns true if current device is a tabet and not a mobile.
//
// Uses class crerated by: http://mobiledetect.net/

function do_wp_is_tablet() {
    include_once ( get_template_directory() . '/mobile-detect.php');
    $detect = new Mobile_Detect;
    if( $detect->isTablet() ) {
        return true;
    } else {
        return false;
    }
}


function remoteFileExists($url) {
    $curl = curl_init($url);

    //don't fetch the actual page, you only want to check the connection is ok
    curl_setopt($curl, CURLOPT_NOBODY, true);

    //do request
    $result = curl_exec($curl);

    $ret = false;

    //if request did not fail
    if ($result !== false) {
        //if request was ok, check response code
        $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);  

        if ($statusCode == 200) {
            $ret = true;   
        }
    }

    curl_close($curl);

    return $ret;
}

function do_do_pdf_image($url = null){

    $imURL = $url;
    
    if ($imURL) {
        
        $name       = explode('wp-content',$imURL);
        $local      = '/app/ukcontr/dco/wp-content'.$name[1];
        $path_parts = pathinfo($local);
        $imName     = $path_parts['filename'];
        
        if (strpos($imName,'.pdf') !== false) {
            $imName = str_replace('.pdf','-L.jpg', $imName);
        } else {
            $imName = $imName.'-L.jpg';
        }

        $imNew = get_bloginfo('url').'/wp-content/themes/dco/images/pdfs/'.$imName;
        
        $exists = remoteFileExists($imNew);
        
        if (!$exists) {
            $im = new Imagick($local.'[0]');
            $im->setImageFormat('jpg');
            $im->thumbnailImage(300, 0);
            $im->writeImage('/app/ukcontr/dco/wp-content/themes/dco/images/pdfs/'.$imName);
        }
        $newImageURL = $imNew;
    }

    return $imNew;
}

function do_add_subscribers_to_dropdown( $query_args, $r ) {
    
    $args = array(
    
        'role__in'     => array('administrator', 'stakeholder', 'editor'),
        'fields' => 'ID'
    
    );

    $users = get_users( $args );

    $query_args['include'] =  $users;
    
    unset( $query_args['who'] );
 
    return $query_args;
}
add_filter( 'wp_dropdown_users_args', 'do_add_subscribers_to_dropdown', 10, 2 );
