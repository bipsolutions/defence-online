<?php

add_action( 'init', 'cptui_register_my_cpts_videos' );

function cptui_register_my_cpts_videos() {
	$labels = array(
		"name" => __( 'Videos', '' ),
		"singular_name" => __( 'Video', '' ),
		);

	$args = array(
		"label" => __( 'Videos', '' ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"show_ui" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => false,
		"show_in_menu" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "videos", "with_front" => true ),
		"query_var" => true,
		"menu_icon" => "dashicons-media-video",		
		"supports" => array( "title", "editor", "thumbnail" ),				
	);
	register_post_type( "videos", $args );

// End of cptui_register_my_cpts_videos()
}
add_action( 'init', 'cptui_register_my_taxes_news_section' );

function cptui_register_my_taxes_news_section() {
	$labels = array(
		"name" => __( 'News Sections', '' ),
		"singular_name" => __( 'News Section', '' ),
		);

	$args = array(
		"label" => __( 'News Sections', '' ),
		"labels" => $labels,
		"public" => true,
		"hierarchical" => false,
		"label" => "News Sections",
		//"show_ui" => true,
		"query_var" => true,
		"rewrite" => array( 'slug' => 'news_section', 'with_front' => true ),
		"show_admin_column" => false,
		"show_in_rest" => false,
		"rest_base" => "",
		"show_in_quick_edit" => false,
	);
	register_taxonomy( "news_section", array( "post" ), $args );


}

add_action( 'init', 'cptui_register_my_cpts_stakeholders' );
function cptui_register_my_cpts_stakeholders() {
	$labels = array(
		"name" => __( 'Stakeholders', '' ),
		"singular_name" => __( 'Stakeholder', '' ),
		);

	$args = array(
		"label" => __( 'Stakeholders', '' ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"show_ui" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => false,
		"show_in_menu" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "stakeholders", "with_front" => true ),
		"query_var" => true,
		"menu_icon" => "dashicons-id",		
		"supports" => array( "title", "editor", "thumbnail" ),				
	);
	register_post_type( "stakeholders", $args );

}

add_action( 'init', 'cptui_register_my_cpts_webinars' );
function cptui_register_my_cpts_webinars() {
	$labels = array(
		"name" => __( 'Webinars', '' ),
		"singular_name" => __( 'Webinar', '' ),
		);

	$args = array(
		"label" => __( 'Webinars', '' ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"show_ui" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => false,
		"show_in_menu" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "webinars", "with_front" => true ),
		"query_var" => true,
		"menu_icon" => "dashicons-microphone",						
	);
	register_post_type( "webinars", $args );

}

add_action( 'init', 'cptui_register_my_cpts_supporters' );
function cptui_register_my_cpts_supporters() {
	$labels = array(
		"name" => __( 'Supporters', '' ),
		"singular_name" => __( 'Supporter', '' ),
		);

	$args = array(
		"label" => __( 'Supporters', '' ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"show_ui" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => false,
		"show_in_menu" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "supporters", "with_front" => true ),
		"query_var" => true,
		"menu_icon" => "dashicons-thumbs-up",				
		"supports" => array( "title", "editor", "thumbnail" ),				
	);
	register_post_type( "supporters", $args );

}

add_action( 'init', 'cptui_register_my_cpts_suppliers' );
function cptui_register_my_cpts_suppliers() {
	$labels = array(
		"name" => __( 'Supplier', '' ),
		"singular_name" => __( 'Supplier', '' ),
		);

	$args = array(
		"label" => __( 'Suppliers', '' ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"show_ui" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => false,
		"show_in_menu" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "supplier", "with_front" => true ),
		"query_var" => true,
		"menu_icon" => "dashicons-groups",		
		"supports" => array( "title", "editor", "thumbnail" ),				
	);
	register_post_type( "supplier", $args );

}

add_action( 'init', 'cptui_register_my_cpts_events' );
function cptui_register_my_cpts_events() {
	$labels = array(
		"name" => __( 'Events', '' ),
		"singular_name" => __( 'Event', '' ),
		);

	$args = array(
		"label" => __( 'Event', '' ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"show_ui" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => false,
		"show_in_menu" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "event", "with_front" => true ),
		"query_var" => true,
		"menu_icon" => "dashicons-awards",		
		"supports" => array( "title", "editor", "thumbnail" ),				
	);
	register_post_type( "events", $args );

}

add_action( 'init', 'cptui_register_my_cpts_publications' );
function cptui_register_my_cpts_publications() {
	$labels = array(
		"name" => __( 'Publications', '' ),
		"singular_name" => __( 'Publication', '' ),
		);

	$args = array(
		"label" => __( 'Publication', '' ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"show_ui" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => false,
		"show_in_menu" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "publication", "with_front" => true ),
		"query_var" => true,
		"menu_icon" => "dashicons-book-alt",		
		"supports" => array( "title", "editor", "thumbnail" ),				
	);
	register_post_type( "publication", $args );

}