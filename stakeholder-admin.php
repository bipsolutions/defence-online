<?php
/*
Template Name: Stakeholder Admin

*/
acf_form_head();
 get_header();

define('DONOTCACHEPAGE',1);

 $current_user = wp_get_current_user();
 ?>
 <?php if(!do_is_this_gated()): ?>
		
		<?php if(in_array_any( array('administrator', 'stakeholder'), $current_user->roles) ):?>

			<div class="level">
				
				<div class="pure-g">

					<div class="pure-u-24-24">
							<?php 
								$user_id = $current_user->ID;
								$post_id = 'user_' . $user_id;
							 ?>
							<div class="page-header">
									<h2 class="line-along"><?php the_title(); ?></h2>
									<a class="cta stakeholder-new" href="<?php echo get_the_permalink(2306);?>">Create a new stakeholder news article</a>
									
							</div>

							<p style="text-align:right">
								<?php $status = get_post_status(get_the_stakeholder_id_by_author($user_id))?>
									
								<?php if(get_the_stakeholder_id_by_author($user_id) != NULL && $status == 'publish'):?>
									<a class="cta" href="<?php echo get_the_permalink(get_the_stakeholder_id_by_author($user_id));?>">View your site</a>
								<?php else:?>
									<span class="cta" style="padding: 5px; background-color: green; width:100%; color:#FFF" href="<?php echo get_the_permalink(get_the_stakeholder_id_by_author($user_id));?>">Note: Your site is still awating moderation and will be live once approved. Please contact <a style="color:yellow" href="mailto:<?php echo get_option('admin_email')?>"><?php echo get_option('admin_email')?></a> for further information</span>
								<?php endif;?>
							</p>
							
							<?php 

							

							acf_form( array(
								'id' => 'acf-stakeholder-form',
								'field_groups' => array(3424),
								'post_id' => $post_id,
								'post_content' => false,
							)); ?>
							

							
					</div>
				
				</div>
		
			</div>

		<?php else:?>

			<div class="level">

				<div class="pure-g">

					<div class="pure-u-24-24">

							not allowed
						
					</div>
					
				</div>
			
			</div>
		
		<?php endif;?>

<?php else: ?>

	<!-- What to do if the content is gated -->
	<?php include_once('access.php'); ?>

<?php endif; ?>

<?php get_footer(); ?>