<?php
/**
 * @package WordPress
 * @subpackage HTML5-Reset-WordPress-Theme
 * @since HTML5 Reset 2.0
 */
 get_header(); ?>

<?php if(do_can_user_view_page()): ?>

 <div class="pure-g dco-content do-suppier-single">

 	<div class="pure-u-24-24">
		
		<h2 class="line-along"><?php echo strtoupper( get_the_title()	) ; ?></h2>
	
	</div>


	 <div class="pure-u-24-24 do-sidebar">

	 	<div class="pure-g grey">

	 		<div class="pure-u-24-24">
	 			
	 			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	 				
	 				<?php if($pubDescription = get_field('description')):?>

	 					<h3>Publication Description</h3>
	 					<p><?php echo $pubDescription; ?></p>

	 				<?php endif;?>

	 				<?php the_content();?>

	 				<?php if($file = get_field('direct_download_link')):?>
	 						<p><a id="pubdownload" data-user="<?php echo get_current_user_id();?>" data-document="<?php echo $post->ID; ?>" class="cta pubdownload" target="_blank" href="<?php echo $file;?>">Download</a></p>
	 					<?php endif;?>

	 				<?php if($url = get_field('external_url')):?>

						<?php $embedid = do_get_url_var($url, 'e'); ?>		
						
						<div id="embed-pub" data-configid="<?php echo $embedid; ?>" style="width:100%;" class="issuuembed"></div><script type="text/javascript" src="//e.issuu.com/embed.js" async="true"></script>
						
						<script>

							$(document).ready(function (){
								jQuery('#embed-pub').height(jQuery('#embed-pub').width() * 0.684);
							});

						</script>
						
					<?php endif; ?>
	 				

	 			<?php endwhile; endif; ?>
	 		
	 		</div>
		
		</div>
 		
	 </div>

 </div>
 <script type="text/javascript">
 	
 	

 	jQuery(document).ready(function () { 
 		
		var datax = {

			'userid' 	: <?php echo get_current_user_id();?>,
			'pubid' 	: <?php echo $post->ID; ?>

		}
 		jQuery.ajax({
				type: "POST",
				url: "<?php bloginfo('template_url');?>/process-pub-click.php",
				data: datax,
				success: function (result) {
							           
				}
	    });

		jQuery('.pubdownload').click(function(e){

			var data = {
				'userid' : jQuery(this).data('user'),
				'pubid' : jQuery(this).data('document'),
			}
			jQuery.ajax({
				 type: "POST",
				 url: "<?php bloginfo('template_url');?>/process-pub-click.php",
				 data: data,
				success: function (result) {
							           
				}
			}); 
		});
	});

 </script>

<?php else: ?>
	<!-- What to do if the content is gated -->
	<?php include_once('access.php'); ?>

<?php endif; ?>


	
<?php get_footer(); ?>