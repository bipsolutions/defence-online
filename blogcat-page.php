<?php
/*
Template Name: News by Category Page

*/
 get_header(); ?>

<?php

  $column_distubution = do_get_distrubution();
  $do_paged = isset( $_GET['do_paged']) ? $_GET['do_paged'] : 1; 

  ?>
  
 <div class="level">

 	<div class="pure-g">
 	 		
 	 		<div class="pure-u-24-24">
 	 						
 	 			<div class="page-header">
 	 				
 	 				<h2 class="line-along"><?php echo strtoupper(get_the_title()) ; ?></h2>
 	 			
 	 			</div>
 	
 	 		</div>
 	
 	 	</div>

 </div>

 <div class="level">

  <div class="pure-g dco-content do-blog">

 	<div class="pure-u-1 pure-u-md-<?php echo $column_distubution['left']?>-24">

 		<div class="padding-top padding-right">

 		<?php 
 			
 			$decision = get_field('show_category_posts_or_news_section_posts');

 			if(!$decision || $decision == 'category' ){
 				$thecategories = get_field('posts_in_this_category_to_show');
	 			$term_ids = array();
	 			
	 			foreach($thecategories as $thecategory){
	 					array_push($term_ids, $thecategory->term_id);
	 			}

				$do_the_posts = do_get_category_posts($term_ids, false, $do_paged );
				$count_posts  = do_count_category_posts($term_ids);
				
 			}

 			if($decision == 'new_section'){

 				$thesections 	= get_field('news_section_to_show_on_this_page');
 				$do_the_posts 	= do_get_section_posts($thesections, false, $do_paged );
 				$count_posts 	= do_count_section_posts($thesections);
 				
 			}

 			

 		?>

		<?php if($do_the_posts): ?>

			<?php session_start(); $_SESSION['IDS'] = array(); ?>

			<?php foreach($do_the_posts as $do_the_post): 

				array_push($_SESSION['IDS'], $do_the_post->ID);

				$thumb 		= 'single-post-feature';
				$image 		= wp_get_attachment_image_src( get_post_thumbnail_id($do_the_post->ID ), $thumb );
				$term 		= get_field('news_section',$do_the_post->ID);
				$termName 	= get_term_by('id', $term, 'news_section');
				$termName 	= $termName->name; 
				$alt_text = get_post_meta(get_post_thumbnail_id($do_the_post->ID), '_wp_attachment_image_alt', true);
			?>

		
			<div class="main-feature-container title-outside">
				
				<?php if($image):?>

					<a href="<?php echo get_the_permalink($do_the_post->ID) ?>"><img alt="<?=$alt_text; ?>" class="wow fadeInUp" src="<?php echo $image[0]?>"></a>
					
					<div class="title-container">

						<div class="padding-vertical">

							<?php do_do_post_title($do_the_post->ID, $term, $termName)?>
				
						</div>
							
					</div>

					

				<?php else:?>

					<div class="no-image-title-container">

						<?php do_do_post_title($do_the_post->ID, $term, $termName)?>
						
					</div>

				<?php endif; ?>

			</div>
			
			<p><?php echo do_get_content_extract($do_the_post->ID, get_field('wordcount_for_post_excerpt_in_news_pages', 'options'), '<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>'); ?></p>

			<?php edit_post_link(__('Edit this entry','html5reset'),'<br><br>','.'); ?>



			<p><hr></p>


			<?php endforeach; ?>

			<?php do_do_nav($do_paged, $count_posts);?>

			<?php else: ?>

				<h2><?php _e('Nothing Found','html5reset'); ?></h2>

			<?php endif;?>

	</div>
	</div>

	<div class="pure-u-1 pure-u-md-<?php echo $column_distubution['right']?>-24 do-sidebar">

	 		<?php get_do_sidebar(); ?>

	 </div>
	</div>

	</div>

<?php get_footer(); ?>
