<?php

?>

<div class="level level-footer blue hide-sm hide-xs">

  <div class="pure-g">
    
    <div class="pure-u-12-24">
        
        <div class="pure-g">
            
            <div class="pure-u-4-24 hide-lg hide-xl">

               <button id="nav-toggle-foot" class="toggle-menu menu-left "><span></span></button>

            </div>
        
        <div class="pure-u-24-24 hide-xs hide-sm hide-md">

            <?php do_do_footer_menu(); ?>
        
        </div>
                    
        <div class="pure-u-24-24" style="padding-left:10px">
          
          <footer id="footer" class="source-org vcard footerDisclaimer" role="contentinfo">

            <?php the_field('disclamer','options')?>
              
          </footer>
        
        </div>
      
      </div>
    
    </div>
           
    <div class="pure-u-12-24 center">
                
      <div class="footer-logo-container">

        <?php 

            $footlogos = get_field('footer_logos', 'options');
                      
            if($footlogos):?>

              <?php while( have_rows('footer_logos','options') ): the_row(); ?>
                          
                <?php $image = get_sub_field('image');?>

                <div class="footerLogo">

                    <h3><?php the_sub_field('title'); ?></h3>
                                  
                    <img alt="<?=$image['alt']; ?>" src="<?php echo $image['sizes']['slider-logo']; ?>">

                </div>

              <?php endwhile;?>

            <?php endif; ?>

        </div>
      
      </div>
  
    </div>
      
</div>

<div class="level level-footer blue hide-md hide-lg hide-xl">
        
  <div class="pure-g">

    <div class="pure-u-24-24 hide-lg hide-xl">

      <p class="center"> <button id="nav-toggle-foot" class="toggle-menu menu-left "><span></span></button></p>

    </div>

    <div class="pure-u-24-24 hide-xs hide-sm hide-md">

      <?php do_do_footer_menu(); ?>

    </div>

    <div class="pure-u-24-24">

      <?php 

        $footlogos = get_field('footer_logos', 'options');
        
        if($footlogos):?>

            <?php while( have_rows('footer_logos','options') ): the_row(); ?>
            
            <?php $image = get_sub_field('image');?>

                <h3 class="center"><?php the_sub_field('title'); ?></h3>
                <p class="center"><img src="<?php echo $image['sizes']['slider-logo']; ?>"></p>

            <?php endwhile;?>

        <?php endif; ?>
    
    </div>

    <div class="pure-u-24-24">
                    
      <footer id="footer" class="source-org vcard footerDisclaimer footerDisclamerResponsive" role="contentinfo">

        <?php the_field('disclamer','options')?>

      </footer>

    </div>
                    
  </div>        
      
</div>

	<?php wp_footer(); session_start(); unset($_SESSION['exclude_ids']); unset($_SESSION['validate_username']); unset($_SESSION['validate_email'])?>

  <?php do_action('do_do_footer'); ?>
  
<script src="<?php bloginfo('template_directory'); ?>/_/js/functions.js"></script>
    
</body>

</html>
