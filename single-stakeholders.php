<?php
/**
 * @package WordPress
 * @subpackage HTML5-Reset-WordPress-Theme
 * @since HTML5 Reset 2.0
 */
 get_header('stakeholder');

 $sh_id 		= 'user_' . do_get_stakeholder_id();
 $unique_id 	= uniqid();
 $companyName 	= strtoupper(get_the_title($post->ID));

 ?>

<?php if(do_can_user_view_page()): ?>

	<?php if(!is_maintenance_mode_on()):?>

		<?php if(isset($_GET['casestudy'])):?>

			<?php do_do_stakeholder_case_study_page(do_get_stakeholder_id(), $companyName); ?>

		<?php endif; ?>

		<?php if(isset($_GET['photos'])):?>

			<?php do_do_stakeholder_photos($sh_id,  $companyName); ?>

		<?php endif;?>

		<?php if(isset($_GET['services'])):?>

			<?php do_do_stakeholder_services($sh_id,  $companyName); ?>

		<?php endif?>

		<?php if(isset($_GET['videos'])):?>

			<?php do_do_stakeholder_videos($sh_id, $companyName); ?>

		<?php endif?>

		<?php if(isset($_GET['ebooks'])):?>

			<?php do_do_stakeholder_ebooks(do_get_stakeholder_id(), $companyName); ?>

		<?php endif?>

		<?php if(count($_GET) == 0):?>
			
			<?php do_do_stakeholder_about($sh_id, $companyName); ?>

			<?php do_do_stakeholder_services($sh_id, $companyName); ?>

			<?php do_do_stakeholder_latest_news($companyName); ?>

			<?php do_do_stakeholder_case_study_list(do_get_stakeholder_id(), $companyName) ?>

			<?php do_do_stakeholder_ebooks(do_get_stakeholder_id(), $companyName); ?>

			<?php do_do_stakeholder_photos($sh_id, $companyName); ?>

			<?php do_do_stakeholder_videos($sh_id, $companyName); ?>

		<?php endif;?>
	
	<?php else:?>

		<div class="level">

			<div class="pure-g">

				<div class="pure-u-24-24">
					
					<h2 style="margin-top: 30px">This page is currently under maintenance. Please check back soon</h2>
				
				</div>
			
			</div>

		</div>

	<?php endif; ?>

<?php else: ?>
	<!-- What to do if the content is gated -->
	<?php include_once('access.php'); ?>

<?php endif; ?>
	
<?php get_footer('stakeholder'); ?>

