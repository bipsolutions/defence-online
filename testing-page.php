<?php
/*
Template Name: Front Page

*/
 get_header(); ?>



	<div class="level level-mod-announcements level-line-along">

		<div class="pure-g dco-content">

			<div class="pure-u-24-24">
				<h2 class="line-along">MOD ANNOUNCEMENTS</h2>
			</div>

			<div class="pure-u-6-24 sub-feature">
				<div class="padding">
					<img src="<?php bloginfo('template_url')?>/images/sub-feature-holding.jpg"/>
					<p class="posted">11 days ago</p>
					<p><a class="sub-feature-category" href="category">SEA - </a><a href="title" class="sub-feature-title">CIOB backs military recruitment initiative <a class="sub-feature-more" href="#"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a></p>
					
				</div>
			</div>
			<div class="pure-u-6-24 sub-feature">
				<div class="padding">
					<img src="<?php bloginfo('template_url')?>/images/sub-feature-holding.jpg"/>
					<p class="posted">11 days ago</p>
					<p><a class="sub-feature-category" href="category">SEA - </a><a href="title" class="sub-feature-title">CIOB backs military recruitment initiative <a class="sub-feature-more" href="#"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a></p>
					
				</div>
			</div>
			<div class="pure-u-6-24 sub-feature">
				<div class="padding">
					<img src="<?php bloginfo('template_url')?>/images/sub-feature-holding.jpg"/>
					<p class="posted">11 days ago</p>
					<p><a class="sub-feature-category" href="category">SEA - </a><a href="title" class="sub-feature-title">CIOB backs military recruitment initiative <a class="sub-feature-more" href="#"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a></p>
					
				</div>
			</div>
			<div class="pure-u-6-24 sub-feature logo-slider-container">
					<div class="pure-g">
							<div class="pure-u-24-24 center"><h4>Supported by</h4></div>
							<div class="pure-u-4-24 center nav-column">
								<i class="fa fa-arrow-circle-left fa-2x fa-nav click-left" aria-hidden="true"></i>
							</div>
							<div class="pure-u-16-24">
									<div class="logo-slider unislide center">
										<ul>
											<li>
												<div class="ls-img-container">
													<img src="<?php bloginfo('template_url');?>/images/supporedbyLogo.jpg">
												</div>
											</li>
											<li>
												<div class="ls-img-container">
													<img src="<?php bloginfo('template_url');?>/images/supporedbyLogo.jpg">
												</div>
											</li>
											<li>
												<div class="ls-img-container">
													<img src="<?php bloginfo('template_url');?>/images/supporedbyLogo.jpg">
												</div>
											</li>
										</ul>
									</div>
							</div>
							<div class="pure-u-4-24 center nav-column">
								<i class="fa fa-arrow-circle-right fa-2x fa-nav click-right" aria-hidden="true"></i>
							</div>
							<div class="pure-u-24-24 center">
								<a href="#" class="cta">Become a supporter</a>
							</div>
						
					</div>

			</div>
			

		
		</div>
	</div>

	<div class="level level-main-feature level-line-along">
		<div class="pure-g dco-content">

			
			<div class="pure-u-24-24">
				<h2 class="line-along">FEATURE</h2>
			</div>
			
		</div>
		<div class="pure-g dco-content">

			<div class="pure-u-16-24 feature-level-content">
					<div>
						<div class="main-feature-container dco-content">
								<div class="overlay">
									<div class="padding-double">
										<span class="category"><a href="category">SEA</a></span>
										<span class="posted">11 days ago</span>
										<h3><a href="#">Offshore wind sector to benefit from £1.5M <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a></h3>
									</div>
								</div>
							<img src="<?php bloginfo('template_url')?>/images/main-feature-holder.jpg">

						</div>
					</div>
			</div>

			<div class="pure-u-8-24">
					<div class="padding-left">
						<div class="grey feature-level-content">
							<div class="padding">
								
								<h3><i class="fa fa-trophy" aria-hidden="true"></i> Contract Awards</h3>
								<div class="contract-award-container">
									<h4>Someone One an Award</h4>
									<p>CIOB backs military recruitment initiative. CIOB backs military recruitment <a href="#"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a></p>
								</div>

								<div class="contract-award-container">
									<h4>Someone One an Award</h4>
									<p>CIOB backs military recruitment initiative. CIOB backs military recruitment <a href="#"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a></p>
								</div>
								<div class="contract-award-container">
									<h4>Someone One an Award</h4>
									<p>CIOB backs military recruitment initiative. CIOB backs military recruitment <a href="#"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a></p>
								</div>
								<div class="contract-award-container">
									<h4>Someone One an Award</h4>
									<p>CIOB backs military recruitment initiative. CIOB backs military recruitment <a href="#"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a></p>
								</div>

								<p>
										<a href="#" class="cta">VIEW ALL</a>
								</p>	



							</div>
						</div>
					</div>
			</div>

		</div>

	</div>

	<div class="level level-line-along">
		<div class="pure-g dco-content">

			
			<div class="pure-u-24-24">
				<h2 class="line-along">VIDEOS AND WEBINARS</h2>
			</div>
			
		</div>
		<div class="pure-g dco-content">

			<div class="pure-u-8-24">
					<div class="padding-right">
						<div class="grey webinar-column">
							<div class="padding">
								<h3><i class="fa fa-headphones" aria-hidden="true"></i> Webinar Timetable</h3>

								<div class="pure-g webinar-row">
									
											<div class="pure-u-8-24">
												16 July 4:30pm
											</div>
											<div class="pure-u-14-24">
												Brexit Plans
											</div>
											<div class="pure-u-2-24">	
												<i class="fa fa-play-circle" aria-hidden="true"></i>
											</div>
									
								</div>

								<div class="pure-g webinar-row">
									
											<div class="pure-u-8-24">
												16 July 4:30pm
											</div>
											<div class="pure-u-14-24">
												Brexit Plans
											</div>
											<div class="pure-u-2-24">	
												<i class="fa fa-play-circle" aria-hidden="true"></i>
											</div>
									
								</div>

								<div class="pure-g webinar-row">
									
											<div class="pure-u-8-24">
												16 July 4:30pm
											</div>
											<div class="pure-u-14-24">
												Brexit Plans
											</div>
											<div class="pure-u-2-24">	
												<i class="fa fa-play-circle" aria-hidden="true"></i>
											</div>
									
								</div>

								<div class="pure-g webinar-row">
									
											<div class="pure-u-8-24">
												16 July 4:30pm
											</div>
											<div class="pure-u-14-24">
												Brexit Plans
											</div>
											<div class="pure-u-2-24">	
												<i class="fa fa-play-circle" aria-hidden="true"></i>
											</div>
									
								</div>

								<div class="pure-g webinar-row">
									
											<div class="pure-u-8-24">
												16 July 4:30pm
											</div>
											<div class="pure-u-14-24">
												Brexit Plans
											</div>
											<div class="pure-u-2-24">	
												<i class="fa fa-play-circle" aria-hidden="true"></i>
											</div>
									
								</div>

								<div class="pure-g webinar-row">
									
											<div class="pure-u-8-24">
												16 July 4:30pm
											</div>
											<div class="pure-u-14-24">
												Brexit Plans
											</div>
											<div class="pure-u-2-24">	
												<i class="fa fa-play-circle" aria-hidden="true"></i>
											</div>
									
								</div>

								<div class="pure-g webinar-row">
									
											<div class="pure-u-8-24">
												16 July 4:30pm
											</div>
											<div class="pure-u-14-24">
												Brexit Plans
											</div>
											<div class="pure-u-2-24">	
												<i class="fa fa-play-circle" aria-hidden="true"></i>
											</div>
									
								</div>

								<div class="pure-g webinar-row">
									
											<div class="pure-u-8-24">
												16 July 4:30pm
											</div>
											<div class="pure-u-14-24">
												Brexit Plans
											</div>
											<div class="pure-u-2-24">	
												<i class="fa fa-play-circle" aria-hidden="true"></i>
											</div>
									
								</div>
								
								
								

								<p>
										<a href="#" class="cta">GET FULL ACCESS</a>
								</p>	



							</div>
						</div>
					</div>
			</div>

			<div class="pure-u-16-24">
					<div>
						<div class="video-container dco-content webinar-column">
								
							<?php echo do_shortcode('[fve]https://vimeo.com/172400601[/fve]'); ?>

						</div>
					</div>
			</div>

			

		</div>

	</div>

		<div class="level level-latest-stuff red">
				<div class="pure-g dco-content">
						<div class="pure-u-5-24">
							<h3 class="padding-bottom">Latest Suppliers</h3>
							<div class="latest-container">
								
							</div>
							<a href="#" class="cta-full margin-vertical">VIEW ALL SUPPLIERS</a>
						</div>
						<div class="pure-u-1-24">
							
						</div>
						<div class="pure-u-5-24">
							<h3 class="padding-bottom">Upcoming Events</h3>
							<div class="latest-container">
								
							</div>
							<a href="#" class="cta-full margin-vertical">REGISTER YOUR INTEREST</a>
						</div>
						<div class="pure-u-1-24">
							
						</div>
						<div class="pure-u-5-24">
							<h3 class="padding-bottom">Latest Ebook</h3>
							<div class="latest-container">
								
							</div>
							<a href="#" class="cta-full margin-vertical">DOWNLOAD EBOOK</a>
						</div>
						<div class="pure-u-1-24">
							
						</div>
						<div class="pure-u-6-24">
							<h3 class="padding-bottom">Latest Tweets</h3>
							<div class="latest-container">
								
							</div>
						</div>
				</div>
		</div>
	

<?php get_footer(); ?>
