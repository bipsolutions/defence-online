<?php


require_once ('do_walker.php');
require_once ('functions/custom-posts-tax.php');
require_once ('functions/theme-setup.php');
require_once ('functions/helpers.php');
require_once ('functions/shortcodes.php');
require_once ('functions/stakeholders.php');

add_action( 'wp', 'do_setup_schedule' );

// Set up an hourly cron job
function do_setup_schedule() {
    if (! wp_next_scheduled ( 'my_hourly_event' )) {
	wp_schedule_event(time(), 'hourly', 'my_hourly_event');
    }
}



add_action('my_hourly_event', 'do_do_this_hourly');

// What to do during the hourly cron job
function do_do_this_hourly() {
	
	add_dco_users();

}

// This function checks an only CSV file which lists users how have 'recently' registered with DCO and have
// checked the box to also regsiter with defence online. This function will add the users to wordpress
// automatically.
function add_dco_users(){

	$file = 'http://10.2.150.113/defenceOnline/defenceOnlineLastTwoHours.csv';
	$csv = array_map('str_getcsv', file($file));
    array_walk($csv, function(&$a) use ($csv) {
      $a = array_combine($csv[0], $a);
    });
    array_shift($csv);
 
    foreach ($csv as $newUser) {

    	global $wpdb;
    	// Check if the user has already been added.
    	$results = $wpdb->get_results( "select * from wp_usermeta where meta_value ='". $newUser['ID'] ."'", OBJECT );

    	// If user is still to be added, then go ahead and add them 
    	if(!$results){
    		
    		$userdata = array(
			    'user_login'  =>  $newUser['EMAIL'],
			    'user_email'  =>  $newUser['EMAIL'],
			    'user_pass'   =>  NULL,  // When creating an user, `user_pass` is expected.
			    'first_name'  =>  $newUser['FIRST_NAME'],
			    'last_name'	  =>  $newUser['LAST_NAME'],
			);

    		$user_id = wp_insert_user( $userdata );

    		//If user was added then add some user meta data for shits n giggles.
    		if ( ! is_wp_error( $user_id ) ) {
    			
	    		update_field('industry_sector', $newUser['INDUSTRY_SECTOR'], 'user_' . $user_id);
	    		update_field('company_name', $newUser['GROUPNAME'], 'user_' . $user_id);
	    		update_field('company_turnover', $newUser['TURNOVER'], 'user_' . $user_id);
	    		update_field('number_of_emplyees', $newUser['EMPLOYEES'], 'user_' . $user_id);
	    		update_field('title', $newUser['TITLE'], 'user_' . $user_id);

	    		add_user_meta( $user_id, 'batch_added_user', 1 );
	    		add_user_meta( $user_id, 'CRM_ID', $newUser['CRM_ID'] );
	    		add_user_meta( $user_id, 'DCO_ID', $newUser['ID'] );

	    		//Custom function in themes function.php
    			wp_batch_user_notification($user_id, NULL, 'both');

			   
			}
    		
    	}
    }

}


function do_do_maps($post_ids, $fieldname){

	if(!$post_ids || !is_array($post_ids)){
		return;
	}
	$point_of_interest = array();

	$mymap = new Mappress_Map(array("width" => '100%'));

	foreach ($post_ids as $post_id) {

			$location = get_field($fieldname, $post_id);

			if($location){
				$poi = new Mappress_Poi(
						array(
							"title" => "<a href='".get_the_permalink($post_id)."'>".get_the_title($post_id)."</a>", 
							"body" 	=> $location['address']. "<br>" . get_field('date',$post_id),
							"point" => array(
										"lat" => $location['lat'],
										"lng" => $location['lng']
										)
							)
				);

				array_push($point_of_interest, $poi);
				
			}
			
	}

	$mymap->pois = $point_of_interest;
	
	echo $mymap->display(array("directions"=>"none"));

}

// Function to get information on the main site logo (as assigned in Site Options).
function dco_main_logo_image_info(){

	$image_array = get_field('site_logo', 'options');
			
	$info = array(
		'url'		=> $image_array['sizes']['site-logo'],
		'height' 	=> $image_array['sizes']['site-logo-height'],
		'width' 	=> $image_array['sizes']['site-logo-width'],
	);

	return $info;

};

// Function to get informnation on the logo uused on dark backgrounds (as assigned in Site Options)
function dco_main_dark_logo_image_info(){

	$image_array = get_field('do_logo_for_dark_backgrounds', 'options');
			
	$info = array(
		'url' 		=> $image_array['sizes']['site-logo'],
		'height' 	=> $image_array['sizes']['site-logo-height'],
		'width' 	=> $image_array['sizes']['site-logo-width'],
	);

	return $info;

};

// Function to set dynamic inline CSS for the logos.
function site_logo_css(){

	$logo_info 		= dco_main_logo_image_info();
	$dark_logo_info = dco_main_dark_logo_image_info();

	?>

	<style>
		
		#main-logo-container h1 a{
			  
			height: <?php echo $logo_info['height'] ?>px;
			background-image:url(<?php echo $logo_info['url']; ?>);
 
		}
		.stakeholder-do-logo #main-logo-container h1 a{
			  
			height: <?php echo $dark_logo_info['height'] ?>px;
			background-image:url(<?php echo $dark_logo_info['url']; ?>);
 
		}

		<?php if($bg = get_field('background_image','options')):?>
			body{
				background-image:url(<?php echo $bg; ?>);
				background-size: contain;
				background-repeat: no-repeat;
			}
		<?php endif;?>

	</style>

	<?php

}

add_filter(	'wp_head', 'site_logo_css' );

// Function to stop a page from being cashed.
function do_kill_session(){

	define('DONOTCACHEPAGE',1);

	return;
}
// Function to add the required markup for the postcode finder on the registration form.
function add_postcode_finder_js(){

	global $post;

	// Only do the following if this is the regration page (as defined in Jouney Options)
	if($post->ID == get_field('registration_page', 'do-journey')):

		add_action('do_do_footer', 'do_kill_session');

		?>

			

			<script>
					// Fire code when the post-modal opens
					jQuery(document).on('opening', '.postcode-modal', function () {

					function iOS() {

						  var iDevices = [
						    'iPad Simulator',
						    'iPhone Simulator',
						    'iPod Simulator',
						    'iPad',
						    'iPhone',
						    'iPod'
						  ];

						  if (!!navigator.platform) {
						    while (iDevices.length) {
						      if (navigator.platform === iDevices.pop()){ return true; }
						    }
						  }

						  return false;
						}
					  var data = {
					  		// Get the post in the postcode field on the form
							'postcode': jQuery('#acf-field_57a04e202741e').val()
					  };

					  // Post the data to postcode.php (which is contains code to an API that we have a licenece to to looup postode information and return some results.)
					  jQuery.ajax({
                             url: '<?php echo get_template_directory_uri(); ?>/postcode.php',
                             type: 'post',
                             data: data,
                             success: function(data) {
                             		// Do stuff with the json we get back from postcode.php
                                    var cleandata = JSON.parse(data);

                                    // If there are reults do select box markup
                                    if(typeof cleandata.Item !== "undefined"){
                                    		if(iOS()){
                                    		var htmlmj = '<select id="postcodeSelect" style="width:100%;">';
                                    	}else{
                                    		var htmlmj = '<select id="postcodeSelect" multiple style="width:100%; height: 200px">';
                                    	}
                                    	// Prepopulate select box with the results
                                    		for(var key in cleandata.Item){

                                    			var listValue = cleandata.Item[key].List.replace(cleandata.Item[key].Postcode, "");

                                    			htmlmj = htmlmj + '<option  value="'+ cleandata.Item[key].Key + '">' + listValue;

                                    		}
                                    		htmlmj  = htmlmj + '</select>';
                                    		//Hide the preloader icon once the markup has been generated.
                                    		jQuery('#postcodeLoading').hide();
                                    		// Add the generated markup to the dom.
                                    		jQuery('#selectPostCodeForm').prepend(htmlmj);

                                    }else{

                                    		// If no results just do some basic signposting yo.
                                    		var htmlmj = '<p class="postcodeerror">No results found for this postcode</p>';
                                    		jQuery('#postcodeLoading').hide();
                                   			jQuery('#selectPostCodeForm').prepend(htmlmj);
                                    		
                                    }
                                    
                             }
                          
                       });
					  	// THis is the fnction that runs once the user has slected the house number and presses submit.
					  jQuery('#selectPostCodeForm').submit(function(event){
							event.preventDefault();
							
							var data = {
								'selected' : jQuery('#postcodeSelect').val()
							}
							// Post the selected value to postcode.php and it will return just one result.
							jQuery.ajax({
                            		url: '<?php echo get_template_directory_uri(); ?>/postcode.php',
                             		type: 'post',
                            		data: data,
                             		success: function(data) {
                             				// Do stuff with the result.
                                    		var cleandata = JSON.parse(data);
                                    		console.log(cleandata);
                                    		//Close the modal windo
                                    		jQuery('.remodal-close.postcodeClose').trigger('click');
                                			
                                			// Remove the selectbox and postcode error  from the dom if they exist.
                                			// This is so that if the post code modal is loaded more than
                                			// once in the same page there will not be multiple select boxes
                                			// added to the dom
                                			jQuery('#postcodeSelect').remove();
                                			jQuery('.postcodeerror').remove();
                                			
                                			// Re add the preloader icon incase the modal is opened again
                                			jQuery('#postcodeLoading').show();

                                			// Populate the form fields with the returned data
                                			jQuery('#acf-field_579f3f706febf').val(cleandata.Item.Street);
                                			jQuery('#acf-field_579f3fc96fec1').val(cleandata.Item.Town);
                                			jQuery('#acf-field_579f400d6fec2').val(cleandata.Item.Authority);
                                			jQuery('#acf-field_57a04e202741e').val(cleandata.Item.Postcode);

                            		}
							
							
							});

						});

					});

					
					jQuery(document).ready(function (){
						// Add the postode button onto the page
						// Need to do this via jQuery as I am using acf_form to generate the form.
						jQuery('#post_code').after('<div id="postcodebutton" class="acf-field acf-field-text"><div class="acf-label"><label><i id="postcodetooltip" class="fa fa-info-circle" aria-hidden="true"></i></label></div><div class="acf-input"><a data-remodal-target="postcodeTheButton" class="cta">Postcode Search</a></div></div>');

						// Add the modal to the markup as hidden so it can be shown when
						// the 'find postcode' button is pressed
						jQuery('.level-nav-bar').after('<div class="remodal postcode-modal" data-remodal-id="postcodeTheButton" data-remodal-options="hashTracking: false, closeOnOutsideClick: true" ><div class="pure-g"><div class="pure-u-1"><div class="padding"><div class="grey-container"><p id="postcodeLoading" style="text-align:center"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><span class="sr-only">Loading...</span></p><button data-remodal-action="close" class="remodal-close postcodeClose"></button><form id="selectPostCodeForm"><input type="submit" value="Select Address" id="addressPicker" class="cta" style="float:right"></form></div></div></div></div></div>');

						// Add a tooltip using the tooltipster API to the username field
						jQuery('.acf-field-579f427b6fec9 label').append(' <span id="usernametooltip"><i class="fa fa-info-circle" aria-hidden="true"></i></span>');

						var usernametooltipContent = 'Your username is a nickname that will identify your Defence Online account and any contributions. Your username must be unique.';

						var Industrytooltip = jQuery('#usernametooltip').tooltipster({
   									 
   									 contentAsHTML : true,
   									 content: usernametooltipContent,
   									 distance: 30,
   									 maxWidth : 400
								});

						// Add a tooltip using the tooltipster API to the public/private sector field
						jQuery('.acf-field-579f413e6fec7 label').append(' <span id="Industrytooltip"><i class="fa fa-info-circle" aria-hidden="true"></i></span>');
						
						 	var inst = jQuery('[data-remodal-id=postcodeTheButton]').remodal();

						 	tooltipContent = '<p><strong>Public Sector</strong> - If the organisation you work for is directly or indirectly funded by government e.g. councils, NHS, police, fire, MoD, housing associations central government etc. This includes non-governmental and non-profit-making third sector organisations like registered charities, social enterprise or voluntary groups</p><p><strong>Private Sector</strong> – If the organisation you work for is profit making and not funded by government for example limited company, Plc, sole trader etc. </p>';

						 	postcodetooltipContent = 'If you live outside the UK, please manually add your address details';

						 	var Industrytooltip = jQuery('#Industrytooltip').tooltipster({
   									 
   									 contentAsHTML : true,
   									 content: tooltipContent,
   									 distance: 30,
   									 maxWidth : 400
								});

						 	var Postcodetooltip = jQuery('#postcodetooltip').tooltipster({
   									 
   									 contentAsHTML : true,
   									 content: postcodetooltipContent,
   									 distance: 30,
   									 maxWidth : 400
								});

						 	jQuery(document).on('closing', '.postcode-modal', function (e) {

						 			jQuery('#postcodeSelect').remove();
						 			jQuery('#postcodeLoading').show();
						 			jQuery('.postcodeerror').remove();
	 								 
							});

					});

			</script>


	<?php endif;

}

add_filter('wp_head','add_postcode_finder_js');

// Function to send a custom email as defined in Journey Options to the user when they have updated
// thier password.
//
// For this the shortcode registered in functions/shortcodes.php has to have the user_id parameter added.
// This action we are using gives us access to the $user object that the email is being sent to so I then
// use a find and replace on the shortcode string to add the parameter.
function do_do_password_reset_confirmation($user){

	$user_id = $user->ID;

	$message = get_field('after_password_set_email_content', 'do-journey');

    $message = str_replace('[do_firstname]', '[do_firstname user_id="'. $user_id .'"]', $message);
    $message = str_replace('[do_surname]', '[do_surname user_id="'. $user_id .'"]', $message);
    $message = str_replace('[do_username]', '[do_username user_id="'. $user_id .'"]', $message);
    $message = str_replace('[do_password_link]', '[do_password_link user_id="'. $user_id .'"]', $message);
    $message = str_replace('[do_email]', '[do_email user_id="'. $user_id .'"]', $message);

    $message_final = do_shortcode($message);

    $email_subject = get_field('after_password_set_email_subject','do-journey');

	wp_mail($user->user_email, $email_subject, $message_final);
	
}

add_action('after_password_reset', 'do_do_password_reset_confirmation');


// Get a selection of random posts and make sure none of the posts belong to a syndicated feed.
function do_get_random_posts($limit = null){

	if(!$limit){
		$limit = -1;
	}

	// Get all the syndicated feeds post
	$feedposts				= do_post_by_custom_post('fp_feed');
	$final_cats_to_exclude	= array();

	if($feedposts){
		// Look at all the syndicated feed posts and collect the IDs ofwhat category they belong to
		foreach ($feedposts	 as $feedpost) {

			$cats_to_exclude = get_post_meta($feedpost->ID, 'fp_new_post_categories');

			foreach ($cats_to_exclude as $cat_to_exclude) {
				array_push($final_cats_to_exclude, $cat_to_exclude);
			}

		}
	}
	
	// Get random posts and ensure that none of the posts belong to a syndicated category
	$args = array(
			'posts_per_page'   => $limit,
			'orderby'		   => 'rand',
			'category__not_in' => $final_cats
	);


	$posts_array = get_posts( $args );

	return $posts_array;


}

// Get the number of posts in a specific category.
function do_count_category_posts($category_id){

	global $post;

	$args = array(
		'posts_per_page'   => -1,
		'category'         => $category_id,
		'exclude'          => array($post->ID),

	);

	return count(get_posts( $args ));
	
}

// Get all the pages to be used in the footer nav.
// In Pages post type there is a checkbox 'Show in footer navigation'
// This function returns all the posts where this checkox has been checked.
function do_get_footer_pages(){

	$footer_pages = get_posts(array(
	'numberposts'	=> -1,
	'post_type'		=> 'page',
	'meta_query'	=> array(
		array(
			// The database stores acf checkboxes as serialised array like "a:1:{i:0;s:3:"yes";}"
			// I'm just running a query on for values containing "yes" in its string
			'key'	 	=> 'show_in_footer_navigation',
			'value'	  	=> 'yes',
			'compare' 	=> 'LIKE',
		)
	)
	));

	return $footer_pages;

}

// Do the markup for the footer navigation bar.
function do_do_footer_menu(){
	
	$footer_pages = do_get_footer_pages();

?>
	<?php if($footer_pages): ?>
		<nav id="nav" role="navigation">

	              <ul class="sf-menu">

	                <?php foreach($footer_pages as $footer_page):?>
	                	
	                	 <li class="page_item">

	                	 	<a href="<?php echo get_the_permalink($footer_page->ID)?>"><?php echo $footer_page->post_title; ?></a>
	                	 	
	                	 </li>
	                <?php endforeach;?>

	                	<li>
	                		<div class="pure-u-4-24 social">
								<a href="<?php the_field('twitter_url','options');?>" class="fa-stack">
										<i class="fa fa-circle fa-stack-2x" aria-hidden="true"></i>
										<i class="fa fa-twitter fa-stack-1x fa-inverse"></i>
								</a>
							</div>
	                	</li>
	                	<li>
	                		<div class="pure-u-4-24 social">
								<a href="<?php the_field('linkedin_url','options');?>" class="fa-stack">
										<i class="fa fa-circle fa-stack-2x" aria-hidden="true"></i>
										<i class="fa fa-linkedin fa-stack-1x fa-inverse"></i>
								</a>
							</div>

	                	</li>
	                	<li>
	                		<div class="pure-u-4-24 social">
								<a href="<?php the_field('media_url','options');?>" class="fa-stack">
										<i class="fa fa-circle fa-stack-2x" aria-hidden="true"></i>
										<i class="fa fa-video-camera fa-stack-1x fa-inverse"></i>
								</a>
							</div>

	                	</li>
	                	<li>
	                		<div class="pure-u-4-24 social">
								<a href="<?php the_field('facebook_url','options');?>" class="fa-stack">
										<i class="fa fa-circle fa-stack-2x" aria-hidden="true"></i>
										<i class="fa fa-facebook fa-stack-1x fa-inverse"></i>
								</a>
							</div>

	                	</li>
	
	              </ul>

	    </nav>
 	<?php endif;

}

// Get the number of posts that belong to the main new sections on the site i.e Air, Sea, Land, Homeland
function do_count_section_posts($term_ids){

	global $post;

	$args = array(

		'posts_per_page'   => -1,
		'exclude'   => array($post->ID),
		'tax_query' => array(
            array(
                'taxonomy' => 'news_section',
                'field' => 'term_id',
                'terms' => $term_ids,
            )
        )


	);

	return count(get_posts( $args ));
	
}

// Get a desired amount of the latest posts of a specific post type.
function do_get_latest_posts($posttype, $limit = null){

	global $post;

	if(!$limit){
		$limit = -1;
	}

	$args = array(

		'posts_per_page'   => $limit,
		'post_type'        => $posttype,
		// Exclude the post that is currently the global post (the one being viewed)
		'exclude'          => array($post->ID),
		'orderby'          => 'date',
		'order'            => 'DESC',
	);

	$posts_array = get_posts( $args );

	return $posts_array;

}



// Get a post type with the option to order by title.
//
// Note: $paged is depreciated.
function do_post_by_custom_post($posttype, $limit = null, $paged = null, $alpha = null){

	global $post;


	if(!$limit){
		$limit = get_option('posts_per_page');
	}
	$args = array(

		'posts_per_page'   => $limit,
		'post_type'        => $posttype,
		// Exclude the post that is currently the global post (the one being viewed)
		'exclude'          => array($post->ID),

	);

	if($alpha){
		$args['orderby'] = 'title';
		$args['order']   = 'ASC';
	}

	$posts_array = get_posts( $args );

	return $posts_array;

}

// Get the post that belong to a specific category ID
function do_get_category_posts($category_id, $limit = null, $paged = null){

	global $post;

	// For every post that is displayed on a sidebar we have to ensure that the post
	// hasnt already been used somewhere on the page.  I am controlling this with the use of
	// the super-global Session variable. For every post that is diplayed we add the post ID
	// to the the session variable 'exclude_ids' then when running a query we always make sure to
	// exclude the post IDs that are currently in the session variable.
	session_start();
	// If the session key doesnt exsist then make it exsist
	if(!is_array($_SESSION['exclude_ids'])){
		$_SESSION['exclude_ids'] = array();
	}
	// Add the current post to the exclude array
	array_push($_SESSION['exclude_ids'], $post->ID);

	$exclude_these = $_SESSION['exclude_ids'];

	$stakeholder_posts_to_exclude = do_get_all_posts_by_stakeholders('draft');
	
	foreach ($stakeholder_posts_to_exclude as $stakeholder_post_to_exclude) {
		array_push($exclude_these, $stakeholder_post_to_exclude->ID);
	}
	
	if(!$limit){
		$limit = get_option('posts_per_page');
	}
	$args = array(

		'posts_per_page'   => $limit,
		'category'         => $category_id,
		// Exclude all previous posts on the page from the current query
		'exclude'          => $exclude_these,
		'post_status'	   => 'publish',

	);
	// Do the pagination if it has been defined
	if($paged){
		// If paged exsists override the $limit param
		$posts_per_page 		= get_option('posts_per_page');
		$args['posts_per_page'] = $posts_per_page;
		$args['offset'] 		= ($paged - 1) * $posts_per_page;

	}

	$posts_array = get_posts( $args );

	return $posts_array;
}

// Get the post that belong to a specific 'new_section' taxonomy ID
function do_get_section_posts($term_ids, $limit = null, $paged = null, $stakeholder = null){

	global $post;

	// For every post that is displayed on a sidebar we have to ensure that the post
	// hasnt already been used somewhere on the page.  I am controlling this with the use of
	// the super-global Session variable. For every post that is diplayed we add the post ID
	// to the the session variable 'exclude_ids' then when running a query we always make sure to
	// exclude the post IDs that are currently in the session variable.
	session_start();
	// If the session key doesnt exsist then make it exsist
	if(!is_array($_SESSION['exclude_ids'])){
			$_SESSION['exclude_ids'] = array();
	}
	// Add the current post to the exclude array
	array_push($_SESSION['exclude_ids'], $post->ID);

	if(!$limit){
		$limit = get_option('posts_per_page');
	}
	$args = array(

		'posts_per_page'   => $limit,
		// Exclude all previous posts on the page from the current query
		'exclude'          => $_SESSION['exclude_ids'],
		'tax_query' => array(
            array(
                'taxonomy' => 'news_section',
                'field' => 'term_id',
                'terms' => $term_ids,
            )
        )
        
	);
    // If $stakeholder param is true then exclude the $stakeholder category ID.
    if (!$stakeholder || $stakeholder == null) {
        $args['cat'] = '-15';
    }
    // Do the pagination if it has been defined
	if($paged){
		// If paged exsists override the $limit param
		$posts_per_page 		= get_option('posts_per_page');
		$args['posts_per_page'] = $posts_per_page;
		$args['offset'] 		= ($paged - 1) * $posts_per_page;

	}

	$posts_array = get_posts( $args );

	return $posts_array;
}

// Get the post that belong have a desired set of tags. (Note $term_ids is an array of ids)
function do_get_tags_posts($term_ids, $limit = null){

	global $post;

	if(!$limit){
		$limit = -1;
	}
	$args = array(
			'posts_per_page'   => $limit,
			'tax_query' => array(
				array(
					'taxonomy' => 'post_tag',
					'field' => 'term_id',
					'terms' => $term_ids
				)
			),
			'exclude'          => array($post->ID),
	);
	$posts_array = get_posts( $args );

	return $posts_array;
}

// Get the number days since a post was published.
function do_get_days_since_post($post_id){

	$date 		= get_post_field('post_date', $post_id);

	$now 		= time();
    $datediff 	= $now - strtotime($date);

    return floor($datediff/(60*60*24));

}

// Posts (news articles) in this site at gated (require login to view) if the post is 
// older than a specific number of days (set in Jouney Options)
function do_is_this_gated($post_id = null){
	
	if(!$post_id){
		global $post;
		$post_id = $post->ID;
	}
	
	$postwas 		= do_get_days_since_post($post_id);
	$gated_option 	= get_field('post_restricted_time','do-journey');

	// If the days since pot are greater than the desired gated time (and the user isn't logged in)
	// then this is a gated page (return true).
	if($postwas >= $gated_option){
		if(!is_user_logged_in()){
			return true;
		}
		
	}

	return false;

}

// Display a comma deliminated list of the regions a supplier is assosiated to
function do_get_supplier_region_markup($post_id){

		$cat_array = get_field('region', $post_id);

		$html = "";

		if($cat_array){
			foreach ($cat_array as $cat) {
				$html .= $cat . ", ";
			}
		}
		

		return rtrim($html, ',');

}

// Return an object of results search post titles on the $GET results of a de key
function get_posts_by_search($post_type, $get_param_key){

	global $wpdb;

	$commonWords = array('a','able','about','above','abroad','according','accordingly','across','actually','adj','after','afterwards','again','against','ago','ahead','ain\'t','all','allow','allows','almost','alone','along','alongside','already','also','although','always','am','amid','amidst','among','amongst','an','and','another','any','anybody','anyhow','anyone','anything','anyway','anyways','anywhere','apart','appear','appreciate','appropriate','are','aren\'t','around','as','a\'s','aside','ask','asking','associated','at','available','away','awfully','b','back','backward','backwards','be','became','because','become','becomes','becoming','been','before','beforehand','begin','behind','being','believe','below','beside','besides','best','better','between','beyond','both','brief','but','by','c','came','can','cannot','cant','can\'t','caption','cause','causes','certain','certainly','changes','clearly','c\'mon','co','co.','com','come','comes','concerning','consequently','consider','considering','contain','containing','contains','corresponding','could','couldn\'t','course','c\'s','currently','d','dare','daren\'t','definitely','described','despite','did','didn\'t','different','directly','do','does','doesn\'t','doing','done','don\'t','down','downwards','during','e','each','edu','eg','eight','eighty','either','else','elsewhere','end','ending','enough','entirely','especially','et','etc','even','ever','evermore','every','everybody','everyone','everything','everywhere','ex','exactly','example','except','f','fairly','far','farther','few','fewer','fifth','first','five','followed','following','follows','for','forever','former','formerly','forth','forward','found','four','from','further','furthermore','g','get','gets','getting','given','gives','go','goes','going','gone','got','gotten','greetings','h','had','hadn\'t','half','happens','hardly','has','hasn\'t','have','haven\'t','having','he','he\'d','he\'ll','hello','help','hence','her','here','hereafter','hereby','herein','here\'s','hereupon','hers','herself','he\'s','hi','him','himself','his','hither','hopefully','how','howbeit','however','hundred','i','i\'d','ie','if','ignored','i\'ll','i\'m','immediate','in','inasmuch','inc','inc.','indeed','indicate','indicated','indicates','inner','inside','insofar','instead','into','inward','is','isn\'t','it','it\'d','it\'ll','its','it\'s','itself','i\'ve','j','just','k','keep','keeps','kept','know','known','knows','l','last','lately','later','latter','latterly','least','less','lest','let','let\'s','like','liked','likely','likewise','little','look','looking','looks','low','lower','ltd','m','made','mainly','make','makes','many','may','maybe','mayn\'t','me','mean','meantime','meanwhile','merely','might','mightn\'t','mine','minus','miss','more','moreover','most','mostly','mr','mrs','much','must','mustn\'t','my','myself','n','name','namely','nd','near','nearly','necessary','need','needn\'t','needs','neither','never','neverf','neverless','nevertheless','new','next','nine','ninety','no','nobody','non','none','nonetheless','noone','no-one','nor','normally','not','nothing','notwithstanding','novel','now','nowhere','o','obviously','of','off','often','oh','ok','okay','old','on','once','one','ones','one\'s','only','onto','opposite','or','other','others','otherwise','ought','oughtn\'t','our','ours','ourselves','out','outside','over','overall','own','p','particular','particularly','past','per','perhaps','placed','please','plus','possible','presumably','probably','provided','provides','q','que','quite','qv','r','rather','rd','re','really','reasonably','recent','recently','regarding','regardless','regards','relatively','respectively','right','round','s','said','same','saw','say','saying','says','second','secondly','see','seeing','seem','seemed','seeming','seems','seen','self','selves','sensible','sent','serious','seriously','seven','several','shall','shan\'t','she','she\'d','she\'ll','she\'s','should','shouldn\'t','since','six','so','some','somebody','someday','somehow','someone','something','sometime','sometimes','somewhat','somewhere','soon','sorry','specified','specify','specifying','still','sub','such','sup','sure','t','take','taken','taking','tell','tends','th','than','thank','thanks','thanx','that','that\'ll','thats','that\'s','that\'ve','the','their','theirs','them','themselves','then','thence','there','thereafter','thereby','there\'d','therefore','therein','there\'ll','there\'re','theres','there\'s','thereupon','there\'ve','these','they','they\'d','they\'ll','they\'re','they\'ve','thing','things','think','third','thirty','this','thorough','thoroughly','those','though','three','through','throughout','thru','thus','till','to','together','too','took','toward','towards','tried','tries','truly','try','trying','t\'s','twice','two','u','un','under','underneath','undoing','unfortunately','unless','unlike','unlikely','until','unto','up','upon','upwards','us','use','used','useful','uses','using','usually','v','value','various','versus','very','via','viz','vs','w','want','wants','was','wasn\'t','way','we','we\'d','welcome','well','we\'ll','went','were','we\'re','weren\'t','we\'ve','what','whatever','what\'ll','what\'s','what\'ve','when','whence','whenever','where','whereafter','whereas','whereby','wherein','where\'s','whereupon','wherever','whether','which','whichever','while','whilst','whither','who','who\'d','whoever','whole','who\'ll','whom','whomever','who\'s','whose','why','will','willing','wish','with','within','without','wonder','won\'t','would','wouldn\'t','x','y','yes','yet','you','you\'d','you\'ll','your','you\'re','yours','yourself','yourselves','you\'ve','z','zero');


	$params = explode(" ", $_GET[$get_param_key]);

	$filtered_params = array_diff($params, $commonWords);

	$query = "select DISTINCT wp_posts.ID, wp_posts.post_title  From wp_posts 
			LEFT JOIN wp_postmeta
			ON wp_posts.ID = wp_postmeta.post_id 

			where post_type='".$post_type."'";

	$querywhere = '';

	foreach ($filtered_params as $key => $param) {
		$querywhere .= " AND ((wp_posts.post_title LIKE '%".$param."%')
			OR (wp_postmeta.meta_value LIKE '%".$param."%')
			OR (wp_posts.post_excerpt LIKE '%".$param."%')
			OR (wp_posts.post_content LIKE '%". $param . "%'))";
	};

	$querywhere .= " and post_status= 'publish'";

	$results = $wpdb->get_results( $query . $querywhere);

	return $results;

}

// Display a comma deliminated list of the categories a supplier is assigned to.
function do_get_supplier_cat_markup($post_id){

		$cat_array = get_field('supplier_category', $post_id);

		$html = "";

		if($cat_array){
			foreach ($cat_array as $cat) {
				$html .= $cat . ", ";
			}
		}
		

		return rtrim($html, ',');

}

// Get a list of posts that are related to the current page
function do_get_related_posts($limit){

	
	if(!is_single()){

		session_start();
		// If this is a page listing news then use the first post as an indicator of
		// related articles
		if(isset($_SESSION['IDS'])){
			$post_id = $_SESSION['IDS'][0];
		}

	}else{
		// If this is a single page then simply use the post as an indicator of
		// related articles
		global $post;
		$post_id = $post->ID;
	}
	
	// Get the terms of the current post so we can query other posts that have the same terms
	$terms 				= wp_get_post_terms($post_id, 'post_tag');
	$term_id_array 		= array();

	// Collect the terms IDS into a structure we can use in a taxonomy query
	if($terms ){

		foreach ($terms as $term) {

			array_push($term_id_array, $term->term_id);

		}

	}
	
	
	// If a single post then excude the post ID, if a list of post then exclude all the posts
	// in collected in the SESSION ID varaible.
	$whattoexclude = is_single() ? $post_id : $_SESSION['IDS'];

	// Setup the query
	$args = array(
			'posts_per_page'   => $limit,
			'exclude'          => $whattoexclude,
			'tax_query' => array(
				array(
					'taxonomy' => 'post_tag',
					'field' => 'term_id',
					'terms' => $term_id_array
				)
			)
	);

	$results = get_posts( $args );
	
	// If the amount of post from this query is enough to fullfill the required amount of posts then return
	// Otherwise lets do some more stuff to get more related posts
	if(count($results) >= $limit ){
		
		$category_posts = $results;

	}else{
		
		// Workout how many more post we need to fullfull the requested amount of posts
		$limit 				= intval($limit) - count($results);
		// Lets now find the categories that the post has
		$categories 		= get_the_category( $post_id );
		
		//Collect the category IDs into an array to work with
		$the_categories 	= array();

		foreach($categories as $category){

			array_push($the_categories, $category->term_id);

		}
		// Get posts in these categories
		$category_posts = do_get_category_posts($the_categories, $limit);

		// Add the new posts to the existing collection of results 
		if($category_posts){

			foreach($category_posts as $category_post){

				array_push($results, $category_post);

			}

		}
		

	}
	// Work out if we have to look for more posts to fullfill the limit
	$leftOver = $limit - count($results);
	if($leftOver > 0){
		// If we need more then just get some random posts
		// TODO: I consider this a copout. In future we should run another query based on
		// someother relatable data such as author, news_section, content. This function also has the
		// limitation as that is doesnt look at the SESSION 'exclude ids' variable and therefore could display 
		// posts that are already on the page.
		$randoms = do_get_random_posts($leftOver);

		if($randoms){

			foreach ($randoms as $random) {

				array_push($results, $random);
		
			}

		}
		
	}

	return $results;

}

// Do markup for a news feature panel (small one)
function do_small_feature_panel($item, $args = null){

		$excerpt 	= $args['excerpt'] ? $args['excerpt'] : false;
		$type 		= $args['slider_type'] ? $args['slider_type'] : 'news';
		$thumb 		= $args['thumb'] ? $args['thumb'] : 'sub-feature';
		
		$image 		= wp_get_attachment_image_src( get_post_thumbnail_id( $item->ID ), $thumb );
		$term 		= get_field('news_section',$item->ID);
		$termName 	= get_term_by('id', $term, 'news_section');
		$termName 	= $termName->name;
		$alt_text = get_post_meta(get_post_thumbnail_id($item->ID), '_wp_attachment_image_alt', true);

		?>

		<div class="pure-g">

			<div class="pure-u-8-24 sub-image-container">

				<a href="<?php echo get_the_permalink($item->ID); ?>"><img alt="<?=$alt_text; ?>" class="wow fadeInUp" src="<?php echo $image[0]; ?>"></a>

			</div>

			<div class="pure-u-16-24">

			 	<div class="padding-left">

					<p class="small-feature"
					>
						<a class="sub-feature-category" href="<?php echo get_the_permalink($item->ID); ?>"><?php echo $termName; ?> - </a><a href="<?php echo get_the_permalink($item->ID); ?>" class="sub-feature-title"><?php echo $item->post_title;?>

							<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
							
						</a>

					</p>

				</div>	

			</div>
			
		</div>

<?php

}
// Do markup for a news feature panel with the title on an image overlay
function do_overlay_features_panel($item, $args = null){
	
	$term 		= get_field('news_section',$item->ID);
	$termName 	= get_term_by('id', $term, 'news_section');
	$termName 	= $termName->name;
	$thumb 		= $args['thumb'] ? $args['thumb'] : 'main-feature';
	$image 		= wp_get_attachment_image_src( get_post_thumbnail_id( $item->ID ), $thumb );
	$alt_text 	= get_post_meta(get_post_thumbnail_id($item->ID), '_wp_attachment_image_alt', true);

	// For every post that is displayed on a sidebar we have to ensure that the post
	// hasnt already been used somewhere on the page.  I am controlling this with the use of
	// the super-global Session variable. For every post that is diplayed we add the post ID
	// to the the session variable 'exclude_ids' then when running a query we always make sure to
	// exclude the post IDs that are currently in the session variable.
	session_start();
	// If the session key doesnt exsist then make it exsist
	if(!is_array($_SESSION['exclude_ids'])){
			$_SESSION['exclude_ids'] = array();
		}
	// Add the current items ID to the exclude array so it never appears on the page again
	array_push($_SESSION['exclude_ids'], $item->ID);

	?>
	
	<div class="main-feature-container dco-content">

		<div class="overlay">

			<div class="padding-double">

				<span class="category">
					<?php if($term):?>

							<a href="<?php echo get_term_link($term); ?>"><?php echo $termName; ?></a>

					<?php endif;?>
				</span>
									
				<span class="posted"><?php echo get_the_date(  get_option('date_format'), $item->ID ); ?></span>
									
				<h3><a href="<?php echo get_the_permalink($item->ID); ?>"><?php echo $item->post_title;?> <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a></h3>
								
			</div>
							
		</div>
							
		<a href="<?php echo get_the_permalink( $item->ID )?>"><img alt="<?=$alt_text; ?>" src="<?php echo $image[0]?>"></a>

	</div>

	<?php

}


// Do markup for a news feature panel (large one)
function do_features_panel($item, $args = null){

		$excerpt 	= $args['excerpt'] ? $args['excerpt'] : false;
		$type 		= $args['slider_type'] ? $args['slider_type'] : 'news';
		$thumb 		= $args['thumb'] ? $args['thumb'] : 'sub-feature';
		$image 		= wp_get_attachment_image_src( get_post_thumbnail_id( $item->ID ), $thumb );
		$term 		= get_field('news_section',$item->ID);
		$termName 	= get_term_by('id', $term, 'news_section');
		$termName 	= $termName->name;
		$alt_text 	= get_post_meta(get_post_thumbnail_id($item->ID), '_wp_attachment_image_alt', true);

		// For every post that is displayed on a sidebar we have to ensure that the post
		// hasnt already been used somewhere on the page.  I am controlling this with the use of
		// the super-global Session variable. For every post that is diplayed we add the post ID
		// to the the session variable 'exclude_ids' then when running a query we always make sure to
		// exclude the post IDs that are currently in the session variable.
		session_start();
		// If the session key doesnt exsist then make it exsist
		if(!is_array($_SESSION['exclude_ids'])){
			$_SESSION['exclude_ids'] = array();
		}
		// Add the current items ID to the exclude array so it never appears on the page again
		array_push($_SESSION['exclude_ids'], $item->ID);

		?>

		<p class="sub-feature-title-container">

			<a href="<?php echo get_the_permalink($item->ID); ?>" class="sub-feature-title"><?php echo $item->post_title;?></a>
		
		</p>
		
		<?php if($args['title']):?>

				<p class="slide-title center"><?php echo strtoupper($args['title']); ?></p>

		<?php else:?>

				<p class="posted"><?php if($termName != ""):?><a class="sub-feature-category" href="<?php echo get_term_link($term); ?>"><?php echo $termName; ?> - </a><?php endif?> <?php echo get_the_date(  get_option('date_format'), $item->ID ); ?> </p>

		<?php endif;?>

		<?php if($image):?>

				<div class="sub-image-container">
					
					<a href="<?php echo get_the_permalink($item->ID); ?>"><img alt="<?=$alt_text; ?>" class="wow fadeInUp" src="<?php echo $image[0]; ?>"></a>
				
				</div>

		<?php endif; ?>
			
		<?php if($excerpt):?>
			
			<p class="sub-feature-excerpt">
					
				<?php echo do_get_content_extract($item->ID, get_field('wordcount_for_post_excerpt_in_post_boxes','options'));?>
				
				<a class="sub-feature-more" href="<?php echo get_the_permalink($item->ID); ?>"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>

			</p>
		
		<?php endif; 

}
// Do the markup for a sub feature panel
function do_sub_feature_panel($item, $args = null){

		$excerpt 		= $args['excerpt'] ? $args['excerpt'] : false;
		$type 			= $args['slider_type'] ? $args['slider_type'] : 'news';
		$thumb 			= $args['thumb'] ? $args['thumb'] : 'sub-feature';
		$delay 			= $args['delay'] ? $args['delay'] : '0';
		$animate		= isset($args['animate']) ? $args['animate'] : true;
		$image 			= wp_get_attachment_image_src( get_post_thumbnail_id( $item->ID ), $thumb );
		$term 			= get_field('news_section',$item->ID);
		$termName 		= get_term_by('id', $term, 'news_section');
		$termName 		= $termName->name;
		$alt_text 		= get_post_meta(get_post_thumbnail_id($item->ID), '_wp_attachment_image_alt', true);

		// For every post that is displayed on a sidebar we have to ensure that the post
		// hasnt already been used somewhere on the page.  I am controlling this with the use of
		// the super-global Session variable. For every post that is diplayed we add the post ID
		// to the the session variable 'exclude_ids' then when running a query we always make sure to
		// exclude the post IDs that are currently in the session variable.
		session_start();
		// If the session key doesnt exsist then make it exsist
		if(!is_array($_SESSION['exclude_ids'])){
			$_SESSION['exclude_ids'] = array();
		}
		// Add the current items ID to the exclude array so it never appears on the page again
		array_push($_SESSION['exclude_ids'], $item->ID);

		?>

		
		<?php if($image):?>
		<div class="sub-image-container">
			<a href="<?php echo get_the_permalink($item->ID); ?>"><img alt="<?=$alt_text; ?>" class="<?php echo $do_animate = $animate ? 'wow fadeInUp' : '';?>" data-wow-delay="<?php echo $delay?>s" src="<?php echo $image[0]; ?>"></a>
		</div>
		<?php endif; ?>
			
		<?php if(isset($args['title'])):?>
				<p class="slide-title center"><?php echo strtoupper($args['title']); ?></p>
		<?php else:?>
				<p class="posted">
					<?php if($args['stakeholder']):?>
						
						<?php $stakeholderName = get_the_stakeholder_by_author($item->post_author);?>
						
						<span><?php echo "News by <a href='" . get_the_permalink(get_the_stakeholder_id_by_author($item->post_author)). "'>" . $stakeholderName . "</a> | "; ?></span>

					<?php endif;?>

					<?php echo get_the_date(  get_option('date_format'), $item->ID ); ?></p>
						
		<?php endif;?>
		
		<?php if(isset($args['slider'])): ?>
			
			<div class="pure-g">
										
				<div class="pure-u-3-24">
					
					<i class="fa fa-arrow-circle-left fa-2x fa-nav click-left" aria-hidden="true"></i>
				
				</div>
				
				<div class="pure-u-18-24">
		
		<?php endif;?>
			
				<p class="sub-feature-title-container">

					<?php if($termName != ""):?><a class="sub-feature-category" href="<?php echo get_the_permalink($item->ID); ?>"><?php echo $termName; ?> - </a><?php endif?><a href="<?php echo get_the_permalink($item->ID); ?>" class="sub-feature-title"><?php echo $item->post_title;?>
		
						
						<?php if(!$excerpt):?>
							<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
						<?php endif?>
					</a>
				</p>
		
		<?php if(isset($args['slider'])): ?>

				</div>
				
				<div class="pure-u-3-24 right-nav">
					
					<i class="fa fa-arrow-circle-right fa-2x fa-nav click-right" aria-hidden="true"></i>
				
				</div>
			
			</div>

		<?php endif;?>
			
			<?php if($excerpt):?>
				<p class="sub-feature-excerpt">
					
					<?php echo do_get_content_extract($item->ID, get_field('wordcount_for_post_excerpt_in_post_boxes','options'));?>
				
					<a class="sub-feature-more" href="<?php echo get_the_permalink($item->ID); ?>"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>

				</p>
			<?php endif; 

}

// Do the markup for a slider (see http://unslider.com/ for api)
function do_do_pub_slider($items){
	$unique_id = uniqid();
	?>

		<div class="pure-g">
			
			<div class="pure-u-4-24 center nav-column">
				
				<i class="fa fa-arrow-circle-left fa-2x fa-nav click-left" aria-hidden="true"></i>
			
			</div>
			
			<div class="pure-u-16-24">
				
				<div class="<?php echo $unique_id ?> logo-slider unislide center">
					
					<ul>
						<?php foreach($items as $item):?>
						<li>
							
							<div class="ls-img-container pub">
								
								<?php $image = get_field('front_cover',$item->ID)?>
								<a href="<?php echo get_the_permalink(324)?>"><img alt="<?=$image['alt']; ?>" src="<?php echo $image['sizes']['slider-logo']; ?>"></a>
							
							</div>
						
						</li>
						<?php endforeach;?>			
					</ul>
				
				</div>
			
			</div>
			
			<div class="pure-u-4-24 center nav-column">
				
				<i class="fa fa-arrow-circle-right fa-2x fa-nav click-right" aria-hidden="true"></i>
			
			</div>
			
		</div>	
			<script>
				

				jQuery(document).ready(function (){
					var logoSlider = jQuery('.<?php echo $unique_id;?>.logo-slider').unslider(
							{
							// Removing the default slider navigation so we can add our own
							arrows: false,
							nav: false
							}
					);
					// Set custom next and previous buttons
					leftButton = jQuery('.<?php echo $unique_id;?>.logo-slider').parent().parent().parent().find('.click-left');
					rightButton = jQuery('.<?php echo $unique_id;?>.logo-slider').parent().parent().parent().find('.click-right');
					leftButton.click(function(){
						logoSlider.unslider('prev');
					});
					rightButton.click(function(){
						logoSlider.unslider('next');
					});

					// Set the navigation buttom into the middle of the container
					alignHeight(jQuery('.<?php echo $unique_id;?>.logo-slider').parent().parent().parent().find('.click-left').parent(), leftButton );
					alignHeight(jQuery('.<?php echo $unique_id;?>.logo-slider').parent().parent().parent().find('.click-right').parent(), rightButton );
					
					// Align the images in the slider into the middle of the container.
					jQuery('.<?php echo $unique_id ?>.logo-slider img').each(function(){
						thisConainterHeight = jQuery(this).closest('[class*="pure-u"]').height();
						thisHeight = jQuery(this).parent().height();

						marginTop = (thisConainterHeight - thisHeight) / 2;

						jQuery(this).css('margin-top', marginTop + 'px' )

					});
					// If there is only one item then hide the nav buttons.
					<?php if(count($items) == 1):?>
							jQuery('.<?php echo $unique_id ?>').parent().parent().parent().find('.nav-column i').hide();	
					<?php endif;?>
				});

			</script>

	<?php

}

// Do the markup for a slider of logos (see http://unslider.com/ for api)

function do_do_logo_slider($items){

	$unique_id = uniqid();

	?>

		<div class="pure-g">
			
			<div class="pure-u-4-24 center nav-column">
				
				<i class="fa fa-arrow-circle-left fa-2x fa-nav click-left" aria-hidden="true"></i>
			
			</div>
			
			<div class="pure-u-16-24">
				
				<div class="<?php echo $unique_id ?> logo-slider unislide center">
					
					<ul>
						<?php foreach($items as $item):?>
						<li>
							
							<div class="ls-img-container">
								
								<?php $image = get_field('logo', $item->ID)?>
								<?php if(get_field('website', $item->ID) != ''): ?>
									<a href="<?php the_field('website', $item->ID) ?>"><img alt="<?=$image['alt']; ?>" src="<?php echo $image['sizes']['slider-logo']; ?>"></a>
								<?php else: ?>
									<a href="<?= get_permalink($item->ID) ?>"><img alt="<?=$image['alt']; ?>" src="<?php echo $image['sizes']['slider-logo']; ?>"></a>
								<?php endif; ?>
							</div>
						
						</li>
						<?php endforeach;?>			
					</ul>
				
				</div>
			
			</div>
			
			<div class="pure-u-4-24 center nav-column">
				
				<i class="fa fa-arrow-circle-right fa-2x fa-nav click-right" aria-hidden="true"></i>
			
			</div>
			
		</div>	
			<script>
				

				jQuery(document).ready(function (){
					var logoSlider = jQuery('.<?php echo $unique_id;?>.logo-slider').unslider(
							{
							// Removing the default slider navigation so we can add our own
							arrows: false,
							nav: false

							}
					);

					// Set custom next and previous buttons
					leftButton = jQuery('.<?php echo $unique_id;?>.logo-slider').parent().parent().parent().find('.click-left');
					rightButton = jQuery('.<?php echo $unique_id;?>.logo-slider').parent().parent().parent().find('.click-right');
					leftButton.click(function(){
						logoSlider.unslider('prev');
					});
					rightButton.click(function(){
						logoSlider.unslider('next');
					});

					// Set the navigation buttom into the middle of the container
					alignHeight(jQuery('.<?php echo $unique_id;?>.logo-slider').parent().parent().parent().find('.click-left').parent(), leftButton );
					alignHeight(jQuery('.<?php echo $unique_id;?>.logo-slider').parent().parent().parent().find('.click-right').parent(), rightButton );
					
					// Align the images in the slider into the middle of the container.
					jQuery('.<?php echo $unique_id ?>.logo-slider img').each(function(){
						thisConainterHeight = jQuery(this).closest('[class*="pure-u"]').height();
						thisHeight = jQuery(this).parent().height();

						marginTop = (thisConainterHeight - thisHeight) / 2;

						jQuery(this).css('margin-top', marginTop + 'px' )

					});
					// If there is only one item then hide the nav buttons.
					<?php if(count($items) == 1):?>
							jQuery('.<?php echo $unique_id ?>').parent().parent().parent().find('.nav-column i').hide();	
					<?php endif;?>
				});


			</script>

	<?php

}
// Do the markup for the logo panel
function do_logos_panel($items, $title){
	
		?>
		
		<div class="pure-u-24-24 center"><h4><?php echo $title; ?></h4></div>
			
		<?php do_do_logo_slider($items); 

}

// 
function do_can_user_view_page(){

	global $post;

	if(is_user_logged_in()) return true;

	$desision = get_field('available_to_registered_users', $post->ID);

	if(is_array($desision) && $desision[0] =='yes'){
		return false;
	}

	return true;

}

// For each page on wordpress there is an option to make a page nav item only availble to
// registered (logged in user). "Only registered users see the page in the navigation?" THis function looks at
// the page ID and returns false if the option is checked. Used in our custom walker: do_walker.php

function do_see_nav_item_if_logged_in($page_id){

	if(is_user_logged_in()) return true;

	$desision = get_field('see_the_page_in_the_navigation', $page_id);

	if(is_array($desision) && $desision[0] =='yes'){
		return false;
	}

	return true;

}

// This function is the beating heart of the theme. Sets up al the markup as per the page builder options
// defined by the client. See ACF Flexible field documentation:
// https://www.advancedcustomfields.com/resources/flexible-content/
function do_page_builder($acf_flexible_field, $section_dir, $admin_field_name, $options = null){
	
	$options = $options ? $options : false;

	if(do_can_user_view_page()): ?>
	
	<?php if( have_rows($acf_flexible_field, $options) ): ?>

   <?php $levelfiles = mj_get_the_level_parts(get_template_directory()."/".$section_dir."/"); ?>
	
   <?php while ( have_rows($acf_flexible_field, $options) ) : the_row(); ?>

   		<?php foreach($levelfiles as $levelfile):?>

   				<?php if( get_row_layout() == $levelfile ): ?>
  
   					<?php $deactivate = get_sub_field($admin_field_name)?>
   					
   					<?php if($deactivate =='deactivate'):?>

						<?php continue; ?>
					
					<?php endif; ?>

					<?php if($deactivate =='admin'):?>

						<?php 

							if ( current_user_can('edit_post') ) {

	 							 include $section_dir ."/".$levelfile.".php"; continue;

							}else{

								 continue;
							
							}

						?>

					<?php endif; ?>

					<?php include $section_dir ."/".$levelfile.".php"; ?>

				<?php endif; ?>

   		<?php endforeach ?>

	<?php endwhile; ?>

<?php endif; ?>

<?php else:?>

	<?php include_once('access.php'); ?>

<?php endif;

}

// This function is used to deliver the any session data to the registrsation form. This is required because if
// a page refresh happens we dont want the user to lose any form information they have already completed.
// Good UX. Nawhatamean.
function do_form_session(){

	session_start();
	?>
	<?php if( !is_user_logged_in()):?>
		<script>
			jQuery(document).ready(function (){
					<?php foreach($_SESSION as $session_key => $session_value) :?>
						jQuery('#acf-<?php echo $session_key ?>').val('<?php echo $session_value; ?>');
					<?php endforeach; ?>
			});
		</script>
	<?php endif ?>
		<script>
			jQuery(document).ready(function (){
				jQuery('.acf-do-username').closest('.acf-valid-container').prependTo('.acf-field-579f427b6fec9');
				jQuery('.acf-do-email').closest('.acf-valid-container').prependTo('.acf-field-579f2e52f0ccb');
				
			});	
		</script>
	<?php

}

// Create the pardot URL to deliver the registratrion data to Pardot
function do_do_pardot_iframe_url($pardot_url_input){

		// For data is stored in the session (see do_external_registration_stuff where the data is added)
		session_start();
		// The root pardot URL for the form handler. Set by client in Journey Options.
		$pardot_url = $pardot_url_input . '?';
		
		if($_SESSION){
			// We have to make sure each key equals something or the URL structure will break. Anything
			// that is empty is make equal to 0.
			foreach ($_SESSION as $key => $value) {
				if($value == '' || $value == null){
					$value = '0';
				}
				//This is a hack because pardot is using the inverse values for opt in data
				if($key == 'field_57a0a1a0d8d97'){
					if($value == 1){
						$value = 0;
					}else{
						$value = 1;
					}
				}

				$pardot_url .= $key . "=" . rawurlencode($value) . '&';
			}
		}
		
		// Remove the trailing "&""
		$pardot_url = substr($pardot_url, 0, -1);

		return $pardot_url;

}

// Hooking into the template redirect action to allow us to run a redirect with php if 
// we have confirmation by the SESSION the ragistration is now complete.
// See do_external_registration_stuff() for how the session veriable is set and 
// https://codex.wordpress.org/Plugin_API/Action_Reference/template_redirect about the redirect
// functionality.
function confirm_registration_and_redirect(){
		
		session_start();

		if($_SESSION['registration_complete'] == 1){
			
			wp_redirect(get_the_permalink(get_field('after_registration','do-journey')));
			// Unset the session variable after redirect to aviod a never ending redirect loop
			unset($_SESSION['registration_complete']);
			exit();
		}
		return;
	 	
}

add_action( 'template_redirect', 'confirm_registration_and_redirect' );


// Do the parkup for the pardot iframe
function pardot_iframe(){

	global $post;

	// If we are on the page that we are taken to after registration (set by user in Journey Options)
	// then we know we should be displaying the Pardot iframe which will deliver the form session
	// data to Pardot.
	if($post->ID == get_field('after_registration','do-journey')){

			session_start();
			
			$pardot_url = do_do_pardot_iframe_url(get_field('pardot_url_for_registration', 'do-journey'));
			echo "<iframe src='" . $pardot_url . "' width='1' height='1'></iframe>";
			
			
	}

}

add_action('do_do_after_body', 'pardot_iframe');

// Do the markup and functionality for the registration form. Using acf_form():
// https://www.advancedcustomfields.com/resources/acf_form/
// This markup is printed out via shortcode [registration]
function do_registration_form(){
	
	session_start();

	$user_id = get_current_user_id();
	// if there is a username validation flag in the session 
	if($_SESSION['validate_username']){
		echo "<div class='acf-valid-container'><div class='acf-field'><div class='acf-error-message acf-do-username'><p>Username already exists!</p></div></div></div>";
	}
	// if there is a email validation flag in the session 
	if($_SESSION['validate_email']){
		echo "<div class='acf-valid-container'><div class='acf-field'><div class='acf-error-message acf-do-email'><p>Email address already exists!</p></div></div></div>";
	}
	// If the (now registered) user somehow gets to this page again then signpost
	if( is_user_logged_in() ){

		$post_id = 'user_' . $user_id;
		echo "<p>Hi ". get_field('first_name', $post_id).", you have already registered. Thanks!</p>";

	// Otherwise display the form.
	}else{
		 
		// Ensure that the post id of the for is completely unique. THis prevents database data
		// being displayed on the page for other users.
		// NOTE: This issue was nerarly the death of me and having this one line of code ensured
		// I lived a long, happy, boring life.
		$post_id = 'user_acf_' . uniqid();
		// 
		echo "<p>Register now and gain access to restricted content on Defence Online for FREE.</p>";

		// See: https://www.advancedcustomfields.com/resources/acf_form/
		acf_form(array(

	 			'field_groups'	=> array(509),
	 			'post_id'		=> $post_id,
	 			'updated_message' => false,
	 			'submit_value' => 'Register'

 		));
	}
	 
}

add_shortcode( 'registration', 'do_registration_form' );


// On the save of the acf_form in function do_registration_form() do the folowing
// * Validate the data
// * Register a user with the form data
// * Collect the data into a session variable so that the data can be delivered to pardot on the
// 	 Thank You page (set in Registstation Journey Options)
function do_external_registration_stuff( $post_id ) {

	session_start();
	// Only do all this stuff if its the registration page or a user page (post ID is set to user_{ID})
	if($post_id == get_field('registration_page','do-journey') || substr($post_id,0,4) == 'user'){

		$postdata = array();

		// For each bit of POST data that begins with acf (the registration form data) at it to
		// the session
		foreach ($_POST as $key => $value) {
			if($key == 'acf'){
				foreach($value as $acf_key => $acf_value){
					$postdata[$acf_key] = $acf_value;
				}
				if( !is_user_logged_in()){
					
					foreach($value as $acf_key => $acf_value){
							$_SESSION[$acf_key] = $acf_value;
					}
				}
			}
		}
		// Check if username already exsists before adding user. If it does create session variable flag
		// Note: https://codex.wordpress.org/Function_Reference/username_exists
		if($invalid = username_exists( $postdata['field_579f427b6fec9'] )){
			session_start();
			$_SESSION['validate_username'] = 1;
		}
		// Check if email already exsists before adding user. If it does create session variable flag.
		// Note: https://codex.wordpress.org/Function_Reference/email_exists
		if($invalid = email_exists( $postdata['field_579f2e52f0ccb'] )) {
			session_start();
			$_SESSION['validate_email'] = 1;
		}
		// If no error flags raise then process user and pardot submission.
		if(!$invalid){

			$userdata = array(
			    'user_login'  =>  $postdata['field_579f427b6fec9'],
			    'user_pass'   =>  NULL,
			    'user_email'  =>  $postdata['field_579f2e52f0ccb']
			);
			// Insert the user into wordpress
			$new_user_id = wp_insert_user( $userdata );

			if ( ! is_wp_error( $new_user_id  ) ) {

	    		// Add the rest of the user data into the corresponding acf custom fields
	    		foreach($postdata as $field_key => $field_value){
					update_field( $field_key, $field_value, 'user_'. $new_user_id );
				}

				// User added successully then send email to admin and user confirming such
	    		wp_new_user_notification($new_user_id,false,'both');

				// Who to alert about this registration? Set in Journey Options
				$recipients 		= preg_replace('/\s+/', '', get_field('do_registration_email','do-journey'));
	   			$recipients 		= explode(",", $recipients);
				
				$subj = 'DO Registration';
				$body = '';
				// Create email body which is just a list of all data submitted by the form	
				foreach ($postdata as $key => $value) {
				    $body .= $key . " : " . $value . "\r\n";
				}
				// Send the email
				wp_mail( $recipients, $subj, $body );

				session_start();
				// Create a session variable flag to registration as successful. This tells the page to render
				// the pardot iframe to deliver the data to Pardot.
				$_SESSION['registration_complete'] = 1;
			}
			
		}
		
	}
		
    return $post_id;

}

add_filter('acf/save_post' , 'do_external_registration_stuff', 20 );

// Get an excerpt of the content of a post.
function do_get_content_extract($post_id, $extractWords = null, $ReadMoreText = null){

	$content = get_post_field('post_content', $post_id);

	$content = strip_tags(apply_filters('the_content', $content));	

	if($extractWords){

		$words = explode(' ', $content, ($extractWords + 1));

		$ReadMoreText = $ReadMoreText ? " ... " . "<a href='". get_permalink($post_id) ."'>". $ReadMoreText . "</a>" : "";

		if(count($words) > $extractWords){

		  	array_pop($words);

		  	$content = implode(' ', $words) .  $ReadMoreText;

		}else{

		  	$content = implode(' ', $words);
		  	
		}

	}
	
	return $content;
}

// Print the video markup for the video level section levels /levels/video.php
function do_video_markup($responsiveness = null){

	?>

		<div class="pure-u-1 pure-u-<?php echo $responsiveness; ?>-16-24 main-column">

			<div>

				<div class="video-container dco-content">
						
					<?php echo do_shortcode('[fve]'. get_sub_field('video_url').'[/fve]'); ?>

				</div>

			</div>

		</div>

	<?php

}
// Do the markup for the header slider. Content is set in Site options.
// See http://unslider.com/ for the API
function do_header_slider(){

	?>
	<div class="header-slider unislide wow fadeIn" >
		
		<?php if( have_rows('rotating_content','options') ): ?>
			
			<ul>
				
				<?php while( have_rows('rotating_content', 'options') ): the_row(); ?>

					<li>

						<div class="pure-g">
							
							<div class="pure-u-1">
								
								<h2><?php echo strtoupper(get_sub_field('title')) ?> <a href="<?php the_sub_field('button_url');?>" class="cta addspaceleft"><?php echo strtoupper(get_sub_field('button_text'));?></a></h2> 
							
							</div>	
						
						</div>
					
					</li>

				<?php endwhile; ?>

			</ul>

		<?php endif; ?>
			
	</div>

	<script>

			jQuery(function() {
				var slider = jQuery('.header-slider').unslider(
					{
					autoplay: true,
					arrows: false,
					nav: false,
					animation: 'fade'
					}
				);
				// Header slider is set at opacity 0 in CSS. Once the slider is loaded then show.
				// Using opacity instead of hide() and show() because it seems smoother.
				slider.on('unslider.ready', function() {
					jQuery('.header-slider').css('opacity', 1);
				});
				
				
			});
	</script>
	<?php

}

// Set up a sidebar pagebulder
function get_do_sidebar($target = null){

	$the_target = $target ? $target : 'do-sidebar';

	do_action('do_sidebar_top');
	
	do_page_builder('sidebar_levels', 'sidebar_levels', 'level_status', $the_target);


}

// Do the markup ofor the post author section on a single post
function do_post_author(){
	
	global $post;
	$post_author = get_post_field( 'post_author', $post->ID );
	$meta_to_get = array( 'nicename', 'email', 'description');
	$meta 		 = get_the_author_meta($meta_to_get,$post_author);
	$image 		 = get_field('profile_picture', 'user_' . $post_author);
	?>

	<?php 
	// Dont show the post author markup if the post author is itsupport or mario
	// This is because the syndicated posts are auto assigned to the site admins.
	if($post_author != 0 && $post_author != 1 && $post_author != 2):?>
	<div class="padding level-author">
		<div class="pure-g">
			<div class="pure-u-6-24">
				<div class="padding">
					<img class="bio-image" src="<?php echo $image['sizes']['profile-logo'];?>"/>
					
				</div>
			</div>
			<div class="pure-u-18-24">
				<div class="padding">
					<h3>Post written by: <a href="mailto:<?php echo get_the_author_meta('user_email', $post_author) ?>"><?php echo get_the_author_meta('display_name', $post_author);?></a></h3>
					<p><?php echo get_the_author_meta('description', $post_author);?></p>
				</div>
			</div>
		</div>
	</div>
	<script>
			jQuery(document).ready(function (){
					jQuery('.bio-image').attr('style', 'height:' + jQuery('.bio-image').width() + 'px!important');
			});
	</script>
	<?php endif;

}

// Do the required query to display paginated post lists.
function do_pre_nav($do_paged){

  global $wp_query;

  $posts_per_page = is_single() ? 1 : get_option('posts_per_page');

   if(is_archive()){

   	$wp_query->query_vars['paged'] = $do_paged;
   	$wp_query->query_vars['posts_per_page'] = $posts_per_page;

   	return $row_amount;

   }elseif(is_search()){

   	$wp_query->query_vars['paged'] = $do_paged;
   	$wp_query->query_vars['posts_per_page'] = $posts_per_page;
   	
   	return $wp_query;

   }else{

   	// The client can select categories to exclude from the main list of blog posts.
   	$cats_to_exclude = get_field('categories_to_exclude', 'options');
   	
  	$args = array(
   		'post_type' => 'post',
   		'posts_per_page' => $posts_per_page,
   		'paged' => $do_paged,
   		'category__not_in' => $cats_to_exclude
   );
  	
  	return new WP_Query( $args );
  }

}

function do_do_pagination($query) {

  if ($query->is_main_query() && is_search()) {

  	$do_paged 		= isset( $_GET['do_paged']) ? $_GET['do_paged'] : 1; 
  	$posts_per_page = is_single() ? 1 : get_option('posts_per_page');
    $query->set('posts_per_page', $posts_per_page );
    $query->set('paged', $do_paged );
    $query->set('order_by', 'date' );
    $query->set('order', 'DESC' );


  }

}



add_action('pre_get_posts', 'do_do_pagination');

// Do the pagination markup
function do_do_nav($current_page, $row_amount = null){

	$post_per_page = get_option('posts_per_page');


	if(($current_page * $post_per_page) >= $row_amount){
		$last_page = true;
	}
	
	?>
	<?php if($current_page != 1 ): ?>
		<a href="<?php echo esc_url( add_query_arg( 'do_paged', $current_page - 1) )?>"><i class="fa fa-arrow-left" aria-hidden="true"></i> Previous</a>
	<?php endif;?>
	<?php if(!$last_page):?>
		<a style="float:right" href="<?php echo esc_url( add_query_arg( 'do_paged', $current_page + 1) )?>">Next <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
	<?php endif;
}

// Do the markup for single posts tags
function do_do_tags(){

	global $post;

	$tags = wp_get_post_tags($post->ID);
	?>
	<p class="do-tag-container">

		<?php if($tags): ?>

			<?php foreach($tags as $tag):?>
					
					<span class="do-tag"><a href="<?php echo get_tag_link($tag->term_id); ?>"><i class="fa fa-tag" aria-hidden="true"></i> <?php echo $tag->name;?></a></span> 

			<?php endforeach;?>

		<?php endif;?>

	</p>

	<?php
}

// Do the makup for a post title
// $term is the news_section ID the post is attached to
// $termName is the name of news section
function do_do_post_title($post_id, $term, $termName){

	?>	
					
		<h2>
			<a href="<?php echo get_the_permalink($post_id);?>"><?php echo get_the_title($post_id); ?></a>
		</h2>
		
		<?php if($aTerm = get_category_link( $term )):?>
		
			<span class="category">

				<a href="<?php echo $aTerm; ?>"><?php echo $termName; ?></a>

			</span>

		<?php endif;?>

		<span class="posted"><?php echo get_the_date(  get_option('date_format'), $post_id ); ?></span>

	<?php
}


function do_login_button($type = null, $session_bar = null){

	if(!$type){
		$type = 'Login';
	}

	global $post;

	$targetID = uniqid();

	if($type == 'Login'){

		$icon = '<i class="fa fa-sign-in" aria-hidden="true"></i>';

	}else{

		$icon = '<i class="fa fa-pencil-square-o" aria-hidden="true"></i>';

	}
	?>
	<?php if($type == 'Login'):?>

	<?php if($session_bar):?>
	<a data-remodal-target="<?php echo $targetID ?>" class="<?php echo $type; ?>Button" style="cursor: pointer">
			<?php echo $icon . strtoupper($type); ?>
	</a>
	<?php else:?>
	<a data-remodal-target="<?php echo $targetID ?>" class="cta cta-full cta-red <?php echo $type; ?>Button">
			<?php echo $icon . " " . strtoupper($type); ?>
	</a>
	<?php endif?>
	
	<div class="remodal login-modal" data-remodal-id="<?php echo $targetID; ?>" data-remodal-options="hashTracking: false, closeOnOutsideClick: true" >
		
		<div class="pure-g">
			
			<div class="pure-u-1">
				
				<div class="padding">

					<div class="grey-container">
						
						<button data-remodal-action="close" class="remodal-close"></button>
							
							<h2>Login</h2>
							
							<?php  $image_array = get_field('site_logo', 'options');?>
							
							 <p>
							 	<img  style="height:auto" src="<?php echo $image_array['sizes']['site-logo']?>" />
							 </p>

							<?php wp_login_form(); ?> 

							<p>Not a member? <a href="<?php echo do_registration_url(); ?>">Register here</a></p>

							<p><a href="<?php echo wp_lostpassword_url(); ?>" title="Lost Password">Lost Password</a></p>
					
					</div>

				</div>

			</div>
	
		</div>

	</div>

	<?php else: ?>
		<?php if($session_bar):?>
		<a href="<?php echo do_registration_url(); ?>" class="<?php echo $type; ?>Button" style="cursor: pointer">
			<?php echo $icon . strtoupper($type); ?>
		</a>
		<?php else:?>
		<a href="<?php echo do_registration_url(); ?>" class="cta cta-full cta-red <?php echo $type; ?>Button">
			<?php echo $icon . " " . strtoupper($type); ?>
		</a>
		<?php endif?>

	<?php endif;

}

function do_registration_url(){

	return get_the_permalink(get_field('registration_page','do-journey'));

}

function do_do_login_modal(){
	
	global $post;

	$targetID = uniqid();
	?>
	<div class="hide-xs hide-sm">
		<a data-remodal-target="<?php echo $targetID ?>" class="cta loginButton">
			Login/Register
		</a>
	</div>

	<div class="hide-md hide-lg hide-xl">
		<a href="<?php  echo wp_login_url( get_post_permalink($post->ID) ); ?>" class="cta loginButton addspaceright">
			Login
		</a>
		<a href="<?php echo wp_registration_url(); ?>" class="cta loginButton">
			Register
		</a>
	</div>


	<div class="remodal login-modal" data-remodal-id="<?php echo $targetID; ?>" data-remodal-options="hashTracking: false, closeOnOutsideClick: true" >
		
		<div class="pure-g">
			
			<div class="pure-u-1 pure-u-md-24-24">
				
				<div class="padding">
					
					<h2>Login</h2>
					
					<div class="grey-container">
						
						<button data-remodal-action="close" class="remodal-close"></button>

							<?php wp_login_form(); ?> 
					
					</div>

					<h2>
					Not a member?
				</h2>
				
				<p>
					<?php echo do_shortcode('[cta url="'. wp_registration_url(). '" text="Register"]'); ?> 
				</p>
									
				</div>

			</div>
		
	
	
	</div>

</div>


	<?php
}


function do_register_page( $register_url ) {
    
    $pageid = get_field('registration_page','do-journey');

    return get_the_permalink($pageid);
    
}

add_filter( 'register_url', 'do_register_page' );

function new_mail_from($old) {
 	
 	return get_option('admin_email');

}
function new_mail_from_name($old) {

 	return 'Defence Online';

}

add_filter('wp_mail_from', 'new_mail_from');
add_filter('wp_mail_from_name', 'new_mail_from_name');


if ( !function_exists('wp_batch_user_notification') ) { 
function wp_batch_user_notification( $user_id, $deprecated = null, $notify = '' ) {
    if ( $deprecated !== null ) {
        _deprecated_argument( __FUNCTION__, '4.3.1' );
    }
 
    global $wpdb, $wp_hasher;
    $user = get_userdata( $user_id );
 
    if ( 'admin' === $notify || ( empty( $deprecated ) && empty( $notify ) ) ) {
        return;
    }


    $message = get_field('batch_registration_email_content', 'do-journey');

    $message = str_replace('[do_firstname]', '[do_firstname user_id="'. $user_id .'"]', $message);
    $message = str_replace('[do_surname]', '[do_surname user_id="'. $user_id .'"]', $message);
    $message = str_replace('[do_username]', '[do_username user_id="'. $user_id .'"]', $message);
    $message = str_replace('[do_password_link]', '[do_password_link user_id="'. $user_id .'"]', $message);
    $message = str_replace('[do_email]', '[do_email user_id="'. $user_id .'"]', $message);

    $messageFinal = do_shortcode($message);

    $email_subject = get_field('batch_reg_email_subject','do-journey');

    wp_mail($user->user_email, $email_subject, $messageFinal);
    
    }
}
function add_featured_image_to_rss_two(){
	global $post;
	if($thumb = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'sub-feature')){

		echo "<featuredImage>" .$thumb[0]. "</featuredImage>";
	}
}

add_action('rss2_item','add_featured_image_to_rss_two');

function log_user_as_active($user, $new_pass){

	$user_id = $user->ID;

	add_user_meta( $user_id, 'user_password_activated', 1, true );
	add_user_meta( $user_id, 'user_password_activated_when', current_time('mysql'), true );

}

add_action('after_password_reset', 'log_user_as_active', 10, 2);

function do_count_logins( $user_login, $user ) {
    
    $user_id = $user->ID;

    $login_count = get_user_meta( $user_id, 'do_login_count', true );

   	if($login_count == ''){
   		$login_count = 0;
   	}

   	$login_count++;

   	update_user_meta( $user_id, 'do_login_count', $login_count);
   	update_user_meta( $user_id, 'user_last_login_when', current_time('mysql'));


}
add_action('wp_login', 'do_count_logins', 10, 2);

function cf_search_join( $join ) {
    global $wpdb;

    if ( is_search() ) {    
        $join .=' LEFT JOIN '.$wpdb->postmeta. ' ON '. $wpdb->posts . '.ID = ' . $wpdb->postmeta . '.post_id ';
    }
    
    return $join;
}
add_filter('posts_join', 'cf_search_join' );


function lookatorderbytwo($thing){
	if ( is_search() ) {
		return "wp_posts.post_date DESC";
	}

return $thing;

}
add_filter( 'posts_search_orderby', 'lookatorderbytwo'  );


function cf_search_where( $where ) {
    global $pagenow, $wpdb;
   
    if ( is_search() ) {
        $where = preg_replace(
            "/\(\s*".$wpdb->posts.".post_title\s+LIKE\s*(\'[^\']+\')\s*\)/",
            "(".$wpdb->posts.".post_title LIKE $1) OR (".$wpdb->postmeta.".meta_value LIKE $1)", $where );
    }

    return $where;
}
add_filter( 'posts_where', 'cf_search_where' );

/**
* Prevent duplicates
*
* http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_distinct
*/
function cf_search_distinct( $where ) {
    global $wpdb;

    if ( is_search() ) {
        return "DISTINCT";
    }

    return $where;
}
add_filter( 'posts_distinct', 'cf_search_distinct' );


function do_acf_flexible_content_layout_title($title, $field, $layout, $i){

	if($postfix = get_sub_field('title')){
		$title = $title. " : ". $postfix;
	}
	
	return $title;
}


add_filter('acf/fields/flexible_content/layout_title/name=levels', 'do_acf_flexible_content_layout_title', 10, 4);

function reportlinks_add_dashboard_widgets() {
  
    wp_add_dashboard_widget(

            'report-links',         
            'DO User Reports',        
            'reportlinks_dashboard_widget_function' 

        ); 
 
    global $wp_meta_boxes;
 
    $normal_dashboard           = $wp_meta_boxes['dashboard']['normal']['core'];
 
    $example_widget_backup      = array( 'report-links' => $normal_dashboard['report-links'] );
    
    unset( $normal_dashboard['report-links'] );
 
    $sorted_dashboard           = array_merge( $example_widget_backup, $normal_dashboard );
 
    $wp_meta_boxes['dashboard']['normal']['core'] = $sorted_dashboard;
}

add_action( 'wp_dashboard_setup', 'reportlinks_add_dashboard_widgets' );

function reportlinks_dashboard_widget_function() {
    
    echo "<table cellpadding='5'><tr>";
    echo "<td style='vertical-align: middle;'>User Report</td><td style='vertical-align: middle;'><a target='blank' class='button-secondary' href='http://www.defenceonline.co.uk/userexport.php'>View</a>";
    echo "</td></tr>";
    echo "</table>";
    
}

function do_do_video_thumbnail_with_link($video, $popup = null){

		$videourl 		= get_field('video_url', $video->ID);
  		$video_type 	= getVideoType($videourl);
  
		switch ($video_type) {
		  	
		  	case 'vimeo': 
		  	
		  	$video_id		= getVimeoId($videourl);
		  	$vidtitle 		= ucwords(get_vimeo_title_by_id($video_id));
			$img 			= '<img style="cursor:pointer" alt="'.$vidtitle. '" src="'.getVideoThumbnailByUrl("http://vimeo.com/". $video_id).'">';
		  	
		  	break;
		  	
		  	case 'youTube':
		  	
		  	$video_id		= getYoutubeId($videourl);
		  	$vidtitle 		= ucwords(get_youtube_title_by_id($video_id));
		  	$img 			= '<img style="cursor:pointer" alt="'.$vidtitle. '" src="http://img.youtube.com/vi/'. $video_id .'/mqdefault.jpg">';
		  	
		  	break;

		}
		?>
		
		<?php if(!$popup): ?>

			<div class="video-wrap">

				<a title="<?php echo $vidtitle; ?>" href="<?php echo get_the_permalink($video->ID)?>">
				<span></span>
				
				<?php echo $img; ?>

				</a>

			</div>

		<?php else: ?>

			<div class="video-wrap">

				<a class="modal-click-<?php echo $video->ID;?>" data-remodal-target="modal-video-<?php echo $video->ID;?>">
					
					<span></span>
				
					<?php echo $img; ?>

				</a>

			</div>

			<div class="remodal" data-remodal-id="modal-video-<?php echo $video->ID;?>">
									  
				<button data-remodal-action="close" class="remodal-close"></button>
											  
				<div><?php echo getEmbedVideo($videourl); ?></div>
											 						  
			</div>

			<script>

				jQuery(document).on('closed', '.remodal', function (e) { 
					
					removed = jQuery(this).find('iframe').detach();
					jQuery(this).find('.embed-container').prepend(removed);

				});
				
			</script>

		<?php endif;?>

		<?php
}

function do_do_help_icon($content){
	
	$unique_id = uniqid();
	?>
	
	<a data-remodal-target="modal-video-<?php echo $unique_id;?>"><i class="fa fa-info-circle" aria-hidden="true"></i></a>

	<div class="dco-content remodal left-align-remodal" data-remodal-id="modal-video-<?php echo $unique_id;?>">
									  
		<button data-remodal-action="close" class="remodal-close"></button>
									  
			<?php echo $content; ?>
									 
									  
	</div>

	<?php
}

function do_do_stakeholder_mini($method = null, $post_id = null, $hide_excerpt = null ){

	if(!$method){
		$method = "random";
	}
	
	switch ($method) {
		case 'random':

			$stakeholdersObj 	= new do_stakeholder;
			$stakeholders 		= $stakeholdersObj->get_all();
			$stakeholder_count	= count($stakeholders);
			$random_index 		= rand ( 0 , $stakeholder_count - 1 );
			$stakeholder 		= $stakeholders[$random_index];
			$profile_header		= 'Random Stakeholder Profile:';

			break;

		case 'latest':

			$stakeholdersObj 	= new do_stakeholder;
			$stakeholders 		= $stakeholdersObj->get_all(null, true);

			$stakeholder 		= $stakeholders[0];
			$profile_header		= 'Latest Stakeholder:';
			break;

		case 'specific':

			$stakeholder_id = do_get_stakeholder_id($post_id);
			$stakeholder 	= get_user_by('ID', $stakeholder_id);
			$profile_header	= 'Featured Stakeholder:';

		break;

		default:
			
		break;
	}
	
	?>
	
	<div class="pure-1 stakeholder-container-mini">
		
		<?php 
			$sh_url = get_the_permalink(get_the_stakeholder_id_by_author($stakeholder->ID));
			$image 	= get_field('sh_company_logo', 'user_' . $stakeholder->ID);
			$alt_text = $image['alt'];
		?>
			
		<p style="margin-top:0; padding-top: 0"><?php echo $profile_header; ?></p>
		
		<a href="<?php echo $sh_url; ?>"><img alt="<?=$alt_text; ?>" class="padding" src="<?php echo $image['sizes']['profile-logo']; ?>" /></a>
			
		<?php if(!$hide_excerpt):?>	
			
			<p>

				<?php echo do_get_stakehoder_extract(get_field('sh_about_us_text','user_'. $stakeholder->ID ), 20, '<a class="readmore" href="'. $sh_url .'"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>')?>
																		
			</p>
			
			<p><a href="<?php echo $sh_url; ?>" class="cta">View Profile</a></p>

		<?php endif;?>
					
	</div>

	<?php
}
function do_do_supplier_mini($method = null, $post_id = null){

	if(!$method){
		$method = "random";
	}
	
	switch ($method) {

		case 'random':

			$args = array(
				'post_type' => 'supplier',
				'posts_per_page' => 1,
				'orderby' => 'rand'
			);

			$supplier_array = get_posts($args);
			$supplier 		= $supplier_array[0];

			break;
		case 'latest':

			$supplier_array = do_post_by_custom_post('supplier', 1);
			$supplier 		= $supplier_array[0];

			break;

		case 'specific':

			$supplier 	= get_post($post_id);

		break;

		default:
			
		break;
	}
	
	?>
	
	<div class="pure-1 supplier-container-mini">
		
		<div class="padding">

			<?php $image 	= get_field('logo',$supplier->ID);?>
		
			<a href="<?php echo get_the_permalink($supplier->ID)?>"><img alt="<?=$image['alt']; ?>" class="padding no-border" src="<?php echo $image['sizes']['profile-logo']?>" /></a>
				
				<?php $regions 	= do_get_supplier_region_markup($supplier->ID);?>
				<?php $cats 	= do_get_supplier_cat_markup($supplier->ID);?>

			<p>Supplier: <a href="<?php echo get_the_permalink($supplier->ID)?>"><?php echo get_the_title($supplier->ID);?></a></p>
			<p><span class="supplier-region">Region: <span><?php echo $regions;?></span></span></p>
			<p><span class="supplier-cat">Category: <span><?php echo $cats;?></span></span></p>
		
			<p><a href="<?php echo get_the_permalink($supplier->ID)?>" class="cta">View Profile</a></p>

		</div>
					
	</div>

	<?php
}
function do_do_supplier_widget($the_title = null, $which_supplier = null, $supplier_id = null, $showSupplier = null, $show_latest_suppliers = null){

	$supplier_url = get_the_permalink(884);

	?>
	<div class="pure-g dco-content stakeholder-widget">
		
	<div class="pure-u-24-24 stakeholderContent column-width-widget">

		<div class="pure-u-24-24">		
						
			<h2 class="line-along">

				<?php if($the_title): ?>
					<?php echo strtoupper($the_title); ?> <?php do_do_help_icon(get_field('content_of_supplier_information_icon', 'options')); ?>
				<?php endif?>						
						
			</h2>		

		</div>

		<div class="padding">
					
			<form id="search_supps" method="get" action="<?php echo $supplier_url; ?>">

				<div class="pure-g search-box">

					<div class="pure-u-1">
					
						<div class="padding">
						
							<?php $category_object = get_field_object('field_57a1c565aa93d', 826);?>
									
							Search by Category <br>

							<select id="category_search" name="category_search" onchange="doCatSearch()" style="width:100%">

								<option value="-1">(All)</option>
							
									<?php foreach($category_object['choices'] as $key => $value):?>
								
										<option <?php echo $_GET['category_search'] == $key ? 'selected' : ''?> value="<?php echo $key;?>"><?php echo $value?></option>
							
									<?php endforeach;?>
				
							</select>
					
						</div>
				
					</div>
				
					<div class="pure-u-1">
					
						<div class="padding">
						
							<?php $region_object = get_field_object('field_57a1c39c5911a', 826);?>
							Search by Region<br>
						
							<select id="region_search" name="region_search" onchange="doRegionSearch()" style="width:100%">
							
								<option value="-1">(All)</option>
							
								<?php foreach($region_object['choices'] as $key => $value):?>
													
									<option <?php echo $_GET['region_search'] == $key ? 'selected' : ''?> value="<?php echo $key;?>"><?php echo $value?></option>
							
								<?php endforeach;?>
						
							</select>
					
						</div>
						
					</div>

					<div class="pure-u-1">
					
						<div class="padding">
						
							Search by Name<br>
							
							<input id="company_search" name="company_search" value="<?php echo $_GET['company_search'] ? $_GET['company_search'] : "";?>">
					
						</div>
								
					</div>

					

				</div>

				<?php if($showSupplier):?>
					<div class="pure-u-1">
						
						<div class="padding">
							
							<?php do_do_supplier_mini($which_supplier, $supplier_id, false ); ?>
						
						</div>
									
					</div>
				<?php endif; ?>

				<?php if($show_latest_suppliers):?>
					
					<div class="pure-u-1">
						
						<div class="padding">			
							
							<div class="pure-g">	
								
								<?php $suppliers = do_post_by_custom_post('supplier', 2);?>
								
								<div class="pure-u-1">
										
									<p style="color: #fff; margin-top:0;padding-top:0">Latest Suppliers:</p>

								</div>
								
								<?php foreach ($suppliers as $supplier):?>

									<?php 
										$image 			= get_field('logo', $supplier->ID);
										$s_url 			= get_the_permalink($supplier->ID);
										$uniqid_thumb 	= uniqid('supplier-thumb-');
										?>

										<div class="pure-u-1 pure-u-md-12-24">
											
											<div class="sh-icon-list-container" style="margin:2px">
												
												<a href="<?php echo $s_url; ?>"><img class="sh-icon-list-image <?php echo $uniqid_thumb; ?>" style="padding:2px; background-color: #fff" src="<?php echo $image['sizes']['profile-logo']?>" /></a>
											
											</div>

										</div>

										<script type="text/javascript">
						
											var the_thumb_tip = jQuery('.<?php echo $uniqid_thumb ?>').tooltipster({
					   							
					   							distance: 40,
					   							maxWidth : 400,
					   							contentAsHTML : true,
					   							content : '<?php echo $supplier->post_title; ?>'
					   							
											});

										</script>

									<?php endforeach; ?>

							</div>
						
						</div>
					
					</div>
				
				<?php endif;?>

				<div class="pure-u-1">

					<div class="padding" style="text-align:center">

						<a style="width:100%; display: inline-block;" class="cta" href="<?php  echo $supplier_url; ?>">View Full Directory</a>

					</div>
				
				</div>

			</form>

		</div>
				
	</div>
	
</div>

<script>

	function doCatSearch() {

			window.location.href = '<?php echo $supplier_url; ?>?category_search=' + jQuery('#category_search').val();

	}

	function doRegionSearch(){

			window.location.href = '<?php echo $supplier_url; ?>?region_search=' + jQuery('#region_search').val();
			
	}

</script>
<?php
}

function do_do_stakeholder_widget($the_title = null, $which_stakeholder = null, $stakeholder_id = null, $showStakeholder = null, $hide_excerpt = null ){

	?>

	<div class="pure-g dco-content stakeholder-widget">
		
		<div class="pure-u-24-24 stakeholderContent column-width-widget">

			<div class="pure-u-24-24">		
						
				<h2 class="line-along">

					<?php if($the_title): ?>
						<?php echo strtoupper($the_title); ?> <?php do_do_help_icon(get_field('content_of_stakeholder_information_icon','options')); ?>
					<?php endif?>						
						
				</h2>		

			</div>

			<div class="padding">
					
				<form id="search_supps" method="get" action="<?php echo get_the_permalink(884)?>">

					<div class="pure-g search-box">

						<div class="pure-u-1">
					
							<div class="padding">

								<?php do_do_stakeholder_mini($which_stakeholder, $stakeholder_id, $hide_excerpt); ?>
					
							</div>
								
						</div>

						<?php if($showStakeholder):?>

							<div class="pure-u-1">

								<div class="padding">

									<?php do_stakeholder_select_box(false); ?>

								</div>

							</div>
					
						<?php endif; ?>
				

					<div class="pure-u-1">

						<div class="padding" style="text-align:center">

							<a style="width:100%; display: inline-block;" class="cta" href="<?php  echo get_the_permalink(872); ?>">Go to Stakeholder Overview</a>

						</div>

					</div>

				</div>

			</form>

		</div>
				
	</div>
	
</div>

<script>

	function doCatSearch() {

			window.location.href = '<?php echo get_the_permalink(884)?>?category_search=' + jQuery('#category_search').val();

	}

	function doRegionSearch(){

			window.location.href = '<?php echo get_the_permalink(884)?>?region_search=' + jQuery('#region_search').val();
			
	}

</script>

<?php
}

function do_get_distrubution(){

	global $post;

	$dist = array();

	if(!get_field('distribution_ratio', $post->ID) || get_field('distribution_ratio', $post->ID) == 0){
		$dist['left'] = get_field('distribution_ratio_gen', 'options');
	}else{
		$dist['left'] = get_field('distribution_ratio', $post->ID);
	}

	$dist['right'] = 24 - intval($dist['left']);

	return $dist;
}