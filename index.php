<?php
/**
 * @package WordPress
 * @subpackage HTML5-Reset-WordPress-Theme
 * @since HTML5 Reset 2.0
 */
 get_header(); ?>

 <?php
  $do_paged = isset( $_GET['do_paged']) ? $_GET['do_paged'] : 1; 
  $query = do_pre_nav($do_paged);
  ?>
		
  <div class="level pure-g dco-content do-blog">

 	<div class="pure-u-1 pure-u-md-12-24">

 		<div class="padding-right padding-top">

			<?php if ($query->have_posts()) : session_start(); $_SESSION['IDS'] = array(); while ($query->have_posts()) : $query->the_post(); ?>


				<?php
				array_push($_SESSION['IDS'], $post->ID);
				
				$thumb = 'single-post-feature';
				$image = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID ), $thumb );
				$term = get_field('news_section',$post->ID);
				$termName = get_term_by('id', $term, 'news_section');
				$termName = $termName->name; ?>

		
			<div class="main-feature-container title-outside">
				
				<?php if($image):?>

					<div class="title-container">

						<div class="padding-vertical">

							<?php do_do_post_title($post->ID, $term, $termName)?>
				
						</div>

					</div>

					<a href="<?php echo get_the_permalink($post->ID) ?>"><img class="wow fadeInUp" src="<?php echo $image[0]?>"></a>

				<?php else:?>

					<div class="no-image-title-container">

						<?php do_do_post_title($post->ID, $term, $termName)?>
						
					</div>

				<?php endif; ?>

			</div>
			
			<p><?php echo do_get_content_extract($post->ID, get_field('wordcount_for_post_excerpt_in_news_pages', 'options'), '<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>'); ?></p>

			<?php edit_post_link(__('Edit this entry','html5reset'),'<br>','.'); ?>

			<hr>

		<?php endwhile; ?>

		<?php do_do_nav($do_paged, $query->found_posts );?>

		</div>
	
	</div>

	<div class="pure-u-1 pure-u-md-12-24 do-sidebar">

	 		<?php get_do_sidebar(); ?>

	 </div>



	<?php else : ?>

		<h2><?php _e('Nothing Found','html5reset'); ?></h2>

	<?php endif; ?>

		
	</div>

<?php get_footer(); ?>
