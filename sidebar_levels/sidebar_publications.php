<?php 
	
	$unique_id = uniqid();

?>

<div class="pure-g dco-content <?php echo $unique_id;  ?>">
	
	<div class="pure-u-24-24">
		
		<h2 class="line-along">
			
			<?php if($title = get_sub_field('title')): ?>
				<?php echo strtoupper($title); ?>
			<?php endif?>						
		</h2>
	
	</div>

	<?php $latest_pubs = do_get_latest_posts('publication', 2);?>

	<div class="pure-u-24-24">

		<?php if($content = get_sub_field('sidebar_content', 'options')):?>
			<?php echo $content; ?>
		<?php endif?>

	</div>
	
	<?php foreach ($latest_pubs as $pub): ?>
		
		<div class="pure-u-12-24">
			
			<div class="padding">

				<div class="pub-container">
				   				
				   	<?php $image = get_field('front_cover', $pub->ID); ?>
				   	
				   	<?php if($image):?>	
				   		
				   		<div class="pub-image-container">

				   			<a href="<?php echo get_the_permalink($pub->ID)?>">

				   					<img style="width:100%; height: auto" src="<?php echo $image['sizes']['large']?>"/>

				   			</a>

				   		</div>
				   	
				   	<?php endif;?>
				   			
				   	<p class="pub-description"><?php echo get_field('description', $pub->ID); ?></p>
				   	<p><a href="<?php echo get_the_permalink($pub->ID)?>" class="cta">VIEW</a></p>

				   </div>
			</div>

		</div>

	<?php endforeach; ?>
		
</div>

<script>

	jQuery(document).imagesLoaded( function() {
		get_max_height_from_set('.<?php echo $unique_id;  ?> .pub-image-containerr',0);
		get_max_height_from_set('.<?php echo $unique_id;  ?> .pub-description',0);

	});

</script>