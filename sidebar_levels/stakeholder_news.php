<div class="pure-g dco-content stakeholder-widget">
		
	<div class="pure-u-24-24 stakeholderContent">

		<div class="pure-u-24-24">		
						
			<h2 class="line-along">

				<?php if($title = get_sub_field('title')): ?>
					<?php echo strtoupper($title); ?> <?php do_do_help_icon(get_field('content_of_stakeholder_information_icon','options')); ?>
				<?php endif?>						
						
			</h2>		

		</div>

		<div class="padding white-stakeholder-content">
					
			<?php 	$stakeholderFeedID 	= get_cat_ID( 'Stakeholders' );
					$subfeatured 		= do_get_category_posts($stakeholderFeedID, get_sub_field('number_of_articles_to_show'));

					$args = array(
						'thumb' => 'sub-feature',
						'stakeholder' => true,
						'animate' => false,
					);

					if($showExcerpt = get_sub_field('show_excerpt')){
						$args['excerpt'] = true;
					}
					
				?>

				<div class="pure-g">

					<?php foreach ($subfeatured	 as $sub_feature) :?>
						
					<div class="pure-u-1">
					
						<div class="padding">
							
							<?php do_sub_feature_panel($sub_feature, $args); ?>
						</div>
								
					</div>

					<?php endforeach;?>

				</div>

		</div>
				
	</div>
	
</div>