<?php 
	
	$unique_id = uniqid();

?>

<div class="pure-g dco-content <?php echo $unique_id;  ?>">
		<div class="pure-u-24-24">
						<h2 class="line-along">
						<?php if($title = get_sub_field('title')): ?>
							<?php echo strtoupper($title); ?>
						<?php endif?>						
						</h2>
					</div>

		<?php $related_posts = do_get_related_posts(get_sub_field('max_amount_to_show'));?>

		<?php if(get_sub_field('show_excerpt')){
				$args = array(
					'excerpt' => true
				);
				
			}?>

		<?php foreach ($related_posts as $related_posts): ?>
		
		<div class="pure-u-12-24 sub-feature">
			<div class="padding">
				<?php if(get_sub_field('layout_type') == 'big_images' || !get_sub_field('layout_type')):?>
					<?php do_sub_feature_panel($related_posts, $args); ?>
				<?php else:?>
					<?php do_small_feature_panel($related_posts, $args);?>
				<?php endif; ?>
			</div>
		</div>

		<?php endforeach; ?>
		
</div>

<script>

	jQuery(document).imagesLoaded( function() {
		get_max_height_from_set('.<?php echo $unique_id;  ?> .sub-image-container',0);
		get_max_height_from_set('.<?php echo $unique_id;  ?> .slide-title',0);
		get_max_height_from_set('.<?php echo $unique_id;  ?> .posted',0);
		get_max_height_from_set('.<?php echo $unique_id;  ?> .sub-feature-title-container',0);
		get_max_height_from_set('.<?php echo $unique_id;  ?> .sub-feature-excerpt',0);
	});

</script>