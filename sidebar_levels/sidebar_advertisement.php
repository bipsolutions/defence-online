<?php 

	$unique_id = uniqid();

	$style = generate_ad_padding(get_sub_field('top_padding'),get_sub_field('bottom_padding'),get_sub_field('left_padding'),get_sub_field('right_padding'));

?>
<div class="pure-u-24-24">

	<h2 class="line-along">

		<?php if($title = get_sub_field('title')): ?>

			<?php echo strtoupper($title); ?>

		<?php endif?>	

	</h2>

</div>

<div class="pure-g dco-content level-advertisement <?php echo $unique_id; ?>">
		
	<div class="pure-u-24-24">

		<div style="<?php echo $style; ?>" class="double-padding ad-content-container">

			<?php echo $content = remove_autop(get_sub_field('sidebar_content'));?>
			

		</div>

	</div>
		
</div>