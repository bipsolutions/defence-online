
<?php 
	
	$which_supplier = get_sub_field('which_supplier_to_show');
	$the_title 		= get_sub_field('title');
	$supplier_id	= $which_supplier == 'specific' ? get_sub_field('which_supplier') : null;

	do_do_supplier_widget($the_title, $which_supplier, $supplier_id, true);

?>
