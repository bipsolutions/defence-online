<?php
		$unique_id = uniqid(); 
 ?>
<div class="pure-g dco-content stakeholder-widget <?php echo $unique_id;?>">
		
	<div class="pure-u-24-24 stakeholderContent">

		<div class="pure-u-24-24">		
						
			<h2 class="line-along">

				<?php if($title = get_sub_field('title')): ?>
					<?php echo strtoupper($title); ?> <?php do_do_help_icon(get_field('content_of_stakeholder_information_icon','options')); ?>
				<?php endif?>						
						
			</h2>		

		</div>

		<div class="padding">
					
			

				<div class="pure-g">

					<div class="pure-u-1">
					
						<div class="padding">
							
							<?php

								switch (get_sub_field('what_stakeholders_to_show')) {
								
								case 'latest':

									$stakeholdersObj 	= new do_stakeholder;
									$stakeholders 		= $stakeholdersObj->get_all(null, true); ?>

									<div class="pure-g">

										<?php foreach ($stakeholders as $stakeholder): ?>

											<?php $image 	= get_field('sh_company_logo', 'user_' . $stakeholder->ID);?>

											<?php $sh_url 	= get_the_permalink(get_the_stakeholder_id_by_author($stakeholder->ID));?>

												<div class="pure-u-1 pure-u-md-12-24">
													<div class="sh-icon-list-container" style="margin:2px">
														<a href="<?php echo $sh_url; ?>"><img class="sh-icon-list-image" style="padding:2px; background-color: #fff" src="<?php echo $image['sizes']['profile-logo']?>" /></a>
													</div>
												</div>

										<?php endforeach; ?>

									</div>

								<?php

									break;

								case 'specific':

									$stakeholders = get_sub_field('pick_and_order_stakeholders');?>
									<div class="pure-g">
									<?php foreach ($stakeholders as $stakeholder): ?>
											<?php $stakeholder_id = do_get_stakeholder_id($stakeholder);?>

											<?php $image 	= get_field('sh_company_logo', 'user_' . $stakeholder_id);?>

									<?php $sh_url 	= get_the_permalink($stakeholder->ID);?>

										<div class="pure-u-1 pure-u-md-12-24">
											<div class="sh-icon-list-container" style="margin:2px">
												<a href="<?php echo $sh_url; ?>"><img class="sh-icon-list-image" style="padding:2px; background-color: #fff" src="<?php echo $image['sizes']['profile-logo']?>" /></a>
											</div>
										</div>
										<?php endforeach; ?>

									</div>
									<?php

								break;

								default:
									
								break;
							}
							?>
							
						</div>
								
					</div>

					<div class="pure-u-1">
						<div class="padding" style="text-align:center">
							<a style="width:100%; display: inline-block;" class="cta" href="<?php  echo get_the_permalink(872); ?>">Go to Stakeholder Overview</a>
						</div>
					</div>

				</div>

			

		</div>
				
	</div>
	
</div>

<script>

		jQuery(document).imagesLoaded( function() {
			
			get_max_height_from_set('.<?php echo $unique_id;?> .sh-icon-list-container',0);

		jQuery('.<?php echo $unique_id;?> .sh-icon-list-container').each(function(){

			alignHeight(jQuery(this), jQuery(this).find('img')); 

		});
	});

</script>