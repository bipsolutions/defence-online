<?php 
	
	$the_title = get_sub_field('title');
	$which_stakeholder = get_sub_field('which_stakeholder_to_show');
	$stakeholder_id = $which_stakeholder == 'specific' ? get_sub_field('which_stakeholder') : null;
	$hide_excerpt = get_sub_field('hide_stakeholder_excerpt');
	do_do_stakeholder_widget($the_title , $which_stakeholder, $stakeholder_id, true, $hide_excerpt);

 ?>

