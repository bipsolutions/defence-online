<?php
	 $unique_id = uniqid();
	 if(!$_SESSION['exclude_ids']){
		$_SESSION['exclude_ids'] = array();
	}
?>

<div class="pure-g dco-content <?php echo $unique_id;?>">

		<div class="pure-u-24-24">
			
			<h2 class="line-along">
				
				<?php if($title = get_sub_field('title')): ?>
					
					<?php echo strtoupper($title); ?>
				
				<?php endif?>						
			
			</h2>
		
		</div>

		<?php $sections = do_get_section_posts(get_sub_field('section_to_show'), get_sub_field('max_amount_to_show'));?>

		<?php if(get_sub_field('show_excerpt')){
				$args = array(
					'excerpt' => true
				);
				
			}?>

		<?php foreach ($sections as $section): ?>
			
			
			<?php $responsiveClass = get_sub_field('layout_type') == 'big_images' ? "pure-u-12-24" : "pure-u-24-24 pure-u-sm-12-24 pure-u-md-12-24 pure-u-lg-12-24 pure-u-xl-12-24" ?>
		

			<div class="<?php echo $responsiveClass; ?> sub-feature">

				<div class="padding">

					<?php if(get_sub_field('layout_type') == 'big_images' || !get_sub_field('layout_type')):?>

						<?php do_sub_feature_panel($section, $args); ?>

					<?php else:?>

						<?php do_small_feature_panel($section, $args);?>

					<?php endif; ?>

				</div>

			</div>

		<?php endforeach; ?>
		
</div>

<script>

	jQuery(document).imagesLoaded( function() {
		get_max_height_from_set('.<?php echo $unique_id;  ?> .sub-image-container',0);
		get_max_height_from_set('.<?php echo $unique_id;  ?> .slide-title',0);
		get_max_height_from_set('.<?php echo $unique_id;  ?> .posted',0);
		get_max_height_from_set('.<?php echo $unique_id;  ?> .sub-feature-title-container',0);
		get_max_height_from_set('.<?php echo $unique_id;  ?> .sub-feature-excerpt',0);
	});

</script>