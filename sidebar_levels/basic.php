<div class="pure-g dco-content">
		
		<div class="pure-u-24-24">

				<div class="pure-u-24-24">
					
						
						<h2 class="line-along">

						<?php if($title = get_sub_field('title')): ?>
							<?php echo strtoupper($title); ?>
						<?php endif?>						
						
						</h2>
					

				</div>

				<div class="padding">
					
					<?php the_sub_field('sidebar_content')?> 

				</div>
				

		</div>
		
</div>