<?php
/**
 * @package WordPress
 * @subpackage HTML5-Reset-WordPress-Theme
 * @since HTML5 Reset 2.0
 */
 get_header(); ?>

<?php if(do_can_user_view_page()): ?>

 <div class="level">

 	<div class="pure-g">
 	 		
 	 		<div class="pure-u-24-24">
 	 						
 	 			<div class="page-header">
 	 				
 	 				<h2 class="line-along"><?php echo strtoupper(get_the_title()) ; ?></h2>
 	 				<a class="cta backbutton hide-xs hide-sm" href="<?php echo get_the_permalink(322); ?>">Back to Events List</a>
 	 			
 	 			</div>
 	
 	 		</div>
 	
 	 	</div>

 </div>

 <div class="level pure-g dco-content do-suppier-single">

 	

 	<div class="pure-u-1 pure-u-md-14-24">

 		<div class="padding-horizontal">

			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

				<?php the_content();?>

			<?php endwhile; endif; ?>

 		</div>
 		
 	</div>

	 <div class="pure-u-1 pure-u-md-10-24 do-sidebar">

	 		<div class="pure-g grey">

	 				<div class="pure-u-24-24">

	 					<h2>Event Detail</h2>

						<h3>Event Date</h3>
						
						<p><?php the_field('date');?></p>
						
						<h3>Event Location</h3>
						
						<p><?php the_field('venue');?> - <?php echo get_field('location')['address'] ;?></p>	
	 				</div>

	 		</div>
	 		
	 		<div class="pure-g grey product-servies">
	 			
	 			<div class="pure-u-24-24">
	 				
	 				<h2>Map</h2>

	 				<?php  echo do_do_maps(array($post->ID), 'location') ?>

	 			</div>

	 		</div>
	 		
	 </div>

 </div>

<?php else: ?>
	<!-- What to do if the content is gated -->
	<?php include_once('access.php'); ?>

<?php endif; ?>
	
<?php get_footer(); ?>