<?php
/**
 * @package WordPress
 * @subpackage HTML5-Reset-WordPress-Theme
 * @since HTML5 Reset 2.0
 */
?><!doctype html>

<!--[if lt IE 7 ]> <html class="ie ie6 ie-lt10 ie-lt9 ie-lt8 ie-lt7 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7 ]>    <html class="ie ie7 ie-lt10 ie-lt9 ie-lt8 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8 ]>    <html class="ie ie8 ie-lt10 ie-lt9 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 9 ]>    <html class="ie ie9 ie-lt10 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 9]><!--><html class="no-js" <?php language_attributes(); ?>><!--<![endif]-->
<!-- the "no-js" class is for Modernizr. -->

<head data-template-set="html5-reset-wordpress-theme">

	<meta charset="<?php bloginfo('charset'); ?>">

	<!-- Always force latest IE rendering engine (even in intranet) -->
	<!--[if IE ]>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<![endif]-->

	<?php
		if (is_search())
			echo '<meta name="robots" content="noindex, nofollow" />';
	?>

	<title><?php wp_title( '|', true, 'right' ); ?></title>

	<meta name="title" content="<?php wp_title( '|', true, 'right' ); ?>">

	<!--Google will often use this as its description of your page/site. Make it good.-->
	<meta name="description" content="<?php bloginfo('description'); ?>" />

	<meta name="Copyright" content="Copyright &copy; <?php bloginfo('name'); ?> <?php echo date('Y'); ?>. All Rights Reserved.">
	<meta name="viewport" content="width=device-width, initial-scale=1.0 minimal-ui">

	<!-- concatenate and minify for production -->
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/reset.css" />
	<link rel="stylesheet" href="<?php echo get_stylesheet_uri(); ?>" />

	<!-- Lea Verou's Prefix Free, lets you use only un-prefixed properties in yuor CSS files -->
    <script src="<?php echo get_template_directory_uri(); ?>/_/js/prefixfree.min.js"></script>

	
	<script src="<?php echo get_template_directory_uri(); ?>/_/js/modernizr-2.8.0.dev.js"></script>

	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

	<?php wp_head(); ?>
	<script src="https://npmcdn.com/imagesloaded@4.1/imagesloaded.pkgd.min.js"></script>

	<script src="<?php bloginfo('template_directory'); ?>/_/js/header-functions.js"></script>

	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,300italic,400italic,600italic,700' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,300italic,700' rel='stylesheet' type='text/css'>

	<!--[if gt IE 8]><!-->
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/grids-responsive.css">
	<!--[endif]---->
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/unslider.css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/unslider-dots.css">
	<script src="<?php echo get_template_directory_uri(); ?>/js/unslider-min.js"></script>

	<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url')?>/css/tooltipster.bundle.css" />
	<script type="text/javascript" src="<?php bloginfo('template_url')?>/js/tooltipster.bundle.min.js"></script>
	
	<?php do_form_session(); session_start();?>
	
	<script src='https://www.google.com/recaptcha/api.js'></script>

</head>



<body <?php body_class(); ?>>
		<?php do_action( 'do_do_after_body' ); ?>
		
		<nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left">
	
		<h3>Menu</h3>

		<?php $walker = new do_walker; 
			
			wp_list_pages(array('walker' => $walker, 'title_li' => '')); ?>

			<?php if($thelink == get_field('header_button_url','options')):?>
			<li>
				<a href="<?php echo $thelink;?>" class="cta-full">
					<?php echo strtoupper(get_field('header_button','options'));?>
				</a>
			</li>
		<?php endif;?>

		<?php if(is_user_logged_in()): ?>
			
			<li>
				<a href="<?php echo wp_logout_url( get_bloginfo('url') ); ?>" class="loginButton">Logout</a>
			</li>

		<?php else: ?>

			<li>
				<a href="<?php echo wp_login_url( home_url() ); ?>" title="Login">Login</a>
			</li>
		<?php endif;?>

		<li style="padding:10px">
			
				<?php get_search_form( true ); ?>
			
		</li>

	</nav>
<div class='container'>
	<?php

	
	if( have_rows('levels', 'options') ):

	     // loop through the rows of data
	    while ( have_rows('levels', 'options') ) : the_row();

	        if( get_row_layout() == 'advertisement' ):

	        	?>

	        	<?php 

					$unique_id = uniqid();

				?>
				<div class="level level-advertisement <?php echo $unique_id; ?>">

					<div class="pure-g">

						<div class="pure-u-24-24">

							<div class="double-padding ad-content-container">

								<?php the_sub_field('content');?>

								
							</div>	

						</div>	

					</div>

				</div>
				

	        	<?php

	        endif;

	    endwhile;

	else :    

	endif;

	?>


	<div id="header" class="level level-nav-bar level-nav-bar-normal">

		<div class="pure-g">

			<div class="pure-u-4-24 hide-lg hide-xl">

				<button id="nav-toggle" class="toggle-menu menu-left "><span></span></button>

			</div>

			<div class="pure-u-18-24 hide-xs hide-sm hide-md">

				<?php $image = get_field('inverted_site_logo', 'options'); ?>

				<div class="fixed-logo-container">
					<a href="<?php bloginfo('url'); ?>" title="Return to the homepage">
						<img src="<?php echo $image['sizes']['mini-logo'] ?>" />
					</a>
				</div>

				<nav id="nav" role="navigation">

					<ul class="sf-menu">
						<?php

							$walker = new do_walker; 
							wp_list_pages(array('walker' => $walker, 'title_li' => '')); ?>

					</ul>

				</nav>

			</div>

			<div class="pure-u-20-24 pure-u-lg-6-24 session-nav">

					
					<?php if(!is_user_logged_in()):?>
			
							<?php do_login_button('Login', true); ?>

							<?php do_login_button('Register', true); ?>
							
					<?php else: ?>

							<?
							$current_user_id = get_current_user_id();
							$user_info = get_userdata($current_user_id);
							?>

							<a href="<?php echo wp_logout_url( $redirect ); ?>" ><i class="fa fa-sign-out" aria-hidden="true"></i>LOGOUT</a>
							<?php if( in_array('stakeholder', $user_info->roles) ):?>
								<a href="<?php bloginfo('url');?>/stakeholder-admin/"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>STAKEHOLDER ADMIN</a>
							<?php endif;?>
					<?php endif ?>
			</div>

		</div>

	</div>
	
	<div class="level level-header takeover-container">

		<div class="pure-g">

			<div class="pure-u-1 pure-u-lg-18-24">

				<div class="pure-g">

					<div class="pure-u-1 pure-u-md-9-24">
						<div id="main-logo-container">
							<h1><a href="<?php bloginfo('url'); ?>" title="Return to the homepage"><?php bloginfo('name'); ?></a></h1>
						</div>
					</div>

					<div class="pure-u-15-24 hide-xs hide-sm free-content">
							<?php the_field('header-free-content','options'); ?>
							
					</div>

				</div>
			</div>
			<div class="pure-u-6-24 header-right hide-xs hide-sm hide-md">

				<div class="padding-no-right" style="margin-top: 10px">

					<div class="pure-g">

					
						<div class="pure-u-15-24 header-search-box">
							<?php get_search_form(); ?>
						</div>

						<div class="pure-u-1-24">
							
						</div>


						<div class="pure-u-8-24">
							<a href="<?php the_field('header_button_url_two','options');?>" class="cta-full cta-red">
									<?php echo strtoupper(get_field('header_button_two','options'));?>
								</a>
						</div>

						<div class="pure-u-24-24 blank-row">
								&nbsp;
						</div>

						<div class="pure-u-24-24">
							<a href="<?php the_field('header_button_url','options');?>" class="cta-full">
								<?php echo strtoupper(get_field('header_button','options'));?>
							</a>
						</div>
						
						
						
					</div>

				</div>
					
			</div>

		</div>

	</div>

