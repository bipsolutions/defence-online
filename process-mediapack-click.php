<?php
require_once('../../../wp-load.php');

$id 		= $_POST['userid'];
$postid 	= $_POST['source'];

update_user_meta($id, 'mediapack_downloaded', 1);

update_user_meta($id, 'mediapack_downloaded_from', $postid);

echo json_encode($id);

?>