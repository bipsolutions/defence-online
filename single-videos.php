<?php
/**
 * @package WordPress
 * @subpackage HTML5-Reset-WordPress-Theme
 * @since HTML5 Reset 2.0
 */

acf_form_head();

get_header(); ?>

<div class="pure-g dco-content do-videos">

 	


 			<div class="pure-u-24-24">
				<h2 class="line-along"><?php echo strtoupper( get_the_title()	) ; ?></h2>
				
			</div>

 			<div class="pure-u-24-24 pure-u-md-16-24">
 					<?php $url = get_field('video_url'); ?>
 					<?php echo getEmbedVideo($url);?>
 			</div>

 			<div class="pure-u-24-24 pure-u-md-8-24 other-videos">
 				<div class="padding-horizontal">
 					<h3>Description</h3>
 					<?php the_field('video_content');?>
 					<h3>Other Video's</h3>
 					<div class="pure-g">
 					

 								<?php 
 								  $videos = get_posts(array('posts_per_page'	=> 4,
 								  		'orderby' 		 	=> 'rand',
 								  		'post_type'        => 'videos',)
 								  		
 								  );

 								  foreach ($videos as $video) :?>
 								 
		 							<div class="pure-u-1 pure-u-md-12-24">
		 								
		 								<div class="img-container">
										
											<?php do_do_video_thumbnail_with_link($video); ?>

										 </div>
										 
									</div>

 								  <?php endforeach; ?>
 								

								 <div class="pure-u-1">
 								 <p> <a class="cta" href="<?php echo get_the_permalink(308);?>">View All Featured Videos</a></p>
 								</div>
 								


							</div>
						</div>
 				</div>
 					
 			</div>

 		

 	</div>


<?php get_footer(); ?>
