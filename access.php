<?php
/**
 * @package WordPress
 * @since HTML5 Reset 2.0
 */
?>


<div class="pure-g">

	<div class="pure-u-1">

		<div class="padding">

			<div class="scotia-item-content">

				<?php if($header_title = get_field('page_restricted_header','do-journey')): ?>

						<div class="pure-u-24-24">
							<div class="page-header">
									<h2 class="line-along"><?php echo $header_title; ?></h2>
							</div>
						</div>

				<?php endif;?>

				<?php the_field('restricted_text', 'do-journey')?>

				<p>
					<?php do_do_login_modal(); ?>
				</p>

			</div>

		</div>

	</div>

</div>