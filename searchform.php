<form role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
    <div class="dco-searchform">
        <input type="search" id="s" name="s" value="" />
        <input type="submit" value="<?php _e('Search','html5reset'); ?>" id="searchsubmit" tabindex="-1" />
    </div>
</form>