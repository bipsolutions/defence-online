<?php
  global $wp_query;
  $sh_id = $wp_query->queried_object->post_author;
?>

<div class="level level-footer blue">
        
                <div class="pure-g">

                    <div class="pure-u-24-24">
                      
                        <div class="table">
                            <ul class="stakeholder-footer-nav">
                               <?php echo do_stakeholder_menu($sh_id); ?>
                            </ul>
                        </div>
                        
                    </div>

                   

                <div class="pure-u-24-24">

                    <?php 

                      $footlogos = get_field('footer_logos', 'options');
                      
                      if($footlogos):?>

                          <?php while( have_rows('footer_logos','options') ): the_row(); ?>
                          
                          <?php $image = get_sub_field('image');?>

                              <h3 class="center"><?php the_sub_field('title'); ?></h3>
                              <p class="center"><img src="<?php echo $image['sizes']['slider-logo']; ?>"></p>

                          <?php endwhile;?>

                      <?php endif; ?>
                </div>

                 <div class="pure-u-24-24">
                    
                     <footer id="footer" class="source-org vcard footerDisclaimer footerDisclamerResponsive" role="contentinfo">

                              <?php the_field('disclamer','options')?>

                     </footer>

                </div>
                    
            </div>

          
      
</div>

	<?php wp_footer(); session_start(); unset($_SESSION['exclude_ids']); unset($_SESSION['validate_username']); unset($_SESSION['validate_email'])?>

  <?php do_action('do_do_footer'); ?>
  
<script src="<?php bloginfo('template_directory'); ?>/_/js/functions.js"></script>
    
</body>

</html>
