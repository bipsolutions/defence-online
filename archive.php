<?php
/**
 * @package WordPress
 * @subpackage HTML5-Reset-WordPress-Theme
 * @since HTML5 Reset 2.0
 */

global $wp_query;

if($wp_query->query["author_name"]){
	
	get_header('stakeholder');

}else{
	
	get_header();
	
}

$column_distubution = do_get_distrubution();
$do_paged = isset( $_GET['do_paged']) ? $_GET['do_paged'] : 1;
 
do_pre_nav($do_paged);

?>

	<div class="level pure-g">
 		
 		<div class="pure-u-24-24">
 			
 			<div class="page-header">
 				
 				<?php if (have_posts()) : ?>

 					<?php $post = $posts[0]; // Hack. Set $post so that the_date() works. ?>

					<?php /* If this is a category archive */ if (is_category()) { ?>
					
						<h2 class="line-along"><?php _e('Archive for the','html5reset'); ?> &#8216;<?php single_cat_title(); ?>&#8217; <?php _e('Category','html5reset'); ?></h2>

					<?php /* If this is a tag archive */ } elseif( is_tag() ) { ?>
						
						<h2 class="line-along"><?php _e('Posts Tagged','html5reset'); ?> &#8216;<?php single_tag_title(); ?>&#8217;</h2>

					<?php /* If this is a daily archive */ } elseif (is_day()) { ?>
						
						<h2 class="line-along"><?php _e('Archive for','html5reset'); ?> <?php the_time('F jS, Y'); ?></h2>

					<?php /* If this is a monthly archive */ } elseif (is_month()) { ?>
				
						<h2 class="line-along"><?php _e('Archive for','html5reset'); ?> <?php the_time('F, Y'); ?></h2>

					<?php /* If this is a yearly archive */ } elseif (is_year()) { ?>
						
						<h2 class="line-along"><?php _e('Archive for','html5reset'); ?> <?php the_time('Y'); ?></h2>

					<?php /* If this is an author archive */ } elseif (is_author()) { ?>
						
						<h1 class="line-along"><?php echo strtoupper('Latest News by ' . get_the_stakeholder_by_author($wp_query->query_vars['author'])) ; ?></h1>

					<?php /* If this is a paged archive */ } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) { ?>
						
						<h2 class="line-along"><?php _e('Blog Archives','html5reset'); ?></h2>
				
					<?php } ?>
			</div>
 
 		</div>
 
 	</div>
	
	<div class="pure-g dco-content do-blog">

 		<div class="pure-u-1 pure-u-md-<?php echo $column_distubution['left']; ?>-24">

 			<div class="padding-top padding-right">

 				<?php session_start(); $_SESSION['IDS'] = array(); ?>

				<?php while (have_posts()) : the_post(); ?>
			
				<?php
					array_push($_SESSION['IDS'], $post->ID);
				
					$thumb = 'single-post-feature';
					$image = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID ), $thumb );
					$term = get_field('news_section',$post->ID);
					$termName = get_term_by('id', $term, 'news_section');
					$termName = $termName->name; 
					$alt_text = get_post_meta(get_post_thumbnail_id($post->ID), '_wp_attachment_image_alt', true);
				?>

		
				<div class="main-feature-container title-outside">
				
					<?php if($image):?>
						
						<a href="<?php echo get_the_permalink($post->ID) ?>"><img alt="<?=$alt_text; ?>" class="wow fadeInUp" src="<?php echo $image[0]?>"></a>
						
						<div class="title-container">

							<div class="padding-vertical">

								<?php do_do_post_title($post->ID, $term, $termName)?>
					
							</div>
								
						</div>


					<?php else:?>

						<div class="no-image-title-container">

							<?php do_do_post_title($post->ID, $term, $termName)?>
						
						</div>

					<?php endif; ?>

				</div>
			
				<p><?php echo do_get_content_extract($post->ID, get_field('wordcount_for_post_excerpt_in_news_pages', 'options'), '<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>'); ?></p>

				<?php edit_post_link(__('Edit this entry','html5reset'),'<br><br>','.'); ?>

				<p><hr></p>

			<?php endwhile; ?>

			<?php do_do_nav($do_paged, $wp_query->found_posts);?>

		</div>

	</div>

	<div class="pure-u-1 pure-u-md-<?php echo $column_distubution['right']; ?>-24 do-sidebar">

	 		<?php get_do_sidebar(); ?>
	 		
	 </div>

</div>
		
<?php else : ?>

	<h2><?php _e('Nothing Found','html5reset'); ?></h2>

<?php endif; ?>

<?php get_footer(); ?>
