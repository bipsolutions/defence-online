<?php
/*
Template Name: Suppliers Grid Page

*/
 get_header(); ?>

<div class="pure-g">
 		
 		<div class="pure-u-24-24">
 						
 			<div class="page-header">
 				
 				<h2 class="line-along"><?php global $post;?>
					<?php echo strtoupper(	$post->post_title ) ; ?></h2>

				<?php echo $content = apply_filters('the_content', get_the_content($post->ID));?>
 			
 			</div>

 		</div>

</div>
		
  <div class="pure-g dco-content do-suppliers">

 		<div class="pure-u-24-24">
				

				<form id="search_supps" method="get">

					<div class="pure-g search-box">

								<div class="pure-u-1 pure-u-md-8-24">
									<div class="padding">
									<?php $category_object = get_field_object('field_57a1c565aa93d', 826);?>
									
										Category <br><select id="category_search" name="category_search" onchange="doCatSearch()">
												<option value="-1">(All)</option>
												<?php foreach($category_object['choices'] as $key => $value):?>
													<option <?php echo $_GET['category_search'] == $key ? 'selected' : ''?> value="<?php echo $key;?>"><?php echo $value?></option>
												<?php endforeach;?>
				
										</select>
							  		</div>
							   </div>
								<div class="pure-u-1 pure-u-md-8-24">
										<div class="padding">
											<?php $region_object = get_field_object('field_57a1c39c5911a', 826);?>
											Region<br>
											<select id="region_search" name="region_search" onchange="doRegionSearch()">
											<option value="-1">(All)</option>
											<?php foreach($region_object['choices'] as $key => $value):?>
													
													<option <?php echo $_GET['region_search'] == $key ? 'selected' : ''?> value="<?php echo $key;?>"><?php echo $value?></option>
											<?php endforeach;?>
											</select>
										</div>
										
								</div>

								<div class="pure-u-1 pure-u-md-8-24">
										<div class="padding">
											Search<br><input id="company_search" name="company_search" value="<?php echo $_GET['company_search'] ? $_GET['company_search'] : "";?>">
										</div>
								
								</div>
						</div>


						
				</form>
				
			<?php if(isset($_GET['company_search'])): ?>
					<?php $suppliers = get_posts_by_search('supplier', 'company_search');?>
			<?php else:?>
					<?php $suppliers = do_post_by_custom_post('supplier' , -1, false, true); ?>
			<?php endif?>

			

			<?php if($suppliers):?>

				<div class="pure-g all-suppliers-container">
				
				<?php foreach ($suppliers as $supplier):?>

					<?php if($_GET['category_search'] && $_GET['category_search'] != -1):?>

							<?php if($cat_array = get_field('supplier_category', $supplier->ID)):?>
							
								<?php if(!in_array($_GET['category_search'], $cat_array) ):?>
									<?php continue; ?>
								<?php endif;?>

							<?php else:?>
									<?php continue;?>
							<?php endif;?>

					<?php endif;?>

					<?php if($_GET['region_search'] && $_GET['region_search'] != -1):?>

							<?php if($region_array = get_field('region', $supplier->ID)):?>
									<?php if(!in_array($_GET['region_search'], $region_array) ):?>
											<?php continue; ?>
									<?php endif;?>
							<?php else:?>
									<?php continue;?>
							<?php endif;?>
							
					<?php endif;?>

						<div class="pure-u-1 pure-u-md-12-24">
							
							<?php $image = get_field('logo',$supplier->ID);?>
								
								<div class="supplier-grid-outer" style="background: repeating-linear-gradient(45deg, rgba(255,255,255,1), 90%, rgba(255,255,255,0.8)), url(<?php echo $image['sizes']['main-feature']?>);">
								
									<div class="supplier-grid-inner">
									
										<div class="pure-g">

											<div class="pure-u-1">
												
												<h3 class="line-along"><?php echo strtoupper( $supplier->post_title	) ; ?></h3>
											
											</div>

											<div class="pure-u-1">
										
												<a href="<?php echo get_the_permalink($supplier->ID)?>"><img alt="<?=$image['alt'] ?>" class="padding" style="width:auto!important" src="<?php echo $image['sizes']['site-logo']?>" /></a>

												<?php $regions = do_get_supplier_region_markup($supplier->ID);?>
												<?php $cats = do_get_supplier_cat_markup($supplier->ID);?>
								
												<p><span class="supplier-region">Region: <span><?php echo $regions;?></span></span></p>
												<p><span class="supplier-cat">Category: <span><?php echo $cats;?></span></span></p>

												<p><a href="<?php echo get_the_permalink($supplier->ID)?>" class="cta">View Profile</a></p>

											</div>

										</div>

									</div>
							
								</div>	

							</div>
					
						<?php endforeach;?>

					</div>
				
				<?php else:?>

					<div class="pure-g all-suppliers-container">
						
						<div class="pure-u-24-24">
						
							<h3>No results found</h3>
						
						</div>
					
					</div>
				<?php endif;?>
			
			</div>
		
		</div>	
	
	</div>

<?php get_footer(); ?>

<script>
	function doCatSearch(){

			window.location.href = '?category_search=' + jQuery('#category_search').val();

	}

	function doRegionSearch(){
			window.location.href = '?region_search=' + jQuery('#region_search').val();
	}

	jQuery(document).imagesLoaded( function() {
		
		get_max_height_from_set('.supplier-grid-outer',0);
		
	});

</script>