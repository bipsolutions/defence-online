// Browser detection for when you get desparate. A measure of last resort.
// http://rog.ie/post/9089341529/html5boilerplatejs

// var b = document.documentElement;
// b.setAttribute('data-useragent',  navigator.userAgent);
// b.setAttribute('data-platform', navigator.platform);

// sample CSS: html[data-useragent*='Chrome/13.0'] { ... }


// remap jQuery to $
(function ($) {

	/* trigger when page is ready */
	jQuery(document).ready(function (){


		do_hidden_logo();

		do_linealong();

		var stakeholderSlider = jQuery('.stakeholder-slider').unslider(
			{
					arrows: false,
					nav: false,
			}
		);

		jQuery('.stakeholder-logo-container .click-left').click(function(){
			stakeholderSlider.unslider('prev');
		});
		jQuery('.stakeholder-logo-container .click-right').click(function(){
			stakeholderSlider.unslider('next');
		});

		

		jQuery('.latest-container').each(function(){
			jQuery(this).height(jQuery(this).width());
		});



		get_max_height_from_set('.feature-level-content',0);

		get_max_height_from_set('.webinar-column',0);

		new WOW().init();

		jQuery('#nav-toggle').jPushMenu();

    jQuery('#nav-toggle-stakeholder').jPushMenu();

    jQuery('#nav-toggle-sh').jPushMenu();

    var requiredSliderHeight = Math.max.apply(null, jQuery('.header-slider li').map(function (){
       
        return jQuery(this).height();

    }).get());
    jQuery('.header-slider').height(requiredSliderHeight);

    // This is to remove the red line if there is no title on the top widget panel
    if(typeof jQuery('div.do-sidebar > div:first-of-type h2:first-of-type span').html() === "undefined"){
        jQuery('div.do-sidebar > div:first-of-type h2:first-of-type').parent().addClass('removeTheLine');
    }
    if(typeof jQuery('div.do-full-sidebar > div:first-of-type h2:first-of-type span').html() === "undefined"){
        jQuery('div.do-full-sidebar > div:first-of-type h2:first-of-type').parent().addClass('removeTheLine');
    }

	});

}(window.jQuery || window.$));



function do_linealong(){

	jQuery('.line-along').each(function(){
		if(jQuery(this).html().trim().length != 0){
			jQuery(this).wrapInner('<span></span>');
		}
		
	});

}

function accordionMe(classOfContainer, speed){
	jQuery(classOfContainer).toggle(speed);
}

function accordionClick(classofbutton, classofContainer, speed){
		jQuery(classofbutton).on("click", function(e){
			
				accordionMe(classofContainer,speed);
				window.parentTop = undefined;		    
			
		});
}

function do_hidden_logo(){
	
	jQuery('.fixed-logo-container').css('margin-top', -1*(jQuery('#header').offset().top + 60) + 'px');

	jQuery('.fixed-logo-container').show();
		
}

function do_fixed_header() {

   	var toolbar        = jQuery('.level-nav-bar-normal');
   	
   if(toolbar.length){

    	var  offset         = toolbar.offset(),
       		 scrollTop      = jQuery(window).scrollTop();
       		 if(window.parentTop === undefined){
       		 	window.parentTop = document.getElementById('header').offsetTop;
       		 }

   		if ((scrollTop > offset.top) && (scrollTop < offset.top + toolbar.height())) {
           	
           	toolbar.addClass('fixedHeader');

           	jQuery('.fixedHeader.level-nav-bar-normal nav').css('padding-left', 10 + jQuery('.fixed-logo-container img').width() + 'px');


           	if(jQuery('.spendanalysistoolbar .accodlink.deactiveAccord').length == 0){

           		jQuery( ".spendHeaderClick" ).trigger( "click" );	

           	}
           	
       	} else if(scrollTop <= window.parentTop) {
        	
           	toolbar.removeClass('fixedHeader');
           	jQuery('.level-nav-bar-normal nav').css('padding-left', 0 + 'px');
       };

    }
 
}
function do_fixed_header_stakeholder() {

    var toolbar        = jQuery('.level-nav-bar-stakeholder');
    
   if(toolbar.length){

      var  offset         = toolbar.offset(),
           scrollTop      = jQuery(window).scrollTop();
           if(window.parentTop === undefined){
            window.parentTop = document.getElementById('header').offsetTop;
           }

      if ((scrollTop > offset.top) && (scrollTop < offset.top + toolbar.height())) {
            
            toolbar.addClass('fixedHeader');

            jQuery('.fixedHeader.level-nav-bar-stakeholder nav').css('padding-left', 10 + jQuery('.fixed-logo-container img').width() + 'px');


            if(jQuery('.spendanalysistoolbar .accodlink.deactiveAccord').length == 0){

              jQuery( ".spendHeaderClick" ).trigger( "click" ); 

            }
            
        } else if(scrollTop <= window.parentTop) {
          
            toolbar.removeClass('fixedHeader');
            jQuery('.level-nav-bar-stakeholder nav').css('padding-left', 0 + 'px');
       };

    }
 
}

jQuery(function() {
   
   jQuery(document)
    .scroll(do_fixed_header)
    .scroll(do_fixed_header_stakeholder)
    .trigger("scroll");
   
});
/*!
 * jPushMenu.js
 * 1.1.1
 * @author: takien
 * http://takien.com
 * Original version (pure JS) is created by Mary Lou http://tympanus.net/
 */


(function(jQuery) {
    jQuery.fn.jPushMenu = function(customOptions) {
        var o = jQuery.extend({}, jQuery.fn.jPushMenu.defaultOptions, customOptions);

        jQuery('body').addClass(o.pushBodyClass);

        // Add class to toggler
        jQuery(this).addClass('jPushMenuBtn');

        jQuery(this).click(function(e) {
            e.stopPropagation();

            var target     = '',
            push_direction = '';

            // Determine menu and push direction
            if (jQuery(this).is('.' + o.showLeftClass)) {
                target         = '.cbp-spmenu-left';
                push_direction = 'toright';
            }
            else if (jQuery(this).is('.' + o.showRightClass)) {
                target         = '.cbp-spmenu-right';
                push_direction = 'toleft';
            }
            else if (jQuery(this).is('.' + o.showTopClass)) {
                target = '.cbp-spmenu-top';
                push_direction = 'todown';
            }
            else if (jQuery(this).is('.' + o.showBottomClass)) {
                target = '.cbp-spmenu-bottom';
            }

            if (target == '') {
                return;
            }

            jQuery(this).toggleClass(o.activeClass);
            jQuery(target).toggleClass(o.menuOpenClass);

            if (jQuery(this).is('.' + o.pushBodyClass) && push_direction != '') {
                jQuery('body').toggleClass(o.pushBodyClass + '-' + push_direction);
            }

            // Disable all other buttons
            jQuery('.jPushMenuBtn').not(jQuery(this)).toggleClass('disabled');

            return;
        });

        var jPushMenu = {
            close: function (o) {
                jQuery('.jPushMenuBtn,body,.cbp-spmenu')
                    .removeClass('active disabled ' + o.activeClass + ' ' + o.menuOpenClass + ' ' + o.pushBodyClass + '-toleft ' + o.pushBodyClass + '-toright ' + o.pushBodyClass + '-todown');
            }
        }

        // Close menu on clicking outside menu
        if (o.closeOnClickOutside) {
             jQuery(document).bind("touchstart click", function(e){
                if(!jQuery(e.target).closest('nav').hasClass('cbp-spmenu-vertical') )

                       {
                            jPushMenu.close(o);            
                       }
                   
                
            });
        
         }

        // Close menu on clicking menu link
        if (o.closeOnClickLink) {
            jQuery('.cbp-spmenu a').on('touchstart click',function(e) {
                
                    jPushMenu.close(o);
                
                
            });
        }
    };

   /*
    * In case you want to customize class name,
    * do not directly edit here, use function parameter when call jPushMenu.
    */
    jQuery.fn.jPushMenu.defaultOptions = {
        pushBodyClass      : 'push-body',
        showLeftClass      : 'menu-left',
        showRightClass     : 'menu-right',
        showTopClass       : 'menu-top',
        showBottomClass    : 'menu-bottom',
        activeClass        : 'menu-active',
        menuOpenClass      : 'menu-open',
        closeOnClickOutside: true,
        closeOnClickLink   : false
    };
})(jQuery);
