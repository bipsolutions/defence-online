jQuery(document).on("ready", function(){
	setTimeout(function(){
		jQuery(document).trigger( "doAlignmentStuff" );
	}, 5000);
});

jQuery(document).on('doAlignmentStuff', function(){
	//console.log('alignment fired');
});

function alignStakeHolderImages(total, row){

    stakeholderwidth  = jQuery('.stakeholder-slider li').innerWidth();
    imgWidth      = jQuery('.sh-img-container img').outerWidth();
    NumberOfImages    = total / row;
    paddingRight = (stakeholderwidth - (NumberOfImages * imgWidth)) / (NumberOfImages - 1);
    
    jQuery('.sh-img-container').css('margin-right', paddingRight + 'px');
    jQuery('.sh-content-container').css('margin-right', paddingRight + 'px');
    jQuery('.sh-img-container:nth-of-type(' + NumberOfImages + 'n)').css('margin-right','0');
    jQuery('.sh-img-container:nth-of-type(1)').css('margin-left','0');
    jQuery('.sh-img-container').css('margin-top', paddingRight + 'px');

    jQuery('.stakeholder-logo-container').css('padding-bottom', paddingRight + 'px');

    alignHeight('.stakeholder-logo-container .nav-column', '.stakeholder-logo-container .nav-column i');

    alignHeight('.logo-slider', '.logo-slider-container i');

    get_max_height_from_set('.sh-img-container .sh-image-inner',0);


}

		//Had to put this function in the header because of a scope issue with imageLoaded Function
		function get_max_height_from_set(classSet, offset){

		  if ( jQuery(window).width() > 760 ) {
			var maxHeight = Math.max.apply(null, jQuery(classSet).map(function (){
				   
				return jQuery(this).height();

			}).get());
			
			jQuery(classSet).each(function(){
				
				jQuery(this).height(maxHeight - offset);
					
			});
		  } 

		}
		function alignHeight(outerHeight, innerHeight){

		navHeight = jQuery(outerHeight).outerHeight();
		iconHeight = jQuery(innerHeight).height();

		marginTop = (navHeight - iconHeight) / 2;
		if(marginTop == 0){
			laterAlignHeight(outerHeight, innerHeight);

		}else{
			jQuery(innerHeight).css('margin-top', marginTop + 'px');
		}
		
}
function laterAlignHeight(outerHeight, innerHeight){

		setTimeout(function(){

			navHeight = jQuery(outerHeight).outerHeight();
			iconHeight = jQuery(innerHeight).height();

			marginTop = (navHeight - iconHeight) / 2;
			
			jQuery(innerHeight).css('margin-top', marginTop + 'px');
			console.log('later fired');

		}, 2000);

}