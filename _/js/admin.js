
(function ($) {

	/* trigger when page is ready */
	$(document).ready(function (){

    var template = jQuery('#page_template').val();
    if(template != 'pbwsb-page.php'){
      jQuery(".post-type-page #acf-group_579774df16a28").hide();
    }
	

	});
	$(document).bind('DOMSubtreeModified', function () {
   		
      var template = jQuery('#page_template').val();

      if(template != 'pbwsb-page.php'){

   		   if(jQuery('div:not(.acf-clone)[data-layout="content_with_custom_sidebar"]').length >= 1){
   		 	   $('.post-type-page #acf-group_579774df16a28').show();
   		   }else{
   		 	   $('.post-type-page #acf-group_579774df16a28').hide();
   		   }

  		}
	});

}(window.jQuery || window.$));

