<?php
/*
Template Name: Events Page

*/
 get_header(); ?>

 <div class="level">

 	<div class="pure-g">
 	 		
 	 		<div class="pure-u-24-24">
 	 						
 	 			<div class="page-header">
 	 				
 	 				<h2 class="line-along"><?php global $post;?>
							<?php echo strtoupper(	$post->post_title ) ; ?></h2>
 	 			
 	 			</div>
 	
 	 		</div>
 	
 	 	</div>

 </div>
		
  <div class="pure-g dco-content do-events">

 	

 			<div class="pure-u-24-24">
				
				
				<?php $events = do_post_by_custom_post('events'); ?>

				
				<?php if($events) :?>


				<div class="pure-g all-events-container">
				
				<?php foreach ($events as $event):?>
					
						<div class="pure-u-24-24">
								
								<h3 class="event-title line-along"><?php echo strtoupper( $event->post_title	) ; ?></h3>
						</div>
						<div class="pure-u-18-24 event-container">

								<h3>Event Date</h3>
								<p><?php the_field('date', $event->ID);?></p>
								<h3>Event Location</h3>
								<p><?php the_field('venue', $event->ID);?> - <?php echo get_field('location', $event->ID)['address'] ;?></p>	
								<h3>Event Summary</h3>
								<p><?php echo do_get_content_extract($event->ID, 50, 'Read More');?></p>

								<p><a href="<?php echo get_the_permalink($event->ID)?>" class="cta">View Event</a></p>
						</div>
						
						<div class="pure-u-6-24 event-container">
							 <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $event->ID ), 'profile-logo' );?>
							 <?php $alt_text = get_post_meta(get_post_thumbnail_id($event->ID), '_wp_attachment_image_alt', true); ?>
							 <a href="<?php echo get_the_permalink($event->ID);?>"><img alt="<?=$alt_text; ?>" class="padding" src="<?php echo $image[0];?>"/></a>
						</div>
					
				<?php endforeach;?>

				</div>

				<?php endif;?>
	</div>

		
	</div>

<?php get_footer(); ?>