<?php
	

	$unique_id = uniqid();

	$showExcerpt = get_sub_field('show_excerpt');

	if(!$_SESSION['exclude_ids']){
		$_SESSION['exclude_ids'] = array();
	}
?>

<div class="level level-sub-features <?php echo $unique_id;?> level-sub-main-features">
		
		<div class="pure-g dco-content">

			<?php if($title = get_sub_field('title')): ?>
				<div class="pure-u-1">
					<h2 class="line-along"><?php echo strtoupper($title); ?></h2>
					<?php if($image = get_sub_field('title_image')):?>
						<div class="level-header-image">
							<img src="<?php echo $image['sizes']['profile-logo'];?>"/>
						</div>
					<?php endif;?>
				</div>
			<?php endif;?>
			<?php 

				$subfeatured = do_get_category_posts(get_sub_field('category_to_show'), 2);

				$args = array(
					'thumb' => 'main-feature-mini'
				);

				if($showExcerpt){
					$args['excerpt'] = true;
				}

			?>
			<?php if($blurb = get_sub_field('section_blurb')):?>
				<div class="pure-u-1">

						<p><?php echo $blurb;?></p>

				</div>
			<?php endif; ?>

			<?php if(get_sub_field('type_of_feature_box')){
					
					$type = get_sub_field('type_of_feature_box');

				}else{

					$type = 'title_overlay';

			}?>

			<?php $count = 1;?>
			<?php foreach($subfeatured as $sub_feature): ?>

				<div class="pure-u-1 pure-u-sm-12-24 pure-u-md-12-24 two-big <?php echo $type; ?> ">

					<?php if($count==1):?>
						<div class="padding-right">
					<?php else:?>
						<div class="padding-left">
					<?php endif?>
						
						<?php if($type == 'title_overlay'): ?>

							<?php do_overlay_features_panel($sub_feature,$args); ?>

						<?php else:?>

							<?php do_features_panel($sub_feature, $args); ?>

						<?php endif?>

					</div>

				</div>
				<?php $count ++;?>
			<?php endforeach;?>	
	
	</div>

</div>

<script type="text/javascript">

		jQuery(document).imagesLoaded( function() {

			get_max_height_from_set('.<?php echo $unique_id;  ?> .sub-image-container',0);
			get_max_height_from_set('.<?php echo $unique_id;  ?> .slide-title',0);
			get_max_height_from_set('.<?php echo $unique_id;  ?> .posted',0);
			get_max_height_from_set('.<?php echo $unique_id;  ?> .sub-feature-title-container',0);
			get_max_height_from_set('.<?php echo $unique_id;  ?> .sub-feature-excerpt',0);
			get_max_height_from_set('.<?php echo $unique_id;  ?> .two-big.title_overlay img',0);
				
		});

</script>




	