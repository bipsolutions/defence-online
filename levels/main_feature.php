<?php 

	$unique_id = uniqid();

	$featured = do_get_category_posts(get_sub_field('category_to_show'), 1);

	$responsiveness = get_sub_field('disable_responsiveness_on_this_level') ? 'md' : 'lg';

?>
	<div class="level level-main-feature wow fadeIn <?php echo $unique_id; ?> with-widget">

		<div class="pure-g dco-content">

			<?php if($title = get_sub_field('title')): ?>

				<div class="pure-u-24-24">
					<h2 class="line-along"><?php echo strtoupper($title); ?></h2>
					<?php if($image = get_sub_field('title_image')):?>
						<div class="level-header-image">
							<img src="<?php echo $image['sizes']['profile-logo'];?>"/>
						</div>
					<?php endif;?>
				</div>

			<?php endif;?>

			<div class="pure-u-1 pure-u-<?php echo $responsiveness; ?>-18-24 main-column column-width-widget">

				<div class="padding-vertical">

					<?php foreach($featured as $feature): ?>
						<?php $args = array(
								'thumb' => 'main-feature-main',
							); ?>
						<?php do_overlay_features_panel($feature, $args); ?>

					<?php endforeach; ?>

					</div>

				</div>

				<?php $right_content = get_sub_field('right_content_type');
						
						if(!$right_content || $right_content == 'normal'): ?>

							<div class="pure-u-1 pure-u-<?php echo $responsiveness; ?>-6-24 padding-top">

								<div class="margin-left">

									<div class="<?php the_sub_field('right_content_background_colour')?> main-column">

										<div class="padding-horizontal">

											<?php the_sub_field('right_content');?>

										</div>

									</div>

								</div>
								
							</div>

				<?php endif;?>

				<?php if($right_content == 'supplier'): ?>

					<?php 	
							//$which_supplier = get_sub_field('which_supplier_to_show');
							$the_title 		= get_sub_field('supplier_widget_title');
							//$supplier_id	= $which_supplier == 'specific' ? get_sub_field('which_specific_supplier_to_show') : null; ?>
					
					<div class="pure-u-1 pure-u-<?php echo $responsiveness; ?>-6-24 a-widget">
						
						<div class="padding">
							
							<?php do_do_supplier_widget($the_title, null,null,null,get_sub_field('show_latest_suppliers')); ?>

							
						</div>
					
					</div>

				<?php endif; ?>

				<?php if($right_content == 'stakeholder'): ?>

					<?php 
						$the_title = get_sub_field('stakeholder_widget_title');
						$which_stakeholder = get_sub_field('which_stakeholder_to_show');
						$stakeholder_id = $which_stakeholder == 'specific' ? get_sub_field('select_specific_stakeholder') : null;
						$hide_excerpt = get_sub_field('hide_stakeholder_excerpt');
						?>

						<div class="pure-u-1 pure-u-<?php echo $responsiveness; ?>-6-24 a-widget">
						
						<div class="padding">
							
							<?php do_do_stakeholder_widget($the_title , $which_stakeholder, $stakeholder_id, true, $hide_excerpt ); ?>
						
						</div>
					
					</div>

				<?php endif; ?>

			</div>

		</div>
<script>
	jQuery(document).imagesLoaded( function() {
		if ( jQuery(window).width() > 1020 ) {
		get_max_height_from_set('.<?php echo $unique_id; ?> .main-column', 20);
		get_max_height_from_set('.<?php echo $unique_id; ?> .column-width-widget', 0);
		}
	});

	jQuery(document).imagesLoaded( function() {
			
			get_max_height_from_set('.<?php echo $unique_id;?> .sh-icon-list-container',0);

		jQuery('.<?php echo $unique_id;?> .sh-icon-list-container').each(function(){

			alignHeight(jQuery(this), jQuery(this).find('img')); 

		});
	});
		
</script>