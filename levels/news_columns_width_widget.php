<?php
	
	
	$distrubtion 	= 24 / (get_sub_field('number_of_columns') + 1);
	
	$unique_id = uniqid();

	$showExcerpt = get_sub_field('show_excerpt');

	if(!$_SESSION['exclude_ids']){
		$_SESSION['exclude_ids'] = array();
	}
?>

<div class="level level-sub-features <?php echo $unique_id;?> with-widget">
		
		<div class="pure-g dco-content">

			<?php if($title = get_sub_field('title')): ?>

				<div class="pure-u-1">

					<h2 class="line-along"><?php echo strtoupper($title); ?></h2>

					<?php if($image = get_sub_field('title_image')):?>

						<div class="level-header-image">

							<img src="<?php echo $image['sizes']['profile-logo'];?>"/>

						</div>

					<?php endif;?>

				</div>

			<?php endif;?>

			<?php 

				$subfeatured = do_get_category_posts(get_sub_field('category_to_show'), get_sub_field('number_of_columns'));
				

				$args = array(
					'thumb' => 'sub-feature'
				);
				if(in_array(15, get_sub_field('category_to_show'))){
					$args['stakeholder'] = true;
				}
				if($showExcerpt){
					$args['excerpt'] = true;
				}

			?>
				<?php if($blurb = get_sub_field('section_blurb')):?>

					<div class="pure-u-1">

							<p><?php echo $blurb;?></p>

					</div>

				<?php endif; ?>

				<?php $count = 0; foreach($subfeatured as $sub_feature): ?>

					<div class="pure-u-1 pure-u-sm-12-24 pure-u-md-<?php echo $distrubtion; ?>-24 sub-feature column-width-widget">
						
						<div class="padding">
							
							<?php $args['delay'] = $count/6; do_sub_feature_panel($sub_feature, $args); ?>
						
						</div>
					
					</div>
				<?php $count++;?>

				<?php endforeach;?>

				<?php $widget_choice = get_sub_field('widget_to_show');?>
				
				<?php if($widget_choice  == 'supplier'):?>


					<?php 	
							$which_supplier = get_sub_field('which_supplier_to_show');
							$the_title 		= get_sub_field('supplier_widget_title');
							$supplier_id	= $which_supplier == 'specific' ? get_sub_field('which_specific_supplier_to_show') : null; ?>
					
					<div class="pure-u-1 pure-u-sm-12-24 pure-u-md-<?php echo $distrubtion; ?>-24 a-widget">
						
						<div class="padding">
							
							<?php do_do_supplier_widget($the_title); ?>
						
						</div>
					
					</div>
				<?php endif; ?>

				<?php if($widget_choice  == 'stakeholder'):?>

					<?php 
						$the_title = get_sub_field('stakeholder_widget_title');
						$which_stakeholder = get_sub_field('which_stakeholder_to_show');
						$stakeholder_id = $which_stakeholder == 'specific' ? get_sub_field('select_specific_stakeholder') : null;
						$hide_excerpt = get_sub_field('hide_stakeholder_excerpt');
						?>

						<div class="pure-u-1 pure-u-sm-12-24 pure-u-md-<?php echo $distrubtion; ?>-24 a-widget">
						
						<div class="padding">
							
							<?php do_do_stakeholder_widget($the_title , $which_stakeholder, $stakeholder_id, true, $hide_excerpt ); ?>
						
						</div>
					
					</div>

				
				<?php endif;?>
	
		</div>

	</div>

	
	<script type="text/javascript">
		
		jQuery(document).imagesLoaded( function() {

			get_max_height_from_set('.<?php echo $unique_id;  ?> .sub-image-container',0);
			get_max_height_from_set('.<?php echo $unique_id;  ?> .slide-title',0);
			get_max_height_from_set('.<?php echo $unique_id;  ?> .posted',0);
			get_max_height_from_set('.<?php echo $unique_id;  ?> .sub-feature-title-container',0);
			get_max_height_from_set('.<?php echo $unique_id;  ?> .sub-feature-excerpt',0);
			get_max_height_from_set('.<?php echo $unique_id;  ?> .unislide .sub-feature-title-container',10);

			
			get_max_height_from_set('.<?php echo $unique_id;  ?> .column-width-widget',20);
			
			
			<?php if(get_sub_field('show_a_slider')): ?>
				
				var subFeatureSlider = jQuery('.<?php echo $unique_id;?> .sub-feature-slider').unslider({
							arrows: false
						});
				
				jQuery('.<?php echo $unique_id;?> .sub-feature .click-left').click(function(){
					subFeatureSlider.unslider('prev');
				});
				jQuery('.<?php echo $unique_id;?> .sub-feature .click-right').click(function(){
					subFeatureSlider.unslider('next');
				});
			
			<?php endif?>
					
		});

	</script>




	