<?php 
	
	$unique_id = uniqid();

?>

	<div class="level level-stakeholder red <?php echo $unique_id; ?>">

		<div class="pure-g dco-content">

			<div class="pure-u-24-24 stakeholder-shop-header">
				
				<div class="left" style="float:left">

					<h2><?php the_sub_field('title'); ?></h2>
					<h3><?php the_sub_field('sub_title'); ?></h3>
					
				</div>	

				<div class="join-us-button hide-xs hide-sm">
						<a href="<?php the_field('become_a_stakeholder_link','do-journey');?>" class="cta-full left">
							
							<h2>

								<?php the_sub_field('button_icon'); ?> <?php the_sub_field('button_title'); ?>
								
							</h2>

							<span class="sub-title-button"><?php the_sub_field('button_sub_title')?></span>
						</a>
				</div>

			</div>
			
				
			
			<? $items = do_get_category_posts(get_sub_field('category_of_news_to_show'), get_sub_field('max_number_of_news_articles_to_show'));

			?>
			<div class="pure-u-1 pure-u-md-6-24 stakeholder-panel">
				
				<div class="padding">
					
					<div class="stakeholder-news-slider unislide" >
						
						<ul>
							<?php foreach($items as $item):?> 
								
								<li>
									
									<?php $args = array(
												'slider'=> true,
												'stakeholder' =>true
									);?>
									<?php do_sub_feature_panel($item, $args); ?>
								
								</li>
							
							<?php endforeach;?>
											
						</ul>
									
					</div>
			
				</div>
							
			</div>

			<div class="pure-u-1-24 hide-xs hide-sm">

			</div>

			<div class="pure-u-1 pure-u-md-17-24">

				<?php  $stakeholders 	= get_sub_field('stakeholder_to_show');
						
   				?>
				<div class="stakeholder-logo-container">
					
					<div class="pure-g">
						
						<div class="pure-u-2-24 center nav-column">
							
							<i class="fa fa-arrow-circle-left fa-2x fa-nav click-left" aria-hidden="true"></i>
						
						</div>
						
						<div class="pure-u-20-24">
							
							<div class="stakeholder-slider unislide">
								
								<ul>
									
									<?php if($count_stakeholders < 10): ?>

										<?php $index = 1; ?>

										<?php foreach ($stakeholders as $stakeholder): ?>

											<?php $stakeholderPostID = do_get_stakeholder_id($stakeholder->ID);?>
											
											<?php if($index == 1 || $index % 4 == 0 || do_wp_is_mobile()):?>
												
												<li>
													
													<div class="pure-g">
											
											<?php endif;?>
												<?php $stakeholderURL = get_the_permalink($stakeholder->ID)?>
													
													<div class="pure-u-1 pure-u-md-8-24">
														
														<div class="sh-img-outer-container">
															
															<div class="sh-img-container">
												
																<?php $logo	= get_field('sh_company_logo', 'user_'. $stakeholderPostID)['sizes']['profile-logo'];?>

																<?php if($logo):?>

																	<a class="imagelink" href="<?php echo $stakeholderURL ;?>">
																		<div class="sh-image-inner">
																			<img src="<?php echo $logo;?>">
																		</div>
																	</a>

																<?php endif;?>
															
															</div>
														
															<div class="sh-content-container">
																
																<p>
																		<?php echo do_get_stakehoder_extract(
																	get_field('sh_about_us_text','user_'. $stakeholderPostID ), 20, '<a class="readmore" href="'.$stakeholderURL.'"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>')?>
																	
																</p>

															</div>
												
														</div>
													
													</div>
										
													<?PHP $index++; ?>
										
												<?php if($index % 4 == 0 || do_wp_is_mobile() ):?>
										
														</div>
													
													</li>
												
												<?php endif;?>
											
									
									
										<?php endforeach; ?>
									

									<?php else: ?>
							
										<li>
											<div class="sh-img-container">
												<img src="<?php bloginfo('template_url');?>/images/sk-logo.jpg">
											</div>
											<div class="sh-img-container">
												<img src="<?php bloginfo('template_url');?>/images/sk-logo.jpg">
											</div>
											<div class="sh-img-container">
												<img src="<?php bloginfo('template_url');?>/images/sk-logo.jpg">
											</div>
											<div class="sh-img-container">
												<img src="<?php bloginfo('template_url');?>/images/sk-logo.jpg">
											</div>
											<div class="sh-img-container">
												<img src="<?php bloginfo('template_url');?>/images/sk-logo.jpg">
											</div>
											<div class="sh-img-container">
												<img src="<?php bloginfo('template_url');?>/images/sk-logo.jpg">
											</div>
											<div class="sh-img-container">
												<img src="<?php bloginfo('template_url');?>/images/sk-logo.jpg">
											</div>
											<div class="sh-img-container">
												<img src="<?php bloginfo('template_url');?>/images/sk-logo.jpg">
											</div>
										</li>
										<li>
											<div class="sh-img-container">
												<img src="<?php bloginfo('template_url');?>/images/sk-logo.jpg">
											</div>
											<div class="sh-img-container">
												<img src="<?php bloginfo('template_url');?>/images/sk-logo.jpg">
											</div>
											<div class="sh-img-container">
												<img src="<?php bloginfo('template_url');?>/images/sk-logo.jpg">
											</div>
											<div class="sh-img-container">
												<img src="<?php bloginfo('template_url');?>/images/sk-logo.jpg">
											</div>
											<div class="sh-img-container">
												<img src="<?php bloginfo('template_url');?>/images/sk-logo.jpg">
											</div>
											<div class="sh-img-container">
												<img src="<?php bloginfo('template_url');?>/images/sk-logo.jpg">
											</div>
											<div class="sh-img-container">
												<img src="<?php bloginfo('template_url');?>/images/sk-logo.jpg">
											</div>
											<div class="sh-img-container">
												<img src="<?php bloginfo('template_url');?>/images/sk-logo.jpg">
											</div>
										</li>
									<?php endif;?>
								
								</ul>
							
							</div>
						
						</div>
						
						<div class="pure-u-2-24 center nav-column">
							
							<i class="fa fa-arrow-circle-right fa-2x fa-nav click-right" aria-hidden="true"></i>
						
						</div>
					
					</div>
					
				</div>

			</div>

			<div class="pure-u-1">
				
				<div class="join-us-button hide-md hide-lg hide-xl">
						<a href="<?php the_field('become_a_stakeholder_link','do-journey');?>" class="cta-full left">
							
							<h2>

								<?php the_sub_field('button_icon'); ?> <?php the_sub_field('button_title'); ?>
								
							</h2>

							<span class="sub-title-button"><?php the_sub_field('button_sub_title')?></span>
						</a>
				</div>

			</div>
		
		</div>

	</div>

<script type="text/javascript">

	jQuery(document).imagesLoaded( function() {

		var shSlider = jQuery('.<?php echo $unique_id;?> .stakeholder-news-slider').unslider(
			{
				arrows: false
			}
		);

		jQuery('.<?php echo $unique_id;?> .stakeholder-news-slider .click-left').click(function(){

			shSlider.unslider('prev');

		});

		jQuery('.<?php echo $unique_id;?> .stakeholder-news-slider .click-right').click(function(){

			shSlider.unslider('next');
			
		});
		<?php if(!do_wp_is_mobile()):?>
			<?php if($count_stakeholders < 10):?>
				alignStakeHolderImages(3,1);
			<?php else:?>
				alignStakeHolderImages(3,2);
			<?php endif; ?>
		<?php endif;?>
		

		jQuery('.sh-image-inner').each(function(){

			alignHeight(jQuery(this), jQuery(this).find('img')); 

		});
	});
</script>