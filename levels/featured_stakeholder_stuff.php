<?php
	

	$unique_id = uniqid();

	$showExcerpt = get_sub_field('show_excerpt');

	if(!$_SESSION['exclude_ids']){
		$_SESSION['exclude_ids'] = array();
	}
?>

<div class="level level-sub-features <?php echo $unique_id;?> level-sub-main-features">
		
		<div class="pure-g dco-content">

			
			
			<?php if($blurb = get_sub_field('section_blurb')):?>
				<div class="pure-u-1">

						<p><?php echo $blurb;?></p>

				</div>
			<?php endif; ?>
			
			

				<div class="pure-u-1 pure-u-md-12-24 two-big">
					<h2 class="line-along">FEATURED VIDEOS</h2>
					<?php 
						$featured_videos = get_field('featured_videos', 'options');

					 ?>
					<div class="pure-g featvideos">
						<?php foreach ($featured_videos as $key => $featured_video):?>

							<?php 
								  $video_type 		= explode("|",$featured_video['feature_sh_video'])[2];
								  $video_id 		= explode("|",$featured_video['feature_sh_video'])[1];
								  $stakeholder_user = explode("|",$featured_video['feature_sh_video'])[0];
								  $companyName 		= get_the_stakeholder_by_author( $stakeholder_user);

								  switch ($video_type) {
								  	case 'vimeo':

								  		$img 		= '<img src="'.getVideoThumbnailByUrl("http://vimeo.com/". $video_id).'">';
								  		$vidtitle 	= ucwords(get_vimeo_title_by_id($video_id));
								  		$vidEmbed 	= do_shortcode('[fve]http://vimeo.com/'. $video_id.'[/fve]');

								  		break;

								  	case 'youTube':
								  		
								  		$img 		= '<img src="http://img.youtube.com/vi/'. $video_id .'/mqdefault.jpg">';
								  		$vidtitle 	= ucwords(get_youtube_title_by_id($video_id));
								  		$vidEmbed 	= do_shortcode('[fve]https://www.youtube.com/watch?v=' .$video_id. '[/fve]');

								  		break;
								  	
								  	default:
								  		
								  		break;
								  }
								  
							?>
							<div class="pure-u-1 pure-u-md-12-24">

								<div class="padding-right">

									<div class="img-container">
										
										<?php echo $img; ?>
										<a data-remodal-target="modal-video-<?php echo $key;?>"><div id="play"></div></a>

									</div>

									<div class="remodal" data-remodal-id="modal-video-<?php echo $key;?>">
									  
									  <button data-remodal-action="close" class="remodal-close"></button>
									  
									  <h2><?php echo $vidtitle; ?></h2>
									  
									  <p>
									  		<?php echo $vidEmbed;?>
									  </p>
									 
									  
									</div>
									
									<h3><a data-remodal-target="modal-video-<?php echo $key;?>"><?php echo $vidtitle; ?></a></h3>

									<p class="vidaddedby">Video added by <a href="<?php echo get_the_permalink(get_the_stakeholder_id_by_author($stakeholder_user)); ?>"><?php echo $companyName;?></a></p>

								</div>

							</div>

						<?php endforeach?>
						
					</div>
				
			
	
				</div>

				<div class="pure-u-1 pure-u-sm-12-24 pure-u-md-12-24 two-big">
					<h2 class="line-along">FEATURED EBOOKS</h2>
					
					<?php $featured_ebooks = get_field('feature_ebook', 'options'); ?>

					<div class="pure-g">

						<?php foreach ($featured_ebooks as $featured_ebook):?>

							<?php $ebookInfo = do_get_ebook_infomation($featured_ebook['select_an_ebook']);?>

						<div class="pure-u-2-24">
								
						</div>

						<div class="pure-u-2-24">

							<p style="text-align:center; margin-top: 10px;"><a target="_blank" href="<?php echo $ebookInfo['ebUrl']; ?>"><i class="fa fa-file-pdf-o large-fa" aria-hidden="true"></i></a></p>
								
						</div>
					
						<div class="pure-u-18-24">
								
							<h3>
								<a target="_blank" href="<?php echo $ebookInfo['ebUrl']; ?>"><?php echo $ebookInfo['ebTitle']; ?></a>
							</h3>
			
							<p>eBook added by <a href="<?php echo $ebookInfo['stakeholder_url']; ?>"><?php echo $ebookInfo['companyName'];?></a></p>
								
							<p><a target="_blank" class="cta" href="<?php echo $ebookInfo['ebUrl']; ?>">DOWNLOAD</a></p>

						</div>

						<div class="pure-u-2-24">
								
						</div>
					<?php endforeach;?>
				
				</div>
	
			</div>

	</div>

	
<script type="text/javascript">
	
	jQuery(document).imagesLoaded( function() {

		get_max_height_from_set('.<?php echo $unique_id;  ?> .sub-image-container',0);
		get_max_height_from_set('.<?php echo $unique_id;  ?> .slide-title',0);
		get_max_height_from_set('.<?php echo $unique_id;  ?> .posted',0);
		get_max_height_from_set('.<?php echo $unique_id;  ?> .sub-feature-title-container',0);
		get_max_height_from_set('.<?php echo $unique_id;  ?> .sub-feature-excerpt',0);
					
		get_max_height_from_set('.img-container',0);
		get_max_height_from_set('.featvideos h3',0);
		get_max_height_from_set('.vidaddedby',0);
									
	});

</script>




	