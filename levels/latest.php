<?php 

	$unique_id = uniqid();

?>
		<div class="level level-latest-stuff red <?php echo $unique_id; ?>">
				<div class="pure-g dco-content">
						<div class="pure-u-1 pure-u-sm-11-24 pure-u-md-6-24 pure-u-lg-5-24">
							<h3 class="padding-bottom">Latest Suppliers</h3>
							<div class="latest-container suppliers">
								<?php $suppliers = do_post_by_custom_post('supplier', 5); ?>
								<?php do_do_logo_slider($suppliers);?>
							</div>
							<a href="<?php echo get_the_permalink(884)?>" class="cta-full margin-vertical">VIEW ALL SUPPLIERS</a>
						</div>
						<div class="pure-u-1-24 pure-u-sm-2-24 pure-u-md-3-24 pure-u-lg-1-24 hide-xs">
							
						</div>
						<div class="pure-u-1 pure-u-sm-11-24 pure-u-md-6-24 pure-u-lg-5-24">
							<h3 class="padding-bottom">Upcoming Events</h3>
							<div class="latest-container events">
								<?php $events = do_post_by_custom_post('events', 5); ?>
								<?php do_do_logo_slider($events);?>
							</div>
							<a href="https://www.defenceonline.co.uk/more/events/" class="cta-full margin-vertical">VIEW OUR EVENTS PAGE</a>
						</div>
						<div class="pure-u-1-24 pure-u-sm-2-24 pure-u-md-3-24 pure-u-lg-1-24 hide-sm hide-xs">
							
						</div>
						<div class="pure-u-1 pure-u-sm-11-24 pure-u-md-6-24 pure-u-lg-5-24">
							<h3 class="padding-bottom">Latest Publication</h3>
							<div class="latest-container ebooks">
								<?php $pubs = do_post_by_custom_post('publication', 5); ?>

								<?php do_do_pub_slider($pubs);?>
							</div>
							<a href="<?php echo get_the_permalink(324)?>" class="cta-full margin-vertical">VIEW ALL</a>
						</div>
						<div class="pure-u-1-24 pure-u-sm-2-24 pure-u-lg-1-24 hide-xs hide-md">
							
						</div>
						<div class="pure-u-1 pure-u-sm-11-24 pure-u-lg-6-24 hide-md">
							<h3 class="padding-bottom">Latest Tweets</h3>
							<div class="latest-container twitter">
								<a class="twitter-timeline" href="<?php the_field('twitter_url','options')?>"></a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
							</div>
						</div>
				</div>
		</div>

		<script>
			jQuery(document).imagesLoaded( function() {

					alignHeight(jQuery('.<?php echo $unique_id;?> .latest-container.suppliers'), jQuery('.<?php echo $unique_id;?> .latest-container.suppliers').find('[class*="pure-u"]'));

					alignHeight(jQuery('.<?php echo $unique_id;?> .latest-container.events'), jQuery('.<?php echo $unique_id;?> .latest-container.events').find('[class*="pure-u"]'));

					alignHeight(jQuery('.<?php echo $unique_id;?> .latest-container.ebooks'), jQuery('.<?php echo $unique_id;?> .latest-container.ebooks').find('[class*="pure-u"]'));

			});

		</script>