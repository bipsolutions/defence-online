<?php 

	$unique_id = uniqid();

?>

<div class="level level-publications <?php echo $unique_id;?>">
		
		<div class="pure-g dco-content">

			<?php if($title = get_sub_field('title')): ?>
				<div class="pure-u-24-24">
					<h2 class="line-along"><?php echo strtoupper($title); ?></h2>
					<?php if($image = get_sub_field('title_image')):?>
						<div class="level-header-image">
							<img src="<?php echo $image['sizes']['profile-logo'];?>"/>
						</div>
					<?php endif;?>
				</div>
			<?php endif;?>
			<?php 
					
					$pubs = get_sub_field('publications_to_show');

				if( $pubs ): ?>

				<div id="mainwrap">
				<ul>
					<?php foreach( $pubs as $pub):  ?>
						
					<li>
				   		<div class="pure-u-24-24">
				   			<?php $link = get_the_permalink($pub->ID);?>
				   			<h3 class="pub-title"><?php echo $pub->post_title; ?></h3>
				   			<div class="pub-container">
				   				
				   				<?php $image = get_field('front_cover', $pub->ID); ?>
				   					
				   				<div class="pub-image-container"><a href="<?php echo $link; ?>"><img width="300" height="auto" src="<?php echo $image['sizes']['large']?>"/></a></div>
				   			
				   				<p class="pub-description"><?php echo get_field('description', $pub->ID); ?></p>

				   				<p>

				   					<a href="<?php echo $link; ?>" class="cta">VIEW</a>
				   				
				   					<?php if($download = get_field('direct_download_link', $pub->ID)):?>
				   						
				   						<a id="pubdownload" data-user="<?php echo get_current_user_id();?>" data-document="<?php echo $pub->ID; ?>" target="_blank" href="<?php echo $download ?>" class="cta pubdownload">DOWNLOAD</a>
				   						
				   					<?php endif; ?>

				   				</p>
				   				
				   			</div>
				   		</div>
				   			
				   		
					</li>
					<?php endforeach;?>
				 </ul>
				</div>
				<?php endif; ?>
											
			
			
		
	
	</div>

</div>

	
		<script type="text/javascript">
				jQuery(document).imagesLoaded( function() {

					get_max_height_from_set('.<?php echo $unique_id;  ?> .pub-image-container',0);
					get_max_height_from_set('.<?php echo $unique_id;  ?> .pub-description',0);
					get_max_height_from_set('.<?php echo $unique_id;  ?> .pub-title',0);
					
				
				});

				jQuery(document).ready(function () { 
				   jQuery('.pubdownload').click(function(e){
				        
				        
				        var data = {
								'userid' : jQuery(this).data('user'),
								'pubid' : jQuery(this).data('document'),
							}
				       	jQuery.ajax({
				       			type: "POST",
				       			url: "<?php bloginfo('template_url');?>/process-pub-click.php",
				       			data: data,
							    success: function (result) {
							           
							    }
				       	}); 
				   });
				});


</script>




