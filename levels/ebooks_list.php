<?php
	$unique_id = uniqid();
?>

<div class="level level-video-slider <?php echo $unique_id;?> level-sub-main-features">
		
	<div class="pure-g dco-content">

		<?php if($title = get_sub_field('title')): ?>
			<div class="pure-u-1" style="margin-bottom: 20px">
				<h2 class="line-along"><?php echo strtoupper($title); ?></h2>
				<?php if($image = get_sub_field('title_image')):?>
					<div class="level-header-image">
						<img src="<?php echo $image['sizes']['profile-logo'];?>"/>
					</div>
				<?php endif;?>
			</div>
		<?php endif;?>
		
	
				
		<?php if(get_sub_field('which_ebooks_to_show') == 'specific'):?>

			<?php 
				$ebooks = get_sub_field('select_specific_ebooks');
				$amount_selected = count($ebooks);
				$grid_size = 24 / $amount_selected;

			?>

			<?php foreach ($ebooks as $ebook): ?>
				<div class="pure-u-1 pure-u-md-<?php echo $grid_size; ?>-24">
					
					<?php $uniqid_thumb = uniqid('ebook-pdf-thumb-'); ?>
					<div class="padding-horizontal">
				
						<?php 

							$ebookinfo 	= do_get_ebook_infomation($ebook['ebook_select']);
							$newImage 	= do_do_pdf_image($ebookinfo['ebUrl']);

						?>

						<a target="_blank" href="<?php echo $ebookinfo['ebUrl']; ?>">
							<img class="ebook-pdf-thumb <?php echo $uniqid_thumb; ?>" src="<?php echo $newImage; ?>">
						</a>

						<p>
							<a target="_blank"  class="cta cta-full" href="<?php echo $ebookinfo['ebUrl']; ?>">View eBook</a>
						</p>

					</div>
					<script type="text/javascript">
						
						var the_thumb_tip = jQuery('.<?php echo $uniqid_thumb ?>').tooltipster({
   							
   							distance: 30,
   							maxWidth : 400
   							interactive : true,
   							contentAsHTML : true,
   							content : '<?php echo $ebookinfo['ebTitle']; ?><br>By: <a class="tooltipsterA" href="<?php echo $ebookinfo['stakeholder_url'] ?>"><?php echo $ebookinfo['companyName']; ?></a>'
   							
						});

					</script>
					
				
				</div>
			<?php endforeach ?>
			
		<?php else: ?>

				<?php $ebooks = do_get_latest_ebooks(get_sub_field('max_amount_to_show'));
					  $ebooks_count = count($ebooks);
					  $grid_size = 24 / $ebooks_count;
				?>

				<?php foreach ($ebooks as $ebook) :?>
					
					<?php $uniqid_thumb = uniqid('ebook-pdf-thumb-'); ?>

					<div class="pure-u-1 pure-u-md-<?php echo $grid_size; ?>-24">
					
					<div class="padding-horizontal">
				
						<?php 

							$ebookinfo 	= do_get_ebook_infomation($ebook);
							$newImage 	= do_do_pdf_image($ebookinfo['ebUrl']);

						?>

						<a target="_blank" href="<?php echo $ebookinfo['ebUrl']; ?>">
							<img class="ebook-pdf-thumb <?php echo $uniqid_thumb; ?>" src="<?php echo $newImage; ?>">
						</a>

						<p>
							<a target="_blank" title="<?php echo $ebookinfo['ebTitle']; ?>" class="cta cta-full" href="<?php echo $ebookinfo['ebUrl']; ?>">View eBook</a>
						</p>

					</div>

					<script type="text/javascript">
						
						var the_thumb_tip = jQuery('.<?php echo $uniqid_thumb ?>').tooltipster({
   							
   							distance: 30,
   							maxWidth : 400,
   							interactive : true,
   							contentAsHTML : true,
   							content : '<?php echo $ebookinfo['ebTitle']; ?><br>By: <a class="tooltipsterA" href="<?php echo $ebookinfo['stakeholder_url'] ?>"><?php echo $ebookinfo['companyName']; ?></a>'

						});

					</script>
				
				</div>

				<?php endforeach;?>

		<?php endif; ?>
		
	</div>

</div>

<script type="text/javascript">

	jQuery(document).imagesLoaded( function() {

		get_max_height_from_set('.<?php echo $unique_id;  ?> .ebook-pdf-thumb',0);

	});

</script>




	