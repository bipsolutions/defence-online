<?php 

	$unique_id = uniqid();

	$featured = do_get_category_posts(get_sub_field('category_to_show'), 1);

	$thumb =  'main-feature';

	$side = get_sub_field('video_position');

	$responsiveness = get_sub_field('disable_responsiveness_on_this_level') ? 'md' : 'lg';

?>
	<div class="level level-video wow fadeIn <?php echo $unique_id; ?>">

		<div class="pure-g dco-content">

			<?php if($title = get_sub_field('title')): ?>

				<div class="pure-u-24-24">
					<h2 class="line-along"><?php echo strtoupper($title); ?></h2>
				</div>

			<?php endif;?>

			<?php if($side == 'left'):?>

					<?php do_video_markup($responsiveness); ?>

			<?php endif; ?>

			<div class="pure-u-1 pure-u-lg-8-24">

					<div class="padding-<?php echo $side; ?>">

						<div class="<?php the_sub_field('right_content_background_colour')?> main-column">

							<div class="double-padding">

								<?php the_sub_field('content');?>

							</div>

						</div>

					</div>
					
			</div>


			<?php if($side == 'right' || !$side):?>

				<?php do_video_markup($responsiveness); ?>

			<?php endif; ?>

				

			</div>

		</div>
<script>
	jQuery(document).imagesLoaded( function() {
		if ( jQuery(window).width() > 1020 ) {
				get_max_height_from_set('.<?php echo $unique_id; ?> .main-column',0);
		}
	});	
</script>