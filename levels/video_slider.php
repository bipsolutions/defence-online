<?php
	$unique_id = uniqid();
?>

<div class="level level-video-slider <?php echo $unique_id;?> level-sub-main-features">
		
	<div class="pure-g dco-content">

		<?php if($title = get_sub_field('title')): ?>
			<div class="pure-u-1" style="margin-bottom: 20px">
				<h2 class="line-along"><?php echo strtoupper($title); ?></h2>
				<?php if($image = get_sub_field('title_image')):?>
					<div class="level-header-image">
						<img src="<?php echo $image['sizes']['profile-logo'];?>"/>
					</div>
				<?php endif;?>
			</div>
		<?php endif;?>
		
	<div class="pure-u-1-24 video-slide-left">
				
		<i class="fa fa-arrow-circle-left left-click fa-2x" aria-hidden="true"></i>

	</div>
		
	<div class="pure-u-22-24">
		
		<div class="video-slider unislide" >
					
			<?php 	
				
				if(get_sub_field('which_videos_to_show') == 'specific'){
					
					$videos = get_sub_field('select_specific_videos');
					$new_videos = array();

					foreach ($videos as $video) {
						array_push($new_videos, $video['video']);
					}
					$videos = $new_videos;

					}else{

						$videos = do_post_by_custom_post('videos', 10);

					}
							
					$title_poistion = get_sub_field('video_title');
					$video_count = 1;

					?>	
					<ul>
						<?php foreach ($videos as $video) :?>

							<?php $the_title = "<span class='video-slider-title'>" . $video->post_title. "</span>"; ?>
							
							<?php if($video_count % 3 == 1 ):?>
								
								<li>
									
									<div class="pure-g">

							<?php endif;?>
								
										<div class="pure-u-1 pure-u-sm-12-14 pure-u-md-8-24">
											
											<div class="one-slide">

												<?php if($title_poistion == 'top'):?>
													
													<?php echo $the_title; ?>

												<?php endif; ?>
											
												<?php switch (get_sub_field('how_do_the_videos_open')) {
														
													case 'permalink':
														
														do_do_video_thumbnail_with_link($video);

													break;

													case 'popup':

														do_do_video_thumbnail_with_link($video, true);
													
													break;
													
													case 'embed':
														
														$videourl = get_field('video_url', $video->ID);
														echo '<div class="video-wrap">';
														echo getEmbedVideo($videourl);
														echo '</div>';
													
													break;
													
													default:
													
													break;
												
												}
											
											?>

											<?php if($title_poistion == 'bottom'):?>
													
													<?php echo $the_title; ?>

											<?php endif; ?>

										</div>

									</div>
									
						<?php if($video_count % 3 == 0):?>

								</div>
							
							</li>

						<?php endif;?>

						<?php $video_count++;?>

					<?php endforeach;?>

				
				</ul>
				
			</div>
		
		</div>
		
		<div class="pure-u-1-24 video-slide-right">
				
			<i class="fa fa-arrow-circle-right right-click fa-2x" aria-hidden="true"></i>

		</div>
		
	</div>

</div>

<script type="text/javascript">

	jQuery(document).imagesLoaded( function() {

		get_max_height_from_set('.<?php echo $unique_id;  ?> .video-slider-title',0);
		get_max_height_from_set('.<?php echo $unique_id;  ?> .one-slide .video-wrap',0);


		var subFeatureSlider = jQuery('.<?php echo $unique_id;?> .video-slider').unslider({
			
			arrows: false,
			nav : false,
		
		});
		<?php if(count($videos) > 3):?>	
		jQuery('.<?php echo $unique_id;?> .video-slide-left .left-click').click(function(){
			
			subFeatureSlider.unslider('prev');
		
		});
		
		jQuery('.<?php echo $unique_id;?> .video-slide-right .right-click').click(function(){
			
			subFeatureSlider.unslider('next');
		
		});
		<?php else:?>
			jQuery('.<?php echo $unique_id;?> .video-slide-right .right-click').hide();
			jQuery('.<?php echo $unique_id;?> .video-slide-left .left-click').hide();
		<?php endif; ?>
		alignHeight(jQuery('.<?php echo $unique_id;?> .fa-arrow-circle-right').parent().parent().find('.video-wrap'), jQuery('.<?php echo $unique_id;?> .fa-arrow-circle-right'));
		alignHeight(jQuery('.<?php echo $unique_id;?> .fa-arrow-circle-left').parent().parent().find('.video-wrap'), jQuery('.<?php echo $unique_id;?> .fa-arrow-circle-left')  );	
	});

</script>




	