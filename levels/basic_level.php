<?php 
	$distrubtion 		= get_sub_field('column_width');
	$rightDistrubtion 	= 24 - $distrubtion;
?>

<div class="level level-basic">
	<div class="pure-g dco-content">
		<?php if($title = get_sub_field('title')): ?>

				<div class="pure-u-24-24">
					<h2 class="line-along"><?php echo strtoupper($title); ?></h2>
					<?php if($image = get_sub_field('title_image')):?>
						<div class="level-header-image">
							<img src="<?php echo $image['sizes']['profile-logo'];?>"/>
						</div>
					<?php endif;?>
				</div>

		<?php endif;?>
		<?php if($distrubtion != 24):?>

			<div class="pure-u-1 pure-u-md-<?php echo $distrubtion ?>-24">
				<div class="padding-right padding-top">

					<?php the_sub_field('left_column');?>
					
				</div>
			</div>
			<div class="pure-u-1 pure-u-md-<?php echo $rightDistrubtion ?>-24">
				<div class="padding-left">

					<?php the_sub_field('right_column');?>

				</div>
			</div>

		<?php else:?>
			
			<div class="pure-u-24-24">
				<div class="padding-vertical">

					<?php the_sub_field('full_column');?>

				</div>
			</div>

		<?php endif;?>
	</div>
</div>
