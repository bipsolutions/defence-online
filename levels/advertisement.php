<?php 

		$unique_id = uniqid();

		$style = generate_ad_padding(get_sub_field('top_padding'),get_sub_field('bottom_padding'),get_sub_field('left_padding'),get_sub_field('right_padding'));

		
?>
	<div class="level level-advertisement <?php echo $unique_id; ?>">

		<div class="pure-g">

			<div class="pure-u-24-24">

				<div style="<?php echo $style; ?>" class="ad-content-container">

					<?php echo $content = remove_autop(get_sub_field('content'));?>

				</div>	

			</div>

		</div>

	</div>
