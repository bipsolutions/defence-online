<?php 
	$distrubtion 		= 24 / count($boxes = get_sub_field('box'));
?>

<div class="level level-cta">
	<div class="pure-g dco-content">
		<?php if($title = get_sub_field('title')): ?>

				<div class="pure-u-24-24">
					<h2 class="line-along"><?php echo strtoupper($title); ?></h2>
					<?php if($image = get_sub_field('title_image')):?>
						<div class="level-header-image">
							<img src="<?php echo $image['sizes']['profile-logo'];?>"/>
						</div>
					<?php endif;?>
				</div>

		<?php endif;?>
		<?php foreach ($boxes as $key => $box): ?>
			<div class="pure-u-1 pure-u-md-<?php echo $distrubtion ?>-24">
				<div class="padding">
					<div class="box-container box-container-<?php echo $key ?>">
						<div class="padding">
							<div class="pure-g box-circle-container-parent">
								<div class="pure-u-1 center">

									<a href="<?php echo $boxes[$key]['link']; ?>">
											<div class="box-circle-container box-circle-container-<?php echo $key; ?>">
												<?php echo $boxes[$key]['box_icon'];?>
											</div>
									</a>

								</div>
								<div class="pure-u-1">
									<div class="center margin-top">
										<a class="box-title" href="<?php echo $boxes[$key]['link']; ?>"><?php echo $boxes[$key]['box_title']; ?></a>
									</div>	
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		<?php endforeach ?>
		<script>
			jQuery(document).imagesLoaded( function() {
					jQuery('.box-circle-container').each(function(){
						
						width = jQuery(this).width();
						jQuery(this).height(width);
						
						imageHeight 			= jQuery(this).find('i').height();
						imageContainerHeight 	= jQuery(this).height();
						reqMargin 				= (imageContainerHeight - imageHeight) / 2;

						jQuery(this).find('i').css('padding-top', reqMargin + 'px');

					});
			});
		</script>
	</div>
</div>
