<?php
	
	if(!get_sub_field('show_a_slider')){
		$distrubtion 	= 24 / get_sub_field('number_of_columns');
	}else{
		$distrubtion	= round((24 / (get_sub_field('number_of_columns') + 1)/24)*24);
	}
	$unique_id = uniqid();

	$showExcerpt = get_sub_field('show_excerpt');

	if(!$_SESSION['exclude_ids']){
		$_SESSION['exclude_ids'] = array();
	}
?>

<div class="level level-sub-features <?php echo $unique_id;?>">
		
		<div class="pure-g dco-content">

			<?php if($title = get_sub_field('title')): ?>

				<div class="pure-u-1">

					<h2 class="line-along"><?php echo strtoupper($title); ?></h2>

					<?php if($image = get_sub_field('title_image')):?>

						<div class="level-header-image">

							<img src="<?php echo $image['sizes']['profile-logo'];?>"/>

						</div>

					<?php endif;?>

				</div>

			<?php endif;?>

			<?php 

				$subfeatured = do_get_category_posts(get_sub_field('category_to_show'), get_sub_field('number_of_columns'));
				

				$args = array(
					'thumb' => 'sub-feature'
				);
				if(in_array(15, get_sub_field('category_to_show'))){
					$args['stakeholder'] = true;
				}
				if($showExcerpt){
					$args['excerpt'] = true;
				}

			?>
				<?php if($blurb = get_sub_field('section_blurb')):?>

					<div class="pure-u-1">

							<p><?php echo $blurb;?></p>

					</div>

				<?php endif; ?>

				<?php $count = 0; foreach($subfeatured as $sub_feature): ?>

					<div class="pure-u-1 pure-u-sm-12-24 pure-u-md-<?php echo $distrubtion; ?>-24 sub-feature">
						
						<div class="padding">
							
							<?php $args['delay'] = $count/6; do_sub_feature_panel($sub_feature, $args); ?>
						
						</div>
					
					</div>
				<?php $count++;?>

				<?php endforeach;?>
							
				<?php if(get_sub_field('show_a_slider')): ?>

					<?php if(get_sub_field('type_of_slider') =='logos'){

						$sliderArticles = get_sub_field('logos_to_show');

					}else{
						$sliderArticles = do_get_category_posts(get_sub_field('category_of_slider_content'), get_sub_field('max_number_of_sliders_to_show'));
					}
					
					?>

				<?php if(get_sub_field('type_of_slider') =='logos' ):?>

					<div class="pure-u-1 pure-u-sm-12-24 pure-u-md-<?php echo $distrubtion; ?>-24">
						
						<div class="pure-g">

							<?php do_logos_panel($sliderArticles, get_sub_field('slider_title'));?>

							<div class="pure-u-24-24">

								<p class="center"><a class="cta" href="<?php the_sub_field('button_link');?>"><?php the_sub_field('button_text');?></a>

							</div>

						</div>
				
					</div>

				<?php else:?>

					<div class="pure-u-1 pure-u-sm-12-24 pure-u-md-<?php echo $distrubtion; ?>-24 sub-feature">
					
						<div class="padding">
						
							<div class="sub-feature-slider unislide" >
							
								<ul>
								
								<?php foreach($sliderArticles as $sliderArticle): ?>

									<li>
									<?php $args = array(
											'slider' => true,
											'slider_type' => get_sub_field('type_of_slider'),
											'title' => get_sub_field('slider_title'),
											'delay' => ($count+1)/6

									);
										if($showExcerpt){
											$args['excerpt'] = true;
										} ?>
									<?php do_sub_feature_panel($sliderArticle, $args); ?>
									
									</li>

								<?php endforeach; ?>
									
								</ul>
									
							</div>
						
						</div>
							
					</div>
				
				<?php endif;?>
			
			<?php endif;?>
	
		</div>

	</div>

	
	<script type="text/javascript">
		
		jQuery(document).imagesLoaded( function() {

			get_max_height_from_set('.<?php echo $unique_id;  ?> .sub-image-container',0);
			get_max_height_from_set('.<?php echo $unique_id;  ?> .slide-title',0);
			get_max_height_from_set('.<?php echo $unique_id;  ?> .posted',0);
			get_max_height_from_set('.<?php echo $unique_id;  ?> .sub-feature-title-container',0);
			get_max_height_from_set('.<?php echo $unique_id;  ?> .sub-feature-excerpt',0);
			get_max_height_from_set('.<?php echo $unique_id;  ?> .unislide .sub-feature-title-container',10);

			
			<?php if(get_sub_field('show_a_slider')): ?>
				
				var subFeatureSlider = jQuery('.<?php echo $unique_id;?> .sub-feature-slider').unslider({
							arrows: false
						});
				
				jQuery('.<?php echo $unique_id;?> .sub-feature .click-left').click(function(){
					subFeatureSlider.unslider('prev');
				});
				jQuery('.<?php echo $unique_id;?> .sub-feature .click-right').click(function(){
					subFeatureSlider.unslider('next');
				});
			
			<?php endif?>
					
		});

	</script>




	