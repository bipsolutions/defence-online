

<div class="level level-basic">
	<div class="pure-g dco-content">
		<?php if($title = get_sub_field('title')): ?>

				<div class="pure-u-24-24">
					<h2 class="line-along"><?php echo strtoupper($title); ?></h2>
					<?php if($image = get_sub_field('title_image')):?>
						<div class="level-header-image">
							<img src="<?php echo $image['sizes']['profile-logo'];?>"/>
						</div>
					<?php endif;?>
				</div>

		<?php endif;?>
		

			<div class="pure-u-1 pure-u-md-12-24">
				<div class="padding-right padding-top">

					<?php the_sub_field('left_column');?>
					
				</div>
			</div>
			<div class="pure-u-1 pure-u-md-12-24 do-sidebar">
				<div class="padding-left">

					<?php get_do_sidebar(); ?>

				</div>
			</div>

		
		

		
	</div>
</div>
