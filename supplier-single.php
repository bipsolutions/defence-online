<?php
/**
 * @package WordPress
 * @subpackage HTML5-Reset-WordPress-Theme
 * @since HTML5 Reset 2.0
 */
 get_header(); ?>

<?php if(!do_is_this_gated()): ?>

 <div class="pure-g dco-content do-blog">

 	<div class="pure-u-12-24">

 		<div class="padding">

			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

			<?php
				
				$thumb = 'main-feature';
				$image = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID ), $thumb );
				$term = get_field('news_section',$post->ID);
				$termName = get_term_by('id', $term, 'news_section');
				$termName = $termName->name; ?>

		
			<div class="main-feature-container">
				
					<?php if($image):?>
					
					<div class="overlay">

						<div class="padding-double">

							<?php do_do_post_title($post->ID, $term, $termName)?>
				
						</div>
							
					</div>

					<img src="<?php echo $image[0]?>">

				<?php else:?>

					<div class="no-image-title-container">

						<?php do_do_post_title($post->ID, $term, $termName)?>
						
					</div>

				<?php endif; ?>

			</div>
			
			<?php the_content(); ?>

			<?php do_do_tags(); ?>

			<?php do_post_author(); ?>
			
			<?php edit_post_link(__('Edit this entry','html5reset'),'','.'); ?>
			
		

		<?php endwhile; endif; ?>

		
 		</div>
 		
 	</div>

	 <div class="pure-u-12-24 do-sidebar">

	 		<?php get_do_sidebar(); ?>
	 </div>

 </div>

<?php else: ?>
	<!-- What to do if the content is gated -->
	<?php include_once('access.php'); ?>

<?php endif; ?>
	
<?php get_footer(); ?>