<?php
/*
Template Name: Stakeholder New Post

*/
acf_form_head();
 get_header();

 $current_user = wp_get_current_user();
 ?>
 <?php if(!do_is_this_gated()): ?>
		
		<?php if(in_array_any( array('administrator', 'stakeholder'), $current_user->roles) ):?>

			<div class="level stakeholder-new-post">
				
				<div class="pure-g">

					<div class="pure-u-1 pure-u-md-18-24">

							<div class="page-header">
								
								<h2 class="line-along"><?php the_title(); ?></h2>
								
								<a class="cta stakeholder-new" href="<?php echo get_the_permalink(1909);?>">Back to Microsite Admin</a>

							</div>

							<div class="padding">



							<?php 
							
								$user_id = get_current_user_id();
								$post_id = 'user_' . $user_id;

								if(isset($_REQUEST['delete'])){
									$post_id = do_decode_hash($_REQUEST['delete']);

									if(do_post_exists( $post_id  )){
										
										$post_author_id = get_post_field( 'post_author', $post_id );

										if($post_author_id == $user_id){

											$post_deleted = wp_delete_post( $post_id );

											if($post_deleted){
												echo '<div id="message" class="updated"><p>Your post has been successfully deleted.</p></div>';
											}else{
												echo '<div id="message" class="error"><p>There was an error deleting your post.</p></div>';
											}

										}else{
											echo '<div id="message" class="error"><p>There was an error deleting your post. You do not appear to be the owner of this post</p></div>';
										}

									}

								};

								if(isset($_REQUEST['hash'])){

									$post_id = do_decode_hash($_REQUEST['hash']);
										
									if ( do_post_exists( $post_id  ) && get_post_type($post_id) =='post' ) {

										$post_id = $post_id;

	   								}

	   								$post_author_id = get_post_field( 'post_author', $post_id );

	   								if($post_author_id != $user_id){

	   									$post_id = 'new_post';

	   								}

								}else{

									$post_id = 'new_post';

								}
								if(isset($_REQUEST['hash'])){
									$updated_message = 'Your post has been updated';
								}else{
									$updated_message = 'Your post has been submitted and will be live once moderated.';
								}	
							
								acf_form( array(
									'post_id'		=> $post_id,
									'post_title'	=> true,
									'submit_value'	=> 'Submit Post',
									'post_content'	=> true,
									'updated_message' => $updated_message,
									'field_groups' 	=> array(2312, 2620),
									'new_post'		=> array(
										'post_status'		=> 'draft',
										'post_category' 	=>	array(15),
										'post_author' 		=> $user_id,
										'meta_input' 		=> array(
											'stakeholder_post' 	=> 1
										)
									)
								));?>
								<?php if(is_numeric($post_id)):?>
									<?php $hash = do_encode_hash($post_id); ?>
									<a class="cta confirm" style="float: right; margin-top: -2.5em;" href="?delete=<?php echo $hash;?>">Delete Post</a>
								<?php endif;?>
							</div>

					</div>

					<div class="pure-u-1 pure-u-md-6-24 dco-content">

						<div class="padding">
						<?php 
							
							$args = array(
								'posts_per_page'   => -1,
								'orderby'          => 'date',
								'order'            => 'DESC',
								'post_type'        => 'post',
								'author'	   	   => $user_id,
								'post_status'      => 'draft',							
							);
						
							$author_posts = get_posts($args); ?>
							
							<h3>Posts awaiting moderation</h3>
							
							<?php if($author_posts):?>
						
								<ul>
									
									<?php foreach($author_posts as $author_post):?>
										
										<li>
											
											<?php $hash = do_encode_hash($author_post->ID); ?>
											
											<a href="?hash=<?php echo $hash; ?>"><?php echo get_the_title($author_post->ID); ?></a> <a href="?hash=<?php echo $hash; ?>">[Edit]</a>

										</li>
									
									<?php endforeach; ?>
								
								</ul>
							
							<?php else:?>
							
								<p>You have yet to submit a post</p>
						
							<?php endif?>

							<?php $args = array(
								'posts_per_page'   => -1,
								'orderby'          => 'date',
								'order'            => 'DESC',
								'post_type'        => 'post',
								'author'	   	   => $user_id,
								'post_status'      => 'publish',							
							);
						
							$author_posts = get_posts($args); ?>
							
							<h3>My published posts</h3>
							
							<?php if($author_posts):?>
						
								<ul>
									
									<?php foreach($author_posts as $author_post):?>
										
										<li>
											<?php $hash = do_encode_hash($author_post->ID); ?>
											<a target="_blank" href="<?php echo get_the_permalink($author_post->ID);?>"><?php echo get_the_title($author_post->ID); ?></a> <a href="?hash=<?php echo $hash; ?>">[Edit]</a>

										</li>
									
									<?php endforeach; ?>
								
								</ul>
							
							<?php else:?>
							
								<p>You have yet to submit a post</p>
						
							<?php endif?>

							<p style="padding-top: 30px"><a class="cta" href="<?php the_permalink(); ?>">New Post</a>

						</div>

					</div>
				
				</div>
		
			</div>

		<?php else:?>

			<div class="level">

				<div class="pure-g">

					<div class="pure-u-24-24">

							not allowed
						
					</div>
					
				</div>
			
			</div>
		
		<?php endif;?>

<?php else: ?>

	<!-- What to do if the content is gated -->
	<?php include_once('access.php'); ?>

<?php endif; ?>

<?php get_footer(); ?>
<script type="text/javascript">
	jQuery(function() {
    jQuery('.confirm').click(function(e) {
        e.preventDefault();
        if (window.confirm("Are you sure?")) {
            location.href = this.href;
        }
    });
});

</script>
