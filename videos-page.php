<?php
/*
Template Name: Video Index Page

*/
 get_header(); ?>

<div class="pure-g dco-content do-videos">

 	<div class="pure-u-24-24">

 			<div class="pure-u-24-24">
				
				<h2 class="line-along">
					
					<?php global $post;?>
					
					<?php echo strtoupper(	$post->post_title ) ; ?>
											
				</h2>

			</div>

			<?php $videos = do_post_by_custom_post('videos', 10); ?>

			<?php if($videos):?>

				<div class="pure-g">

					<?php $count = 1;
						 $totalrows = ceil(count($videos) / 2);
					?>
					
					<?php foreach ($videos as $video):?>
						<?php $side = ($count % 2 == 0) ? 'right' : 'left'; $row = ceil($count/2); ?>
						
						<div class="pure-u-12-24 video-<?php echo $side; ?> video-row-<?php echo $row; ?>">
							<div class="padding">
								
								<div class="video-container">
								
									<div class="video-box">
									
										<?php echo do_shortcode('[fve]'. get_field('video_url', $video->ID) .'[/fve]')?>
										
									</div>
									
									<div class="video-content">

										<h3><?php echo $video->post_title;?></h3>
										<?php echo get_field('video_content', $video->ID); ?>

									</div>

								</div>

							</div>

						</div>

						<?php $count++;?>
					
					<?php endforeach;?>

				</div>

			<?php endif;?>

		</div>

	</div>

<?php get_footer(); ?>

	<script>
		$(document).ready(function (){
			
			<?php for($i = 1; $i <= $totalrows; $i++):?>
					get_max_height_from_set('.video-row-<?php echo $i; ?> .video-content',0);
					get_max_height_from_set('.video-row-<?php echo $i; ?> h3', 0);
					get_max_height_from_set('.video-row-<?php echo $i; ?> .video-box', 0);
			<?php endfor; ?>
		});

	</script>