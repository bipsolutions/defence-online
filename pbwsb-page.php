<?php
/*
Template Name: Page Builder with Sidebar

*/
 get_header(); global $post;?>

<div class="level content-with-sidebar">

	<div class="pure-g">
 		
 		<div class="pure-u-24-24">
 						
 			<div class="page-header">
 				
 				<h2 class="line-along"><?php echo strtoupper(get_the_title()) ; ?></h2>
 			
 			</div>

 		</div>

	</div>
	
	<div class="pure-g">

		<div class="pure-u-1 pure-u-md-18-24 dco-content">
			
			<div class="padding-top padding-right">

				<?php if($content = get_the_content($post->ID)):?>

					<?php echo apply_filters('the_content', $content); ?>

				<?php endif;?>
				
				<?php do_page_builder('levels', 'levels', 'level_status', $post->ID); ?>

			</div>
		
	</div>
	
	<div class="pure-u-1 pure-u-md-6-24 do-full-sidebar">
		
		

			<?php do_page_builder('sidebar_levels', 'sidebar_levels', 'level_status', $post->ID); ?>
			
		
	
	</div>

</div>	

<?php get_footer(); ?>
