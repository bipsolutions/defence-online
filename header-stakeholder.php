<?php
/**
 * @package WordPress
 * @subpackage HTML5-Reset-WordPress-Theme
 * @since HTML5 Reset 2.0
 */
?><!doctype html>

<!--[if lt IE 7 ]> <html class="ie ie6 ie-lt10 ie-lt9 ie-lt8 ie-lt7 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7 ]>    <html class="ie ie7 ie-lt10 ie-lt9 ie-lt8 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8 ]>    <html class="ie ie8 ie-lt10 ie-lt9 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 9 ]>    <html class="ie ie9 ie-lt10 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 9]><!--><html class="no-js" <?php language_attributes(); ?>><!--<![endif]-->
<!-- the "no-js" class is for Modernizr. -->

<head data-template-set="html5-reset-wordpress-theme">

	<meta charset="<?php bloginfo('charset'); ?>">

	<!-- Always force latest IE rendering engine (even in intranet) -->
	<!--[if IE ]>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<![endif]-->

	<?php
		if (is_search())
			echo '<meta name="robots" content="noindex, nofollow" />';
	?>

	<title><?php wp_title( '|', true, 'right' ); ?></title>

	<meta name="title" content="<?php wp_title( '|', true, 'right' ); ?>">

	<!--Google will often use this as its description of your page/site. Make it good.-->
	<meta name="description" content="<?php bloginfo('description'); ?>" />

	<meta name="Copyright" content="Copyright &copy; <?php bloginfo('name'); ?> <?php echo date('Y'); ?>. All Rights Reserved.">
	<meta name="viewport" content="width=device-width, initial-scale=1.0 minimal-ui">

	<!-- concatenate and minify for production -->
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/reset.css" />
	<link rel="stylesheet" href="<?php echo get_stylesheet_uri(); ?>" />

	<!-- Lea Verou's Prefix Free, lets you use only un-prefixed properties in yuor CSS files -->
    <script src="<?php echo get_template_directory_uri(); ?>/_/js/prefixfree.min.js"></script>

	
	<script src="<?php echo get_template_directory_uri(); ?>/_/js/modernizr-2.8.0.dev.js"></script>

	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

	<?php wp_head(); ?>
	<script src="https://npmcdn.com/imagesloaded@4.1/imagesloaded.pkgd.min.js"></script>
	<script>
		//Had to put this function in the header because of a scope issue with imageLoaded Function
		function get_max_height_from_set(classSet, offset){

		  if ( jQuery(window).width() > 760 ) {
			var maxHeight = Math.max.apply(null, jQuery(classSet).map(function (){
				   
				return jQuery(this).height();

			}).get());
			
			jQuery(classSet).each(function(){
				
				jQuery(this).height(maxHeight - offset);
					
			});
		  } 

		}
	</script>

	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,300italic,400italic,600italic,700' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,300italic,700' rel='stylesheet' type='text/css'>

	<!--[if gt IE 8]><!-->
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/grids-responsive.css">
	<!--[endif]---->
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/unslider.css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/unslider-dots.css">
	<script src="<?php echo get_template_directory_uri(); ?>/js/unslider-min.js"></script>
	

	<script type="text/javascript">
		piAId = '83632';
		piCId = '14256';

		(function() {
		    function async_load(){
		       var s = document.createElement('script'); s.type = 'text/javascript';
		           s.src = ('https:' == document.location.protocol ? 'https://pi' : 'http://cdn') + '.pardot.com/pd.js';
		       var c = document.getElementsByTagName('script')[0]; c.parentNode.insertBefore(s, c);
		     }
		     if(window.attachEvent) {
		     	window.attachEvent('onload', async_load);
		     } else { window.addEventListener('load', async_load, false); }
		})();
	</script>

	
	<?php do_form_session(); session_start();?>

	<?php  $sh_id = $wp_query->query_vars['author'];

			global $wp_query;
			
			if($wp_query->query['post_type'] == 'stakeholders'){
				$sh_id = do_get_stakeholder_id();
			}
			if(is_stakeholder_news() && is_single()){
				global $post;
				$sh_id = $post->post_author;
			}

	 		$mainThemeColour 		= get_field('sh_main_theme_colour', 'user_' .$sh_id);
	 		$buttonTextColor 		= get_field('sh_button_text_colour', 'user_' .$sh_id);
	 		$headerFontColur		= get_field('sh_header_font_colour', 'user_' .$sh_id);
	 		$mainTextColour 		= get_field('sh_font_colour', 'user_' .$sh_id);
	 		$linkColour				= get_field('sh_link_colour', 'user_' .$sh_id);
	 		$textOnColorBg			= get_field('text_colour_on_theme_background_colour', 'user_' .$sh_id);
	 		$rgbMainTheme			= hex2rgb($mainThemeColour);
 ?>

 <style>
 	.sh-content-header{
 	 	background-color: rgba(<?php echo $rgbMainTheme[0]?>,<?php echo $rgbMainTheme[1]?>,<?php echo $rgbMainTheme[2]?>,0.6);
	 }
	 /* Added due to change of the tag from h2 to h1 at author/dprte page to keep the appearance unchanged. SEO reasons */
 	h1.line-along, h1.line-along span{
 		color: <?php echo $mainThemeColour;?> !important;
 	}
 	h2.line-along, h2.line-along span{
 		color: <?php echo $mainThemeColour;?> !important;
 	}
 	.sh-content-header p{
 		color: <?php echo $headerFontColur;?> !important; 
 	}
 	.sh-content-header p a{
 		color: <?php echo $textOnColorBg;?> !important; 
 	}
 	.single-stakeholder a{
 		color:<?php echo $linkColour?> !important;
 	}
 	.single-stakeholder a.cta{
 		background-color: <?php echo $mainThemeColour;?> !important;
 		color: <?php echo $textOnColorBg?> !important;
 	}
 	.single-stakeholder p{
 		color: <?php echo $mainTextColour?> !important;	
 	}
 	.mapp-layout{
 		border:1px solid <?php echo $mainTextColour?> !important;
 	}
 	.single-stakeholder .dco-content .sub-feature a.sub-feature-title{
 		color:  <?php echo $mainThemeColour;?> !important;	
 	}
 	.level-nav-bar{
 		background-color: <?php echo $mainThemeColour;?> !important;
 	}
 	.no-image-title-container a, .no-image-title-container{
 		color: <?php echo $mainThemeColour;?> !important;
 	}
 	.single-stakeholder i{
 		color: <?php echo $mainThemeColour;?> !important;
 	}
 	.service-title, .cs-title, .cs-title a{
 		color: <?php echo $mainThemeColour;?> !important;
 	}
 	 a.sub-feature-more i, .cs-level a.readmore i{
 		color:<?php echo $linkColour?> !important;
 	}
 	.cs-content h1, .cs-content h2, .cs-content h3, .cs-content h4,.cs-content h5, .cs-content h6{
 		color: <?php echo $mainThemeColour;?> !important;
 	}
 	.cs-content td, .cs-content th{
 		border:1px solid <?php echo $mainTextColour?> !important;
 		padding:7px;
 	}
 	.cs-content th{
 		background-color: <?php echo $mainThemeColour;?> !important;
 		color: <?php echo $headerFontColur;?> !important;
 	}
 	.sh-content-header p a.small-icon{
 		color: <?php echo $linkColour?> !important;
 	}
 	.single-stakeholder .dco-content li:before{
 		color: <?php echo $mainThemeColour;?> !important;
 	}

 	<?php echo $sh_custom_css = get_field('sh_custom_css', 'user_' .$sh_id) ? get_field('sh_custom_css', 'user_' .$sh_id) : '' ; ?>
 </style>

</head>



<body <?php body_class(); ?>>
		
		<?php do_action( 'do_do_after_body' ); ?>


		<nav class="cbp-spmenu cbp-spmenu-horizontal cbp-spmenu-top cbp">
			
			<div class="level level-header">

				<div class="pure-g">

					<div class="pure-u-1 pure-u-lg-24-24">

						<div class="pure-g">

							<div class="pure-u-1 pure-u-md-5-24">
								
								<div id="pulldown-logo-container">
									<a href="<?php bloginfo('url'); ?>" title="Return to the homepage">
										<img src="<?php echo dco_main_logo_image_info()['url'];?>"></a>
								
								</div>
							
							</div>

							<div class="pure-u-12-24 hide-xs hide-sm free-content">
								
								<?php the_field('header-free-content','options'); ?>
							
							</div>

							<div class="pure-u-7-24 hide-xs hide-sm account-buttons">
								<div class="pure-g">

								<?php if(!is_user_logged_in()):?>
									
									<div class="pure-u-11-24">
										
										<?php do_login_button('Register'); ?>
									
									</div>

									<div class="pure-u-2-24">
							
									</div>


									<div class="pure-u-11-24">
										
										<?php do_login_button('Login'); ?>
									
									</div>

									<div class="pure-u-24-24 blank-row">
										
										&nbsp;
									
									</div>

								<?php else:?>

										<?
							$current_user_id = get_current_user_id();
							$user_info = get_userdata($current_user_id);
						?>
						<?php if( in_array('stakeholder', $user_info->roles) ):?>
							<div class="pure-u-11-24">
							<a href="<?php bloginfo('url');?>/stakeholder-admin/" class="cta-full cta-red"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> STAKEHOLDER ADMIN</a>
							</div>
							<div class="pure-u-2-24">
							
							</div>
							<div class="pure-u-11-24">
							<a href="<?php echo wp_logout_url( $redirect ); ?>" class="cta-full cta-red"><i class="fa fa-sign-out" aria-hidden="true"></i> LOGOUT</a>
							</div>
						<?php else:?>
						<div class="pure-u-24-24">
							<a href="<?php echo wp_logout_url( $redirect ); ?>" class="cta-full cta-red"><i class="fa fa-sign-out" aria-hidden="true"></i> LOGOUT</a>
						</div>
						<?php endif;?>

						<div class="pure-u-24-24 blank-row">
								&nbsp;
						</div>
								<?php endif; ?>

								<div class="social-container padding-vertical">
							<div class="pure-u-4-24 social">
								<a href="<?php the_field('twitter_url','options');?>" class="fa-stack">
										<i class="fa fa-circle fa-stack-2x" aria-hidden="true"></i>
										<i class="fa fa-twitter fa-stack-1x fa-inverse"></i>
								</a>
							</div>
							<div class="pure-u-4-24 social">
								<a href="<?php the_field('linkedin_url','options');?>" class="fa-stack">
										<i class="fa fa-circle fa-stack-2x" aria-hidden="true"></i>
										<i class="fa fa-linkedin fa-stack-1x fa-inverse"></i>
								</a>
							</div>
							<div class="pure-u-4-24 social">
								<a href="<?php the_field('media_url','options');?>" class="fa-stack">
										<i class="fa fa-circle fa-stack-2x" aria-hidden="true"></i>
										<i class="fa fa-video-camera fa-stack-1x fa-inverse"></i>
								</a>
							</div>
							<div class="pure-u-4-24 social">
								<a href="<?php the_field('facebook_url','options');?>" class="fa-stack">
										<i class="fa fa-circle fa-stack-2x" aria-hidden="true"></i>
										<i class="fa fa-facebook fa-stack-1x fa-inverse"></i>
								</a>
							</div>
							<div class="pure-u-8-24">
								<a href="<?php the_field('header_button_url_two','options');?>" class="cta-full cta-red">
									<?php echo strtoupper(get_field('header_button_two','options'));?>
								</a>
							</div>
						
						</div>
								
								
						
						
							</div>
							</div>

							

							

						</div>
					
					</div>

					<div class="pure-u-6-24 header-right hide-xs hide-sm hide-md">

						
					
					</div>

				</div>

			</div>

			<div id="header" class="level level-nav-bar">

		<div class="pure-g">

			<div class="pure-u-4-24 hide-lg hide-xl">

				<button id="nav-toggle" class="toggle-menu menu-left"><span></span></button>

			</div>

			<div class="pure-u-24-24 hide-xs hide-sm hide-md">

				<?php $image = get_field('inverted_site_logo', 'options'); ?>

				<div class="fixed-logo-container">
					<a href="<?php bloginfo('url'); ?>" title="Return to the homepage">
						<img src="<?php echo $image['sizes']['mini-logo'] ?>" />
					</a>
				</div>

				<nav id="nav" role="navigation">

					<ul class="sf-menu">
						<?php

							$walker = new do_walker; 
							wp_list_pages(array('walker' => $walker, 'title_li' => '')); ?>

					</ul>

				</nav>

			</div>

			

		</div>

	</div>

		</nav>

		<nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left">
	
		<h3>Menu</h3>

		<?php $walker = new do_walker; 
			
			wp_list_pages(array('walker' => $walker, 'title_li' => '')); ?>

			<?php if($thelink == the_field('header_button_url','options')):?>
			<li>
				<a href="<?php echo $thelink;?>" class="cta-full">
					<?php echo strtoupper(get_field('header_button','options'));?>
				</a>
			</li>
		<?php endif;?>

		<?php if(is_user_logged_in()): ?>
			
			<li>
				<a href="<?php echo wp_logout_url( get_bloginfo('url') ); ?>" class="loginButton">Logout</a>
			</li>

		<?php else: ?>

			<li>
				<a href="<?php echo wp_login_url( home_url() ); ?>" title="Login">Login</a>
			</li>
		<?php endif;?>

		<li style="padding:10px">
			
				<?php get_search_form( true ); ?>
			
		</li>

	</nav>
		
		<nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-right">
	
		<h3>Menu</h3>

		<?php stakeholder_select_form(false, true);?>

		<?php  do_stakeholder_menu($sh_id); ?>

		<?php if($thelink == get_field('header_button_url','options')):?>
			<li>
				<a href="<?php echo $thelink;?>" class="cta-full">
					<?php echo strtoupper(get_field('header_button','options'));?>
				</a>
			</li>
		<?php endif;?>
		<?php if(is_user_logged_in()): ?>
			
			<li>
				<a href="<?php echo wp_logout_url( get_bloginfo('url') ); ?>" class="loginButton">Logout</a>
			</li>

		<?php else: ?>

			<li>
				<a href="<?php echo wp_login_url( home_url() ); ?>" title="Login">Login</a>
			</li>
		<?php endif;?>

		<li style="padding:10px">
			
				<?php get_search_form( true ); ?>
			
		</li>

	</nav>




	<?PHP stakeholder_select_form(true); ?>
	<div id="header" class="level level-nav-bar level-nav-bar-stakeholder">

		<div class="pure-g">

			<div class="pure-u-4-24 hide-lg hide-xl">

				<button id="nav-toggle-sh" class="toggle-menu menu-right"><span></span></button>

			</div>

			<div class="pure-u-18-24 hide-xs hide-sm hide-md">

				<?php $image = get_field('inverted_site_logo', 'options'); ?>

				<div class="fixed-logo-container">
					<a href="<?php bloginfo('url'); ?>" title="Return to the homepage">
						<img alt="<?=$image['alt']; ?>" src="<?php echo $image['sizes']['mini-logo'] ?>" />
					</a>
				</div>

				<nav id="nav" role="navigation">

					<ul class="sf-menu">
						
							<?php do_stakeholder_menu($sh_id); ?>

					</ul>

				</nav>

			</div>

			<div class="pure-u-20-24 pure-u-lg-6-24">

					<?php get_search_form(); ?>

			</div>

		</div>

	</div>


	<?php 


	$headerBackground = get_stakeholder_background($sh_id); ?>
	
	<div class="level level-stakeholder" style="background-image: url('<?php echo $headerBackground; ?>')">

		<div class="pure-g">

			<div class="pure-u-1" >
					<div class="sh-logo-container">
						
						<?php $logoImage = get_field('sh_company_logo', 'user_' . $sh_id)?>
						<img alt="<?=$logoImage['alt']; ?>" src="<?php echo $logoImage['sizes']['profile-logo'];?>" />

					</div>
					<div class="sh-content-header">
						<p>
						<?php if($location = get_field('sh_company_location', 'user_' . $sh_id)):?>
							<i class="fa fa-map-marker" aria-hidden="true"></i> <?php echo $location['address']; ?> 
						<?php endif;?>
						<?php if($email = get_field('sh_company_email', 'user_' . $sh_id)):?>
							|
							<i class="fa fa-envelope" aria-hidden="true"></i> <a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a>  
						<?php endif;?>
						<?php if($phone = get_field('sh_company_telephone', 'user_' . $sh_id)):?>
							|
							<i class="fa fa-phone-square" aria-hidden="true"></i> <?php echo $phone;?>
						<?php endif;?>
						<?php if($web = get_field('sh_company_web', 'user_' . $sh_id)):?>
							
							<a title="Website" target="_blank" href="<?php echo $web; ?>" class="fa-stack small-icon">
							<i class="fa fa-circle fa-stack-2x" aria-hidden="true"></i>
							<i class="fa fa-home fa-stack-1x fa-inverse" aria-hidden="true"></i>
							</a>
						<?php endif;?>
						<?php if($twitter = get_field('twitter', 'user_' . $sh_id)):?>
							
							<a href="<?php echo $twitter ?>" class="fa-stack small-icon">
								<i class="fa fa-circle fa-stack-2x" aria-hidden="true"></i>
								<i class="fa fa-twitter fa-stack-1x fa-inverse" aria-hidden="true"></i>
							</a>
						<?php endif;?>
						<?php if($facebook = get_field('facebook', 'user_' . $sh_id)):?>
							
							<a href="<?php echo $facebook ?>" class="fa-stack small-icon">
								<i class="fa fa-circle fa-stack-2x" aria-hidden="true"></i>
								<i class="fa fa-facebook fa-stack-1x fa-inverse" aria-hidden="true"></i>
							</a>
						<?php endif;?>
						<?php if($linkedin = get_field('linkedin', 'user_' . $sh_id)):?>
							
							<a href="<?php echo $linkedin ?>" class="fa-stack small-icon">
								<i class="fa fa-circle fa-stack-2x" aria-hidden="true"></i>
								<i class="fa fa-linkedin fa-stack-1x fa-inverse" aria-hidden="true"></i>
							</a>
						<?php endif;?>
						</p>
					</div>
			</div>
			
		</div>

	</div>
	
	

