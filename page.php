<?php
/**
 * @package WordPress
 * @subpackage HTML5-Reset-WordPress-Theme
 * @since HTML5 Reset 2.0
 */

acf_form_head();

get_header();

do_action('do_after_head');

echo "<div class='level takeover-container'>";
		do_page_builder('levels', 'levels', 'level_status');
echo "</div>";

do_action('do_after_page');

get_footer(); ?>
