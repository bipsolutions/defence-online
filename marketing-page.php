<?php
/*
Template Name: Marketing Page

*/
 get_header(); ?>

<?php

  $do_paged = isset( $_GET['do_paged']) ? $_GET['do_paged'] : 1; 
 

  ?>

 <div class="level light-grey">

 	<div class="pure-g">
 	 		
 	 		<div class="pure-u-24-24">
 	 						
 	 			<div class="marketing-nav">
 	 				
 	 				<div class="pure-g">

 	 					<div class="pure-u-6-24">
	 	 					<a href="#<?php echo $titlehash1 = sanitize_title(get_field('section_heading_one')); ?>"><div class="mega-icon-container">
	 	 						<span class="fa-stack fa-lg">
	 							 	<i class="fa fa-circle fa-stack-2x"></i>
	  								<i class="fa <?php echo $icon1 = get_field('section_heading_one_icon'); ?> fa-stack-1x fa-inverse with-stroke"></i>
								</span>
								<p class="marketing-nav-item"><?php echo $the_title1 = get_field('section_heading_one');?></p>
	 	 					</div>
	 	 				</a>
 	 						
 	 					</div>
 	 					<div class="pure-u-6-24">
 	 						<a href="#<?php echo $titlehash2 = sanitize_title(get_field('section_heading_two')); ?>">
	 	 					<div class="mega-icon-container">
	 	 						<span class="fa-stack fa-lg">
	 							 	<i class="fa fa-circle fa-stack-2x"></i>
	  								<i class="fa <?php echo $icon2 = get_field('section_heading_two_icon'); ?> fa-stack-1x fa-inverse with-stroke"></i>
								</span>
								<p class="marketing-nav-item"><?php echo $the_title2 = get_field('section_heading_two');?></p>
	 	 					</div>
	 	 					</a>
 	 						
 	 					</div>

 	 					<div class="pure-u-6-24">
 	 						<a href="#<?php echo $titlehash3 = sanitize_title(get_field('section_heading_three')); ?>">
		 	 					<div class="mega-icon-container">
		 	 						<span class="fa-stack fa-lg">
		 							 	<i class="fa fa-circle fa-stack-2x"></i>
		  								<i class="fa <?php echo $icon3 = get_field('section_heading_three_icon'); ?> fa-stack-1x fa-inverse with-stroke"></i>
									</span>
									<p class="marketing-nav-item"><?php echo $the_title3 = get_field('section_heading_three');?></p>
		 	 					</div>
	 	 					</a>
 	 						
 	 					</div>

						<div class="pure-u-6-24">
							<a href="#<?php echo $titlehash4 = sanitize_title(get_field('section_heading_four')); ?>">
		 	 					<div class="mega-icon-container">
		 	 						<span class="fa-stack fa-lg">
		 							 	<i class="fa fa-circle fa-stack-2x"></i>
		  								<i class="fa <?php echo $icon4 = get_field('section_heading_four_icon'); ?> fa-stack-1x fa-inverse with-stroke"></i>
									</span>
									<p class="marketing-nav-item"><?php echo $the_title4 = get_field('section_heading_four');?></p>
		 	 					</div>
	 	 					</a>
 	 					</div>
 	 					
 	 					

 	 				</div>
 	 			
 	 			</div>
 	
 	 		</div>
 	
 	 	</div>

 </div>

 <div class="level marketing-image-level" style="background-image: url('<?php bloginfo('template_url'); ?>/images/marketing-image-1.jpg'); min-height: 179px;">

 	<div class="pure-g">
 	 		
 	 	<div class="pure-u-24-24">
 	 	
 	 	</div>

 	 </div>

 </div>

 <div class="level" name="<?php echo $titlehash1; ?>">

 	<div class="pure-g">
 	 		
 	 	<div class="pure-u-24-24">
 	 		<h2 class="marketing-level-header line-along">
 	 			<span class="fa-stack fa-lg">
	 				
	  				<i class="fa <?php echo $icon1; ?> fa-stack-1x "></i>
					</span><?php echo $the_title1; ?></h2>

					<div class="marketing-blurb-container">
						<?php if($blurb1 = get_field('blurb_above_one')):?>

							<?php echo $blurb1; ?>

						<?php endif;?>
					</div>

					<?php if($icons1 = get_field('icons_one')):?>
					<div class="pure-g">
						<div class="pure-u-12-24 pure-u-md-2-24 hide-xs hide-sm">
							
						</div>
						<?PHP 	$count_icons1 = count($icons1);
								$distrubution = 20/$count_icons1;
								$unique_id	  = uniqid();
								$count= 1;
						?>
						<?php while( have_rows('icons_one') ): the_row(); ?>

							<div class="pure-u-12-24 pure-u-md-<?php echo $distrubution; ?>-24">
								<div class="marketing-icon-container">
									<a data-remodal-target="modal-video-<?php echo $unique_id . "_" . $count ;?>" href="#"><img height="114" width="auto" src="<?php the_sub_field('icon')?>"/></a>
									<p><a data-remodal-target="modal-video-<?php echo $unique_id . "_" . $count ;?>" href="#"><?php the_sub_field('title'); ?></a></p>

									<div class="dco-content remodal left-align-remodal" data-remodal-id="modal-video-<?php echo $unique_id . "_" . $count ;?>">
																	  
										<button data-remodal-action="close" class="remodal-close"></button>
																	  
											<?php the_sub_field('window_content');; ?>
																	 
																	  
									</div>
								</div>
							</div>

						<?php $count++; endwhile; ?>
						
						<div class="pure-u-12-24 pure-u-md-2-24 hide-xs hide-sm">
							
						</div>
					</div>
					<?php endif;?>

					<p style="text-align: center; "><?php echo do_do_media_pack_marketing_cta(); ?></p>

					<h3 class="subcta"><span><?php the_field('sub_button_text');?></span></h3>
 	 	</div>

 	 </div>

 </div>

 <div class="level marketing-image-level" style="background-image: url('<?php bloginfo('template_url'); ?>/images/marketing-image-2.jpg'); min-height: 179px;">

 	<div class="pure-g">
 	 		
 	 	<div class="pure-u-24-24">
 	 	
 	 	</div>

 	 </div>

 </div>

  <div class="level" name="<?php echo $titlehash2; ?>">

 	<div class="pure-g">
 	 		
 	 	<div class="pure-u-24-24">
 	 		<h2 class="marketing-level-header line-along">
 	 			<span class="fa-stack fa-lg">
	 				
	  				<i class="fa <?php echo $icon2; ?> fa-stack-1x "></i>
					</span><?php echo $the_title2; ?></h2>

					<div class="marketing-blurb-container">
						<?php if($blurb2 = get_field('blurb_above_two')):?>

							<?php echo $blurb2; ?>

						<?php endif;?>
					</div>

					<?php if($icons2 = get_field('icons_two')):?>
					<div class="pure-g">
						<div class="pure-u-12-24 pure-u-md-2-24 hide-xs hide-sm">
							
						</div>
						<?PHP 	$count_icons2 = count($icons2);
								$distrubution = 20/$count_icons2;
								$unique_id	  = uniqid();
								$count= 1;
						?>
						<?php while( have_rows('icons_two') ): the_row(); ?>

							<div class="pure-u-12-24 pure-u-md-<?php echo $distrubution; ?>-24">
								<div class="marketing-icon-container">
									<a data-remodal-target="modal-video-<?php echo $unique_id . "_" . $count ;?>" href="#"><img height="114" width="auto" src="<?php the_sub_field('icon')?>"/></a>
									<p><a data-remodal-target="modal-video-<?php echo $unique_id . "_" . $count ;?>" href="#"><?php the_sub_field('title'); ?></a></p>

									<div class="dco-content remodal left-align-remodal" data-remodal-id="modal-video-<?php echo $unique_id . "_" . $count ;?>">
																	  
										<button data-remodal-action="close" class="remodal-close"></button>
																	  
											<?php the_sub_field('window_content');; ?>
																	 
																	  
									</div>
								</div>
							</div>

						<?php $count++;endwhile; ?>
						
						<div class="pure-u-12-24 pure-u-md-2-24 hide-xs hide-sm">
							
						</div>
					</div>
					<?php endif;?>

					<p style="text-align: center; "><?php echo do_do_media_pack_marketing_cta(); ?></p>

					<h3 class="subcta"><span><?php the_field('sub_button_text');?></span></h3>
 	 	</div>

 	 </div>

</div>

<div class="level marketing-image-level" style="background-image: url('<?php bloginfo('template_url'); ?>/images/handshake.jpg');background-position: center;
    background-size: cover;
    background-repeat: no-repeat;
    min-height: 179px;
    margin:0">

 	<div class="pure-g">
 	 		
 	 	<div class="pure-u-24-24">
 	 	
 	 	</div>

 	 </div>

 </div>

<div class="level marketing red" name="<?php echo $titlehash3; ?>">

 	<div class="pure-g">
 	 		
 	 	<div class="pure-u-24-24">
 	 		<h2 class="marketing-level-header line-along">
 	 			<span class="fa-stack fa-lg">
	 				
	  				<i class="fa <?php echo $icon3; ?> fa-stack-1x "></i>
					</span><?php echo $the_title3; ?></h2>

					<div class="marketing-blurb-container">
						<?php if($blurb3 = get_field('blurb_above_three')):?>

							<?php echo $blurb3; ?>

						<?php endif;?>
					</div>

					<?php if($icons3 = get_field('icons_three')):?>
					<div class="pure-g">
						<div class="pure-u-12-24 pure-u-md-2-24 hide-xs hide-sm">
							
						</div>
						<?PHP 	$count_icons3 = count($icons3);
								$distrubution = 20/$count_icons3;
								$unique_id	  = uniqid();
								$count= 1;
						?>
						<?php while( have_rows('icons_three') ): the_row(); ?>

							<div class="pure-u-12-24 pure-u-md-<?php echo $distrubution; ?>-24">
								<div class="marketing-icon-container">
									<a data-remodal-target="modal-video-<?php echo $unique_id . "_" . $count ;?>" href="#"><img height="114" width="auto" src="<?php the_sub_field('icon')?>"/></a>
									<p><a data-remodal-target="modal-video-<?php echo $unique_id . "_" . $count ;?>" href="#"><?php the_sub_field('title'); ?></a></p>

									<div class="dco-content remodal left-align-remodal" data-remodal-id="modal-video-<?php echo $unique_id . "_" . $count ;?>">
																	  
										<button data-remodal-action="close" class="remodal-close"></button>
																	  
											<?php the_sub_field('window_content');; ?>
																	 
																	  
									</div>
								</div>
							</div>

						<?php $count++;endwhile; ?>
						
						<div class="pure-u-12-24 pure-u-md-2-24 hide-xs hide-sm">
							
						</div>
					</div>
					<?php endif;?>

				
					<p style="text-align: center; "><?php echo do_do_media_pack_marketing_cta(); ?></p>

					<h3 class="subcta"><span><?php the_field('sub_button_text');?></span></h3>
		</div>

	</div>

</div>
 <div class="level marketing-image-level" style="background-image: url('<?php bloginfo('template_url'); ?>/images/ipad.jpg');     background-position: center;
    background-size: cover;
    background-repeat: no-repeat; min-height: 179px; margin:0">

 	<div class="pure-g">
 	 		
 	 	<div class="pure-u-24-24">
 	 	
 	 	</div>

 	 </div>

 </div>
<div class="level marketing blue" name="<?php echo $titlehash4; ?>">

 	<div class="pure-g">
 	 		
 	 	<div class="pure-u-24-24">
 	 			<h2 class="marketing-level-header line-along">
 	 			<span class="fa-stack fa-lg">
	 				
	  				<i class="fa <?php echo $icon4; ?> fa-stack-1x "></i>
					</span><?php echo $the_title4; ?></h2>

					<div class="marketing-blurb-container">
						<?php if($blurb4 = get_field('blurb_above_four')):?>

							<?php echo $blurb4; ?>

						<?php endif;?>
					</div>

					<?php if($icons4 = get_field('icons_four')):?>
					<div class="pure-g">
						<div class="pure-u-12-24 pure-u-md-2-24 hide-xs hide-sm">
							
						</div>
						<?PHP 	$count_icons4 = count($icons4);
								$distrubution = 20/$count_icons4;
								$unique_id	  = uniqid();
								$count= 1;
						?>
						<?php while( have_rows('icons_four') ): the_row(); ?>

							<div class="pure-u-12-24 pure-u-md-<?php echo $distrubution; ?>-24">
								<div class="marketing-icon-container">
									<a data-remodal-target="modal-video-<?php echo $unique_id . "_" . $count ;?>" href="#"><img height="114" width="auto" src="<?php the_sub_field('icon')?>"/></a>
									<p><a data-remodal-target="modal-video-<?php echo $unique_id . "_" . $count ;?>" href="#"><?php the_sub_field('title'); ?></a></p>

									<div class="dco-content remodal left-align-remodal" data-remodal-id="modal-video-<?php echo $unique_id . "_" . $count ;?>">
																	  
										<button data-remodal-action="close" class="remodal-close"></button>
																	  
											<?php the_sub_field('window_content');; ?>
																	 
																	  
									</div>
								</div>
							</div>

						<?php $count++;endwhile; ?>
						
						<div class="pure-u-12-24 pure-u-md-2-24 hide-xs hide-sm">
							
						</div>
					</div>
					<?php endif;?>

				<p style="text-align: center; "><?php echo do_do_media_pack_marketing_cta(); ?></p>

					<h3 class="subcta"><span><?php the_field('sub_button_text');?></span></h3>
		</div>

	</div>

</div>

<?php get_footer(); ?>
